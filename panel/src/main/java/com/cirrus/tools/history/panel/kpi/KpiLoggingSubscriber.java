/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file KpiLoggingSubscriber.java
 */
package com.cirrus.tools.history.panel.kpi;

import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.scs.studiolink.api.notifications.TopicNotification;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

class KpiLoggingSubscriber implements ISubscriber {
  private static final AtomicLong ON_MESSAGE_RECEIVED_MILLIS_MIN = new AtomicLong(Long.MAX_VALUE);
  private static final AtomicLong ON_MESSAGE_RECEIVED_MILLIS_MAX = new AtomicLong(Long.MIN_VALUE);
  private static final Logger LOG = Logger.getLogger(KpiLoggingSubscriber.class.getName());

  private static int instanceCount;
  private static int onMessageReceivedCount;
  private static int onMessageExceptionCount;

  private final ISubscriber subscriber;

  KpiLoggingSubscriber(ISubscriber subscriber) {
    this.subscriber = Objects.requireNonNull(subscriber);

    log("init", String.format("Count: [%d]", ++instanceCount));
  }

  @Override
  public String getName() {
    return subscriber.getName();
  }

  @Override
  public void setName(String subscriberName) {
    subscriber.setName(subscriberName);
  }

  @Override
  //This method is designed for, and cannot work without catching RuntimeException
  @SuppressWarnings({"PMD.AvoidCatchingGenericException", "checkstyle:IllegalCatch"})
  public void onMessageReceived(TopicNotification message) {
    try {
      long start = System.currentTimeMillis();

      subscriber.onMessageReceived(message);

      long duration = System.currentTimeMillis() - start;
      logOnMessageReceivedSuccess(duration);
    } catch (RuntimeException e) {
      logOnMessageReceivedException();
      throw e;
    }
  }

  private void log(String method, String msg) {
    LOG.severe(() -> String.format("%s|%s.", method, msg));
  }

  private void logOnMessageReceivedSuccess(long duration) {
    String method = "onMessageReceived";
    long min = ON_MESSAGE_RECEIVED_MILLIS_MIN.updateAndGet(previous -> Math.min(duration, previous));
    long max = ON_MESSAGE_RECEIVED_MILLIS_MAX.updateAndGet(previous -> Math.max(duration, previous));

    log(method, String.format(
        "Count: [%d], Duration: [%dms], Min: [%dms], Max: [%dms]",
        ++onMessageReceivedCount,
        duration,
        min,
        max));

    assert max >= min : String.format("Max [%d] less than min: [%d].", max, min);
  }

  private void logOnMessageReceivedException() {
    String msg = String.format(
        "Count: [%d], Exceptions: [%d]",
        ++onMessageReceivedCount,
        ++onMessageExceptionCount);

    log("onMessageReceived", msg);
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file KpiJmxSubscriber.java
 */
package com.cirrus.tools.history.panel.kpi;

import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.scs.studiolink.api.notifications.TopicNotification;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

class KpiJmxSubscriber implements ISubscriber, KpiJmxSubscriberMBean {
  private static final AtomicInteger INSTANCE_COUNT = new AtomicInteger();
  private static final AtomicInteger ON_MESSAGE_RECEIVED_CALL_COUNT = new AtomicInteger();
  private static final AtomicInteger ON_MESSAGE_RECEIVED_EXCEPTION_COUNT = new AtomicInteger();
  private static final AtomicLong ON_MESSAGE_RECEIVED_MILLIS_MIN = new AtomicLong(Long.MAX_VALUE);
  private static final AtomicLong ON_MESSAGE_RECEIVED_MILLIS_MAX = new AtomicLong(Long.MIN_VALUE);

  private final ISubscriber subscriber;

  KpiJmxSubscriber(ISubscriber subscriber) {
    this.subscriber = Objects.requireNonNull(subscriber);
    INSTANCE_COUNT.addAndGet(1);
  }

  @Override
  public String getName() {
    return subscriber.getName();
  }

  @Override
  public void setName(String subscriberName) {
    subscriber.setName(subscriberName);
  }

  @Override
  //This method is designed for, and cannot work without catching RuntimeException
  @SuppressWarnings({"PMD.AvoidCatchingGenericException", "checkstyle:IllegalCatch"})
  public void onMessageReceived(TopicNotification message) {
    ON_MESSAGE_RECEIVED_CALL_COUNT.addAndGet(1);

    try {
      long start = System.currentTimeMillis();

      subscriber.onMessageReceived(message);

      long duration = System.currentTimeMillis() - start;
      updateOnMessageReceivedSuccess(duration);
    } catch (RuntimeException e) {
      ON_MESSAGE_RECEIVED_EXCEPTION_COUNT.addAndGet(1);
      throw e;
    }
  }

  @Override
  public int getInstanceCount() {
    return INSTANCE_COUNT.get();
  }

  @Override
  public int getOnMessageReceivedCallCount() {
    return ON_MESSAGE_RECEIVED_CALL_COUNT.get();
  }

  @Override
  public long getOnMessageReceivedMillisMin() {
    return ON_MESSAGE_RECEIVED_MILLIS_MIN.get();
  }

  @Override
  public long getOnMessageReceivedMillisMax() {
    return ON_MESSAGE_RECEIVED_MILLIS_MAX.get();
  }

  @Override
  public int getOnMessageReceivedExceptionCount() {
    return ON_MESSAGE_RECEIVED_EXCEPTION_COUNT.get();
  }

  @Override
  public void resetStats() {
    ON_MESSAGE_RECEIVED_MILLIS_MIN.set(0);
    ON_MESSAGE_RECEIVED_MILLIS_MAX.set(0);

    INSTANCE_COUNT.set(0);
    ON_MESSAGE_RECEIVED_CALL_COUNT.set(0);
    ON_MESSAGE_RECEIVED_EXCEPTION_COUNT.set(0);

  }

  private void updateOnMessageReceivedSuccess(long duration) {
    ON_MESSAGE_RECEIVED_MILLIS_MIN.updateAndGet(previous -> Math.min(duration, previous));
    ON_MESSAGE_RECEIVED_MILLIS_MAX.updateAndGet(previous -> Math.max(duration, previous));
  }
}

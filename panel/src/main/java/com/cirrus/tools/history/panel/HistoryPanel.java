//  Copyright (c) 2020 Cirrus Logic, Inc and
//  Cirrus Logic International Semiconductor Ltd.  All rights reserved.
//
//  This software as well as any related documentation is furnished under
//  license and may only be used or copied in accordance with the terms of the
//  license.  The information in this file is furnished for informational use
//  only, is subject to change without notice, and should not be construed as
//  a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
//  liability for any errors or inaccuracies that may appear in this document
//  or any software that may be provided in association with this document.
//
//  Except as permitted by such license, no part of this document may be
//  reproduced, stored in a retrieval system, or transmitted in any form or by
//  any means without the express written consent of Cirrus Logic.
//
//  Warning
//    This software is specifically written for Cirrus Logic devices.
//    It may not be used with other devices.
//
//! @file   HistoryPanel.java
package com.cirrus.tools.history.panel;

import com.cirrus.scs.linkclient.events.ILinkClientEventArgs;
import com.cirrus.scs.linkclient.events.ILinkClientEventHandler;
import com.cirrus.scs.studiolink.api.IStudioLink;
import com.cirrus.scs.studiolink.api.exceptions.StudioLinkException;
import com.cirrus.tools.controls.Theme;
import com.cirrus.tools.framework.Panel;
import com.cirrus.tools.framework.PanelInfo;
import com.cirrus.tools.history.impl.LinkVersionChecker;
import com.cirrus.tools.history.javafx.HistoryControl;
import com.cirrus.tools.history.javafx.HistoryDataListJavaFX;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import javafx.scene.Parent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class HistoryPanel implements Panel {

  private static final int EXECUTOR_SHUTDOWN_TIMEOUT_SECS = 5;
  private static final Logger LOG = LoggerFactory.getLogger(HistoryPanel.class);

  private final ExecutorService executorService = Executors.newCachedThreadPool();
  private final ObjectProperty<Theme> themeProperty = new SimpleObjectProperty<>();

  private final LinkStatusHandler statusHandler = new LinkStatusHandler();
  private final BooleanProperty linkConnectedProperty = new SimpleBooleanProperty(false);

  private final IStudioLink link;
  private final HistoryControl control;
  private final HistoryDataListJavaFX historyData;

  /**
   * Constructor.
   */
  public HistoryPanel(PanelInfo panelInfo) {
    Objects.requireNonNull(panelInfo);
    historyData = new HistoryDataListJavaFX(executorService);
    control = new HistoryControl(historyData.getObservableList(), themeProperty);
    control.setDisplayButtonClickHandler(new DisplayButtonClickHandler());

    link = panelInfo.getContext().getLink();
    linkConnectedProperty.addListener((observable, oldValue, newValue) -> {
      if (newValue) {
        checkLinkVersions();
        Task<Boolean> task = historyData.startAsync(link.getNotificationManager());
        Platform.runLater(() -> { //Required for tests
          task.setOnFailed(__ ->
              LOG.error("Could not subscribe to topics.", task.getException())
          );
        });
      }
    });
    boolean linkIsReady = link.isRunning() && link.getNotificationManager() != null;
    if (!linkIsReady) {
      link.getIsRunningStatusChanged().addHandler(statusHandler);
    }
    linkConnectedProperty.set(linkIsReady);
  }

  private void checkLinkVersions() {
    try {
      new LinkVersionChecker().verify(link);
    } catch (StudioLinkException e) {
      LOG.error("Could not check link versions.", e);
    } catch (IllegalStateException e) {
      LOG.error("Incompatible link versions.", e);
    }

  }

  /**
   * Called when the suite is closed, giving the panel an opportunity to clean up etc.
   */
  @Override
  //This method is designed for, and cannot work without catching unhandled exceptions
  @SuppressWarnings({"PMD.AvoidCatchingGenericException", "checkstyle:IllegalCatch"})
  public void onClose() {
    try {
      // On close stop listening for state changes in link
      link.getIsRunningStatusChanged().removeHandler(statusHandler);
      // If we subscribe to link notifications stop.
      Task<Void> task = historyData.stopAsync();
      Platform.runLater(() ->
            task.setOnFailed(__ -> LOG.error("Shutdown problem.", task.getException())));
      executorService.shutdown();
      executorService.awaitTermination(EXECUTOR_SHUTDOWN_TIMEOUT_SECS, TimeUnit.SECONDS);
    } catch (Exception e) {
      e.printStackTrace();  //NOPMD - This is a last chance to report a problem (if logging fails)
      LOG.error("A problem occurred during onClose.", e);
      throw new IllegalStateException("A problem occurred during onClose.", e);
    }
  }

  @Override
  public Parent getRoot() {
    return control;
  }

  @Override
  public void onThemeChange(String themeName) {
    Objects.requireNonNull(themeName);
    Theme theme = Theme.fromThemeName(themeName);
    themeProperty.set(theme);
  }

  private final class LinkStatusHandler implements ILinkClientEventHandler<Boolean> {
    @Override
    public void handle(ILinkClientEventArgs<Boolean> eventArgs) {
      if (eventArgs.getData()) {
        link.getIsRunningStatusChanged().removeHandler(statusHandler);
        linkConnectedProperty.set(true);
      }
    }
  }
}

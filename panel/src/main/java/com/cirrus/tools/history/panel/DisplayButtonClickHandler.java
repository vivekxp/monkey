/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file DisplayButtonClickHandler.java
 */

package com.cirrus.tools.history.panel;

import com.cirrus.tools.history.javafx.DisplayDialogPane;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

class DisplayButtonClickHandler implements Callback<DisplayDialogPane, Void> {
  private final AtomicReference<Stage> dialogStageRef = new AtomicReference<>();

  @Override
  public synchronized Void call(DisplayDialogPane pane) {
    Objects.requireNonNull(pane);

    if (isShowing()) {
      bringToFront();
      return null;
    }

    Stage dialogStage = new Stage();
    dialogStage.setScene(toScene(pane));
    dialogStage.setOnCloseRequest(__ -> dialogStageRef.set(null));
    dialogStageRef.set(dialogStage);
    dialogStage.show();
    bringToFront();

    return null;
  }

  /**
   * Discovered during testing that if opening the dialog multiple times with the same instance
   * of pane, the previous Scene may not have been garbage collected leading to an
   * IllegalStateException when adding the existing pane to a new Scene because the pane is
   * already registered to the previous pane.
   */
  private Scene toScene(DisplayDialogPane pane) {
    Scene scene = pane.getScene();
    return scene == null ? new Scene(pane) : scene;
  }

  private void bringToFront() {
    Stage stage = dialogStageRef.get();
    if (stage != null) {
      stage.toFront();
    }
  }

  private boolean isShowing() {
    Stage stage = dialogStageRef.get();
    return stage != null && stage.isShowing();
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file DisplayButtonClickHandlerRobotTests.java
 */

package com.cirrus.tools.history.panel;

import com.cirrus.tools.controls.CirrusTableView;
import com.cirrus.tools.controls.Theme;
import com.cirrus.tools.framework.PanelInfo;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.javafx.DisplayDialogPane;

import com.cirrus.tools.test.CirrusApplicationTest;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DisplayButtonClickHandlerRobotTests extends CirrusApplicationTest {
  private HistoryPanel historyPanel;

  @BeforeEach
  void beforeEach() {
    PanelInfo mockPanelInfo = SetupUtil.newMockPanelInfo();
    historyPanel = new HistoryPanel(mockPanelInfo);

    interact(() -> root.getChildren().setAll(historyPanel.getRoot()));
  }

  @AfterEach
  void afterEach() {
    if (historyPanel != null) {
      Platform.runLater(() -> {
        historyPanel.onClose();
        historyPanel = null;
      });
    }
  }

  @Test void call_happyPath_dialogShown() {
    CirrusTableView<TableRowModel> table = lookupHistoryTable();

    DisplayButtonClickHandler sut = new DisplayButtonClickHandler();
    ObjectProperty<Theme> themeProperty = new SimpleObjectProperty<>();
    DisplayDialogPane pane = new DisplayDialogPane(table.getColumns(), themeProperty);

    Platform.runLater(() -> sut.call(pane));

    waitForFxThread();
    assertTrue(pane.getScene().getWindow().isShowing());
  }

  @SuppressWarnings("unchecked") //Nodes are not generic so no choice but to cast
  private CirrusTableView<TableRowModel> lookupHistoryTable() {
    Node n = root.lookup(CirrusTableView.class.getSimpleName());
    return (CirrusTableView<TableRowModel>) n;
  }
}

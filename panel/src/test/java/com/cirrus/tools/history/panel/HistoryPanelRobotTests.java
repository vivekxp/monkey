//  Copyright (c) 2020 Cirrus Logic, Inc and
//  Cirrus Logic International Semiconductor Ltd.  All rights reserved.
//
//  This software as well as any related documentation is furnished under
//  license and may only be used or copied in accordance with the terms of the
//  license.  The information in this file is furnished for informational use
//  only, is subject to change without notice, and should not be construed as
//  a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
//  liability for any errors or inaccuracies that may appear in this document
//  or any software that may be provided in association with this document.
//
//  Except as permitted by such license, no part of this document may be
//  reproduced, stored in a retrieval system, or transmitted in any form or by
//  any means without the express written consent of Cirrus Logic.
//
//  Warning
//    This software is specifically written for Cirrus Logic devices.
//    It may not be used with other devices.
//
//! @file   HistoryPanelRobotTests.java
//! @brief

package com.cirrus.tools.history.panel;

import com.cirrus.scs.studiolink.api.notifications.AsyncRequestMessageData;
import com.cirrus.scs.studiolink.api.notifications.INotificationManager;
import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.scs.studiolink.api.notifications.PollingMessageData;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.SecureMessageData;
import com.cirrus.scs.studiolink.api.notifications.UnsolicitedMessageData;
import com.cirrus.tools.controls.CirrusButton;
import com.cirrus.tools.controls.Theme;
import com.cirrus.tools.framework.PanelInfo;
import com.cirrus.tools.history.javafx.HistoryControlSkin;
import com.cirrus.tools.test.CirrusApplicationTest;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class HistoryPanelRobotTests extends CirrusApplicationTest {
  private INotificationManager mockNotificationManager;
  private HistoryPanel sut;

  @BeforeEach
  void beforeEach() {
    mockNotificationManager = mock(INotificationManager.class);
    PanelInfo mockPanelInfo = SetupUtil.newMockPanelInfo(mockNotificationManager);

    interact(() -> {
      sut = new HistoryPanel(mockPanelInfo);
      root.getChildren().setAll(sut.getRoot());
    });
  }

  @AfterEach
  void afterEach() {
    if (sut != null) {
      Platform.runLater(() -> {
        sut.onClose();
        sut = null;
      });
    }
  }

  @Test
  void getRoot_happyPath_notNull() {
    Parent root = sut.getRoot();

    assertNotNull(root);
  }

  @Test
  void displayButton_whenClicked_windowShown() {
    CirrusButton btn = lookupDisplayDialogBtn();

    clickOn(btn);
    waitForFxThread();

    VBox vbox = lookup("#checkboxGroup").queryAs(VBox.class);
    assertTrue(vbox.getScene().getWindow().isShowing());
  }

  @Test
  void onThemeChange_themeNull_exceptionThrown() {
    assertThrows(Exception.class, () -> sut.onThemeChange(null));
  }

  @Test
  void onThemeChange_themeStringIsLight_themePropertySetToLightTheme()
      throws ReflectiveOperationException {
    Theme expected = Theme.LIGHT;

    interact(() -> sut.onThemeChange(expected.getName()));

    Theme actual = getThemePropertyValueByReflection();
    assertEquals(expected, actual);
  }

  @Test
  void onThemeChange_themeStringIsDark_themePropertySetToDarkTheme()
      throws ReflectiveOperationException {
    Theme expected = Theme.DARK;

    interact(() -> sut.onThemeChange(expected.getName()));

    Theme actual = getThemePropertyValueByReflection();
    assertEquals(expected, actual);
  }

  @Test
  void historyPanel_happyPath_isSubscribedToSameTopicsAsSoundclearStudio() {
    /*
     * We consolidate into a single test because each individual test in this
     * class instantiates a JavaFX stage which is quite slow so it makes sense
     * to do it once and then check for topic subscription.,
     */

    List<String> topics = Arrays.asList(
        RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE,
        UnsolicitedMessageData.TOPIC_UNSOLICITED_MESSAGE,
        AsyncRequestMessageData.TOPIC_ASYNC_REQUEST_MESSAGE,
        SecureMessageData.TOPIC_SECURE_MESSAGE,
        PollingMessageData.TOPIC_POLLING_MESSAGE);

    topics.forEach(t ->
        verify(mockNotificationManager)
            .addSubscriber(any(ISubscriber.class), eq(t)));
  }

  private Theme getThemePropertyValueByReflection() throws ReflectiveOperationException {
    Field f = HistoryPanel.class.getDeclaredField("themeProperty");
    f.setAccessible(true);

    @SuppressWarnings("unchecked")  //Generic cast
    ObjectProperty<Theme> prop = (ObjectProperty<Theme>) f.get(sut);

    return prop.getValue();
  }

  private CirrusButton lookupDisplayDialogBtn() {
    return (CirrusButton) sut.getRoot().lookup("#" + HistoryControlSkin.DISPLAY_BUTTON_ID);
  }
}

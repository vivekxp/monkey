/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file SetupUtil.java
 */

package com.cirrus.tools.history.panel;

import com.cirrus.scs.linkclient.events.ILinkClientEvent;
import com.cirrus.scs.linkclient.events.ILinkClientEventHandler;
import com.cirrus.scs.studiolink.api.IStudioLink;
import com.cirrus.scs.studiolink.api.exceptions.StudioLinkException;
import com.cirrus.scs.studiolink.api.notifications.INotificationManager;
import com.cirrus.scs.studiolink.api.utils.Version;
import com.cirrus.tools.framework.Context;
import com.cirrus.tools.framework.PanelInfo;
import com.cirrus.tools.history.panel.preview.HistoryPanelPreview;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Set of utilities encapsulating common code when setting up Panel tests.
 */
public final class SetupUtil {

  private SetupUtil() {
    throw new UnsupportedOperationException("Util class only.");
  }

  /**
   * Create new mock PanelInfo instance with a mocked notification manager.
   */
  public static PanelInfo newMockPanelInfo() {
    return newMockPanelInfo(mock(INotificationManager.class));
  }

  /**
   * Create new mock PanelInfo instance that will return the given NotificationManger from the mock
   * instance of Link within the mock PanelInfo.
   */
  @SuppressWarnings("unchecked")
  public static PanelInfo newMockPanelInfo(INotificationManager notificationManager) {
    IStudioLink mockLink = mock(IStudioLink.class);
    ILinkClientEvent<Boolean> linkNotification = mock(ILinkClientEvent.class);

    doNothing().when(linkNotification).addHandler((ILinkClientEventHandler<Boolean>) any());
    when(mockLink.getNotificationManager()).thenReturn(notificationManager);
    when(mockLink.canConnectToLink()).thenReturn(true);
    when(mockLink.isRunning()).thenReturn(true);
    when(mockLink.getIsRunningStatusChanged()).thenReturn(linkNotification);
    when(mockLink.getClientAPIVersion()).thenReturn(new Version(1, 2, 3));

    try {
      when(mockLink.getStudioLinkVersion()).thenReturn(new Version(3, 2, 1));
    } catch (StudioLinkException e) {
      throw new IllegalStateException("Could not mock getStudioLinkVersion.", e);
    }

    Context mockContext = mock(Context.class);
    when(mockContext.getLink()).thenReturn(mockLink);

    PanelInfo mockPanelInfo = mock(PanelInfo.class);
    when(mockPanelInfo.getName()).thenReturn(HistoryPanelPreview.PANEL_NAME);
    when(mockPanelInfo.getContext()).thenReturn(mockContext);

    return mockPanelInfo;
  }
}

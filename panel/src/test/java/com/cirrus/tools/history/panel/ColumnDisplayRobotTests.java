/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ColumnDisplayRobotTests.java
 */
package com.cirrus.tools.history.panel;

import com.cirrus.tools.controls.CirrusButton;
import com.cirrus.tools.controls.CirrusCheckBox;
import com.cirrus.tools.framework.PanelInfo;
import com.cirrus.tools.history.javafx.HistoryControlSkin;
import com.cirrus.tools.history.javafx.table.definition.ColumnDefinitions;
import com.cirrus.tools.test.CirrusApplicationTest;
import com.sun.javafx.scene.control.skin.TableColumnHeader;
import javafx.application.Platform;
import javafx.scene.control.TableColumnBase;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ColumnDisplayRobotTests extends CirrusApplicationTest {
  private HistoryPanel sut;

  @BeforeEach
  void beforeEach() {
    PanelInfo mockPanelInfo = SetupUtil.newMockPanelInfo();
    sut = new HistoryPanel(mockPanelInfo);

    interact(() -> root.getChildren().setAll(sut.getRoot()));
  }

  @AfterEach
  void afterEach() {
    if (sut != null) {
      Platform.runLater(() -> {
        sut.onClose();
        sut = null;
      });
    }
  }

  @Test @Disabled("Tests fail in headless mode only: https://tracker.cirrus.com/browse/SCSSTU-1218")
  void clickTimestampCheckbox_whenChecked_columnIsHidden() {
    //Arrange
    TableColumnBase<?, ?> col = lookupTimestampColumn();
    assertTrue(col.isVisible(), "Column did not start visible.");

    openDialog();

    CirrusCheckBox cbx = lookupTimestampCheckBox();
    assertTrue(cbx.getSelected(), "Checkbox did not start checked.");

    //Act
    clickOn(cbx);

    //Assert
    assertFalse(col.isVisible(), "Column still visible.");
  }

  @Test @Disabled("Tests fail in headless mode only: https://tracker.cirrus.com/browse/SCSSTU-1218")
  void clickTimestampCheckbox_whenUnchecked_columnIsShown() {
    //Arrange
    TableColumnBase<?, ?> col = lookupTimestampColumn();
    setColumnInvisible(col);
    assertFalse(col.isVisible(), "Column did not start visible.");

    openDialog();

    CirrusCheckBox cbx = lookupTimestampCheckBox();
    assertFalse(cbx.getSelected(), "Checkbox did not start unchecked.");

    //Act
    clickOn(cbx);

    //Assert
    assertTrue(col.isVisible(), "Column still not shown.");
  }

  private void setColumnInvisible(TableColumnBase<?, ?> col) {
    Platform.runLater(() -> col.setVisible(false));
    waitForFxThread();
  }

  private void openDialog() {
    CirrusButton btn = lookupDisplayDialogBtn();

    clickOn(btn);
    waitForFxThread();
  }

  private CirrusCheckBox lookupTimestampCheckBox() {
    return lookup("#Timestamp").queryAs(CirrusCheckBox.class);
  }

  private CirrusButton lookupDisplayDialogBtn() {
    return (CirrusButton) sut.getRoot().lookup("#" + HistoryControlSkin.DISPLAY_BUTTON_ID);
  }

  private TableColumnBase<?, ?> lookupTimestampColumn() {
    TableColumnHeader tc = lookup("#" + ColumnDefinitions.TIMESTAMP_COLUMN_ID).queryAs(TableColumnHeader.class);
    return tc.getTableColumn();
  }
}

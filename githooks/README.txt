To install the hooks in this folder:

- Copy files to the Git hooks folder: .git/hooks/
  -- Note that you may already have commit-msg hook installed.
 
- Ensure hook files are executable

The commit-msg hook can automatically be installed whilst cloning as follows:
git clone "ssh://username@aus-grt-m-01.ad.cirrus.com:29418/cirrus/tools/history" && (cd "history" && gitdir=$(git rev-parse --git-dir); curl -s -k -o ${gitdir}/hooks/commit-msg https://aus-grt-m-01.ad.cirrus.com/r/static/commit-msg ; chmod +x ${gitdir}/hooks/commit-msg)


/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file Jenkinsfile
 */

library 'scs_ci'

GRADLE_CMD_WIN = "gradlew.bat"
JAVA_11_WIN_RELATIVE_EXE = "jdk-11.0.4+11-jre-with-javafx\\bin\\java.exe"  //Relative to workspace
JAVA_11_WIN_LOCATION = "java/11/OpenJDK11U-jre-with-javafx_x64_windows_hotspot_11.0.4_11.zip"
SCS_REPO_PATH = "Aus_Generic_Dev_Local/scs_repo"
ARTIFACTORY_SERVER_NAME = 'scs'

/*
 * The artifactory_api_key is a username/API Key combination for the account srv_scs_test
 * See https://aus-jnkml-tls02.ad.cirrus.com/credentials/ and
 * https://www.jfrog.com/confluence/display/RTF/Artifactory+REST+API#ArtifactoryRESTAPI-Authentication
 */
ARTIFACTORY_CREDENTIALS_ID = "artifactory_api_key"

pipeline {
  agent none
  options {
    timestamps()
    skipDefaultCheckout()
  }

  stages {
    stage('Build & Measure') {
      parallel {
        stage('Build, Unit Test & SonarQube') {
          agent { label "WINB2" }
          options { timeout(time: 15, unit: 'MINUTES') }
          environment {
            JAVA_HOME = "C:\\Program Files\\Java\\jdk1.8.0_161"
            GRADLE = "${GRADLE_CMD_WIN}"
            GRADLE_OPTS = "-Dorg.gradle.daemon=false"
          }
          steps {
            fetchCode()
            buildAndTest()
            buildDemoBsp()
            sonarQube()
          }
          post {
            always {
              archiveDemoBsp()
            }
          }
        }

        stage("KPI Tests with Java11") {
          when {
            expression {params.GERRIT_EVENT_TYPE == 'change-merged' || params.PUBLISH_RELEASE }
          }
          agent { label "WINB2" }
          options { timeout(time: 30, unit: 'MINUTES') }
          environment {
            GRADLE = "${GRADLE_CMD_WIN}"
            J11_EXE = "%WORKSPACE%\\${JAVA_11_WIN_RELATIVE_EXE}"
            GRADLE_OPTS = "-Dorg.gradle.daemon=false"
          }
          steps {
            fetchCode()
            script { downloadJdk11() }
            runKpiTests()
          }
        }
      }
    }

    stage("Publish Artifacts") {
      agent { label "WINB2" }
      options { timeout(time: 10, unit: 'MINUTES') }
      environment {
        JAVA_HOME = "C:\\Program Files\\Java\\jdk1.8.0_161"
        GRADLE = "${GRADLE_CMD_WIN}"
        GRADLE_OPTS = "-Dorg.gradle.daemon=false"
      }
      when {
        expression {params.GERRIT_EVENT_TYPE == 'change-merged' || params.PUBLISH_RELEASE }
      }
      steps {
        fetchCode()
        publishJars()
      }
    }
  }
}

def fetchCode() {
    deleteDir()
    checkout scm
}

def buildAndTest() {
  echo "Running Gradle build (excludes KPI tests)."

  bat '''call %GRADLE% clean'''
  bat '''call %GRADLE% build'''

  junit(
          testResults: '**/TEST-*.xml',
          allowEmptyResults: true,
          healthScaleFactor: 1.0,
          keepLongStdio: true
  )
  jacoco(
          execPattern: '**/**.exec',
          classPattern: '**/classes',
          sourcePattern: '**/src',
          exclusionPattern: '**/*Test*.class'
  )
}

def publishJars() {
  echo "Running Gradle jar on History components."

  bat '''call %GRADLE% -p history clean'''
  bat '''call %GRADLE% -p history jar'''

  withCredentials([usernamePassword(
          credentialsId: ARTIFACTORY_CREDENTIALS_ID,
          usernameVariable: 'ARTIFACTORY_USERNAME',
          passwordVariable: 'ARTIFACTORY_API_KEY')]) {
      bat "call %GRADLE% -p history artifactoryPublish -PpublishUsername=${ARTIFACTORY_USERNAME} -PpublishPassword=${ARTIFACTORY_API_KEY} --info"
  }
}

def runKpiTests() {
  echo "Running KPI tests with Java 11."

  bat '''call %GRADLE% clean'''
  bat '''call powershell ".\\%GRADLE% -p history :history-kpi-tests:test --info -Pjre11exe=\"%J11_EXE%\" | tee .\\console.txt"'''
  bat '''call powershell -File .\\history\\history-kpi-tests\\kpi_duration_console_to_csv.ps1 -input_path ".\\console.txt" -output_path ".\\kpi_duration.csv"'''
  bat '''call powershell -File .\\history\\history-kpi-tests\\kpi_heap_console_to_csv.ps1 -input_path ".\\console.txt" -output_path ".\\kpi_heap.csv"'''
  archiveArtifacts artifacts: 'kpi_heap.csv', fingerprint: true
  archiveArtifacts artifacts: 'kpi_duration.csv', fingerprint: true
  junit(
    testResults: '**/TEST-*.xml',
    allowEmptyResults: true,
    healthScaleFactor: 1.0,
    keepLongStdio: true
  )
}

def sonarQube() {
  timeout(10) {
    script {
      withSonarQubeEnv('SonarQube Austin') {
        def sonarParams = getSonarParams(" -Dsonar.ws.timeout=240")
        echo "Running SonarQube with parameters: ${sonarParams}."
        bat "call %GRADLE% sonarqube ${sonarParams} -Dorg.gradle.jvmargs=\"-XX:MaxMetaspaceSize=1024m -Xmx2048m\""
      }
    }
  }
}

def buildDemoBsp() {
  echo "Building demo BSP."
  bat '''call %GRADLE% publishHistoryToMavenLocal'''
  bat '''call %GRADLE% -p bsp-builder build'''
}

def archiveDemoBsp() {
  echo "Archiving demo BSP."
  archiveArtifacts artifacts: 'bsp-builder/Output/*', fingerprint: true
}

def downloadJdk11() {
  server = Artifactory.server "$ARTIFACTORY_SERVER_NAME"

  jdk11Spec = """{
    "files": [
        {
            "pattern": "$SCS_REPO_PATH/$JAVA_11_WIN_LOCATION",
            "target": "",
            "flat": "true",
            "explode": "true"
        }
    ]
}"""

  server.download(jdk11Spec) //to %WORKSPACE%/%JDK_DIR%
}

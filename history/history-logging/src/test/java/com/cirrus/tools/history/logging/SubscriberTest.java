/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file SubscriberTest.java
 */

package com.cirrus.tools.history.logging;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.scs.studiolink.api.notifications.TopicNotification;
import com.cirrus.tools.history.api.TimestampedNotification;
import com.cirrus.tools.history.api.TopicConsumer;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SubscriberTest {

  /**
   * Copied from Subscriber. Behaviour should be identical since original behaviour is simply
   * decorated.
   */
  @Test
  public void onMessageReceived_happyPath_messageAddedToConsumer() {
    TopicConsumer mockConsumer = mock(TopicConsumer.class);
    ISubscriber sut = new Subscriber(new com.cirrus.tools.history.api.Subscriber(mockConsumer));
    TopicNotification tn = new TopicNotification("topic", new Object());

    sut.onMessageReceived(tn);

    verify(mockConsumer).add(any(TimestampedNotification.class));
  }

  @Test
  //This method is designed for, and cannot work without catching RuntimeException
  @SuppressWarnings({"PMD.AvoidCatchingGenericException", "checkstyle:IllegalCatch"})
  public void onMessageReceived_exceptionThrown_exceptionLogged() {
    //Given
    String topic = "test topic";
    String msg = "message";

    //Arrange
    ListAppender<ILoggingEvent> appender = prepareLogger();
    TopicConsumer mockConsumer = mock(TopicConsumer.class);
    ISubscriber sut = new Subscriber(new com.cirrus.tools.history.api.Subscriber(mockConsumer));

    doThrow(new RuntimeException())
        .when(mockConsumer).add(any(TimestampedNotification.class));

    try {
      //Act
      sut.onMessageReceived(new TopicNotification(topic, msg));
      fail("Expected exception not thrown.");
    } catch (RuntimeException e) {
      //Assert
      String expected = String.format(Subscriber.ERROR_FORMAT, topic, msg);
      String actual = appender.list.get(0).getMessage();
      assertEquals(expected, actual);
    }
  }

  private ListAppender<ILoggingEvent> prepareLogger() {
    Logger sutLogger = (Logger) LoggerFactory.getLogger(Subscriber.class);
    sutLogger.setLevel(Level.ERROR);

    ListAppender<ILoggingEvent> appender = new ListAppender<>();
    appender.start();

    sutLogger.addAppender(appender);
    return appender;
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file Subscriber.java
 */
package com.cirrus.tools.history.logging;

import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.scs.studiolink.api.notifications.TopicNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class Subscriber implements ISubscriber {
  static final String ERROR_FORMAT = "Failed to handle notification: Topic: [%s], Message: [%s].";

  private static final Logger LOGGER = LoggerFactory.getLogger(Subscriber.class);

  private final ISubscriber subscriberReference;

  public Subscriber(ISubscriber subscriber) {
    this.subscriberReference = Objects.requireNonNull(subscriber);
  }

  @Override
  public String getName() {
    return subscriberReference.getName();
  }

  @Override
  public void setName(String subscriberName) {
    subscriberReference.setName(subscriberName);
  }

  @Override
  //This method is designed for, and cannot work without catching RuntimeException
  @SuppressWarnings({"PMD.AvoidCatchingGenericException", "checkstyle:IllegalCatch"})
  public void onMessageReceived(TopicNotification message) {
    try {
      subscriberReference.onMessageReceived(message);
    } catch (RuntimeException e) {
      if (LOGGER.isErrorEnabled()) {  //For performance
        LOGGER.error(toErrorMsg(message), e);
      }
      throw e;
    }
  }

  private String toErrorMsg(TopicNotification message) {
    String msg = message.getMessage() == null ? "NULL" : message.getMessage().toString();
    return String.format(ERROR_FORMAT, message.getTopicURI(), msg);
  }
}

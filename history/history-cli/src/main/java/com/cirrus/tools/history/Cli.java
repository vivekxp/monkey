/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file Cli.java
 */

package com.cirrus.tools.history;

import com.cirrus.scs.linkclient.StudioLink;
import com.cirrus.scs.studiolink.api.exceptions.StudioLinkException;
import com.cirrus.scs.studiolink.api.notifications.AsyncRequestMessageData;
import com.cirrus.scs.studiolink.api.notifications.CLSICDeviceResponseMessageData;
import com.cirrus.scs.studiolink.api.notifications.CoreBankLockStatusChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.CoreFirmwareChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.CoreMemoryViolationOccurredMessageData;
import com.cirrus.scs.studiolink.api.notifications.CoreWatchdogTimeoutMessageData;
import com.cirrus.scs.studiolink.api.notifications.DeviceChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.DeviceLibraryChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.INotificationManager;
import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.scs.studiolink.api.notifications.LinkChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.PluginManagerMessageData;
import com.cirrus.scs.studiolink.api.notifications.PollingMessageData;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.SecureMessageData;
import com.cirrus.scs.studiolink.api.notifications.SystemChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.SystemHardwareIdChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.SystemLibraryChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.SystemRenameMessageData;
import com.cirrus.scs.studiolink.api.notifications.UnsolicitedMessageData;
import com.cirrus.tools.history.api.BaseController;
import com.cirrus.tools.history.api.Controller;
import com.cirrus.tools.history.api.Subscriber;
import com.cirrus.tools.history.api.TopicConsumer;
import com.cirrus.tools.history.impl.LinkVersionChecker;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Link will be started if not already running.
 */
@SuppressWarnings("PMD.SystemPrintln")  //This class designed for console output
public final class Cli {

  private Cli() {
    throw new UnsupportedOperationException();
  }

  /**
   * Link will be started if not already running.
   */
  public static void main(String[] args) throws StudioLinkException, InterruptedException {
    printHelp();

    StudioLink link = new StudioLink();
    link.start();
    new LinkVersionChecker().verify(link);

    INotificationManager nm = link.getNotificationManager();
    Controller controller = new BaseController(nm);

    TopicConsumer consumer = newConsumer();
    ISubscriber subscriber = new Subscriber(consumer);
    controller.subscribeToTopics(subscriber, allKnownTopicNames());
    System.out.println("Subscribed to all topics.");

    while (true) {
      Thread.sleep(Long.MAX_VALUE);
    }
  }

  private static TopicConsumer newConsumer() {

    return topicNotification -> {
      RecursiveToStringStyle style = new RecursiveToStringStyle();
      String msg = new ReflectionToStringBuilder(topicNotification, style).toString();

      System.out.println(msg);
      System.out.println();
    };
  }

  private static void printHelp() {
    System.out.println("===============");
    System.out.println("Ctrl c TO EXIT.");
    System.out.println("===============");
    System.out.println();
  }

  /**
   * Known topics from Link API client v2.15.1.
   * This is a temporary measure.  If we really want to provide a facility for
   * subscribing to all topics then the set of topics should be injected.
   */
  private static Set<String> allKnownTopicNames() {
    Set<String> t = new HashSet<>();

    t.add(AsyncRequestMessageData.TOPIC_ASYNC_REQUEST_MESSAGE);
    t.add(CLSICDeviceResponseMessageData.TOPIC_CLSIC_DEVICE_RESPONSE);
    t.add(CoreBankLockStatusChangeMessageData.TOPIC_CORE_BNAK_LOCK_STATUS_CHANGE);
    t.add(CoreFirmwareChangeMessageData.TOPIC_CORE_FIRMWARE_CHANGE);
    t.add(CoreMemoryViolationOccurredMessageData.TOPIC_CORE_VIOLATION_OCCURRED);
    t.add(CoreWatchdogTimeoutMessageData.TOPIC_CORE_WATCHDOG_TIMEOUT);
    t.add(DeviceChangeMessageData.TOPIC_DEVICE_CHANGE);
    t.add(DeviceLibraryChangeMessageData.TOPIC_DEVICE_LIB_CHANGE);
    t.add(LinkChangeMessageData.TOPIC_LINK_CHANGE);
    t.add(PluginManagerMessageData.TOPIC_PLUGIN_MANAGER_MESSAGE);
    t.add(PollingMessageData.TOPIC_POLLING_MESSAGE);
    t.add(RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE);
    t.add(SecureMessageData.TOPIC_SECURE_MESSAGE);
    t.add(SystemChangeMessageData.TOPIC_SYSTEM_CHANGE);
    t.add(SystemHardwareIdChangeMessageData.TOPIC_SYSTEM_HARDWARE_ID_CHANGE);
    t.add(SystemLibraryChangeMessageData.TOPIC_SYSTEM_LIB_CHANGE);
    t.add(SystemRenameMessageData.TOPIC_SYSTEM_RENAME);
    t.add(UnsolicitedMessageData.TOPIC_UNSOLICITED_MESSAGE);

    return Collections.unmodifiableSet(t);
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file DisplayOrderIntegrationTests.java
 */

package com.cirrus.tools.history.integration;

import com.cirrus.scs.studiolink.api.notifications.AsyncRequestMessageData;
import com.cirrus.scs.studiolink.api.notifications.AsyncRequestMessageData.AsyncRequestEvent;
import com.cirrus.scs.studiolink.api.notifications.TopicNotification;
import com.cirrus.tools.history.impl.table.TableRowModel;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Tests the messages are displayed in the order they where received
 */
public class DisplayOrderIntegrationTests extends BaseIntegrationTest {

  private static final String FIRST = "The frog";
  private static final String SECOND = "Silent sky";
  private static final String THIRD = "Adorable giant";

  private final TopicNotification first = createTestNotification(FIRST);
  private final TopicNotification second = createTestNotification(SECOND);
  private final TopicNotification third = createTestNotification(THIRD);

  @Test
  public void onMessageReceived_byDefault_itemsShouldBeInAscendingOrder() {
    //act
    getSubscriber().onMessageReceived(first);
    getSubscriber().onMessageReceived(second);
    getSubscriber().onMessageReceived(third);
    waitForFxThread();

    //assert
    verifySortedInAscending();
  }

  @Test
  public void onMessageReceived_itemsAddedToANonEmptyList_itemsShouldBeInAscendingOrder() {
    //arrange
    getSubscriber().onMessageReceived(first); // one element already exists

    //act
    Platform.runLater(() -> {
          //then: add two entries
          getSubscriber().onMessageReceived(second);
          getSubscriber().onMessageReceived(third);
        }
    );
    waitForFxThread();

    //assert
    verifySortedInAscending();
  }


  private TopicNotification createTestNotification(String deviceName) {
    AsyncRequestMessageData msg = new AsyncRequestMessageData(AsyncRequestEvent.DEVICE_REFRESH,
        true, "sysName", deviceName, "info");
    return new TopicNotification(AsyncRequestMessageData.TOPIC_ASYNC_REQUEST_MESSAGE, msg);
  }

  private void verifySortedInAscending() {
    ObservableList<TableRowModel> items = getHistoryTable().getItems();
    assertTrue("Items not ordered in ascending",
        items.get(0).getDeviceName().equals(FIRST)
        && items.get(1).getDeviceName().equals(SECOND)
        && items.get(2).getDeviceName().equals(THIRD));

  }

}

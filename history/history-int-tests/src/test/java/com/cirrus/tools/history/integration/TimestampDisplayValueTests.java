/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TimestampDisplayValueTests.java
 */
package com.cirrus.tools.history.integration;

import com.cirrus.tools.history.impl.table.TableRowModel;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * These tests verify that even though the timestamp column is bound to the elapsed time in
 * nanoseconds, it is the timestamp value that is displayed.
 */
public class TimestampDisplayValueTests extends BaseIntegrationTest {

  @Test
  public void getCellData_itemHasElapsedTime_elapsedTimeIsReturned() {
    //Given
    Long elapsedTime = 100L;

    //Arrange
    TableRowModel row = mock(TableRowModel.class);
    when(row.getElapsedTime()).thenReturn(elapsedTime);

    getDataList().addRowToList(row);
    waitForFxThread();

    //Act
    Long actual = lookupTimestampColumn().getCellData(row);

    //Arrange
    assertEquals(elapsedTime, actual);
  }
}

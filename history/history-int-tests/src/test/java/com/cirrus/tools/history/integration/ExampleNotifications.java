/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ExampleNotifications.java
 */

package com.cirrus.tools.history.integration;

import com.cirrus.scs.studiolink.api.enums.ControlInterface.ControlInterfaceValues;
import com.cirrus.scs.studiolink.api.notifications.AsyncRequestMessageData;
import com.cirrus.scs.studiolink.api.notifications.AsyncRequestMessageData.AsyncRequestEvent;
import com.cirrus.scs.studiolink.api.notifications.PollingMessageData;
import com.cirrus.scs.studiolink.api.notifications.SecureMessageData;
import com.cirrus.scs.studiolink.api.notifications.SecureMessageData.SecureMessageType;
import com.cirrus.scs.studiolink.api.notifications.TopicNotification;
import com.cirrus.scs.studiolink.api.notifications.UnsolicitedMessageData;

/**
 * Test helper to supply prebuilt non-mocked Link notifications.
 */
public final class ExampleNotifications {

  private ExampleNotifications() {
    throw new UnsupportedOperationException("Util class only.");
  }

  /**
   * Still to see a genuine example so not sure how realistic this data is.
   */
  public static TopicNotification newUnsolicitedMessage() {
    String topicUri = UnsolicitedMessageData.TOPIC_UNSOLICITED_MESSAGE;

    UnsolicitedMessageData msg = new UnsolicitedMessageData(new byte[] {1, 2, 3, 4});

    return new TopicNotification(topicUri, msg);
  }

  /**
   * Real notification data received after refreshing device in SCS.
   */
  public static TopicNotification newAsyncRequestMessage() {
    String topicUri = AsyncRequestMessageData.TOPIC_ASYNC_REQUEST_MESSAGE;

    AsyncRequestMessageData msg = new AsyncRequestMessageData(
        AsyncRequestEvent.DEVICE_REFRESH,
        true,                               //succeeded,
        "Virtual CDB48LV40-M-1 Mini Board", //systemName
        "CS48LV40F",                        //deviceName,
        "",                                 //additionalInfo
        1,                                  //deviceAddress
        ControlInterfaceValues.CTRL_SPI,    //controlInterfaceType
        false                               //isPaged
    );

    return new TopicNotification(topicUri, msg);
  }

  /**
   * Still to see a genuine example so not sure how realistic this data is.
   */
  public static TopicNotification newPollingMessage() {
    String topicUri = PollingMessageData.TOPIC_POLLING_MESSAGE;

    PollingMessageData msg = new PollingMessageData(
        "Virtual CDB48LV40-M-1 Mini Board", //systemName
        "CS48LV40F",                        //deviceName
        3,                                  //attempts
        4,                                  //maxAttempts
        "GPIO1_CTRL1",                      //messageInterruptRegister
        3800                                //registerAddress
    );

    return new TopicNotification(topicUri, msg);
  }

  /**
   * Still to see a genuine example so not sure how realistic this data is.
   */
  public static TopicNotification newSecureMessage() {
    String topicUri = SecureMessageData.TOPIC_SECURE_MESSAGE;

    SecureMessageData msg = new SecureMessageData(
        "Virtual CDB48LV40-M-1 Mini Board",         //systemName
        "CS48LV40F",                                //deviceName
        "GPIO1_CTRL1",                              //registerName
        3800,                                       //registerAddress
        SecureMessageType.NOTIFICATION.toString(),  //messageTypeName
        20,                                         //messageId
        35,                                         //serviceInstance
        40,                                         //serviceType
        "result",                                   //result
        0,                                          //status
        null,                                       //messageData see - //https://tracker.cirrus.com/browse/SCSSTU-972
        false                                       //isBulk
    );

    return new TopicNotification(topicUri, msg);
  }
}

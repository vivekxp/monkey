/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file BaseIntegrationTest.java
 */
package com.cirrus.tools.history.integration;

import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.tools.controls.CirrusTableView;
import com.cirrus.tools.history.impl.HistoryDataList;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.javafx.HistoryDataListJavaFX;

import com.cirrus.tools.history.javafx.table.definition.ColumnDefinitions;
import com.cirrus.tools.test.CirrusApplicationTest;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import org.junit.After;
import org.junit.Before;

import java.lang.reflect.Field;

import static org.junit.Assert.assertTrue;

public class BaseIntegrationTest extends CirrusApplicationTest {

  private ISubscriber subscriber;
  private CirrusTableView<TableRowModel> historyTable;
  private IntegrationTestController intController;

  @Before
  public void initHistory() {
    interact(() -> {
      intController = new IntegrationTestController();
      root.getChildren().setAll(intController.getHistoryControl());
    });

    historyTable = lookupHistoryTable();
    subscriber = lookupSubscriberViaReflection();
    waitForFxThread();

    assertTrue("Table did not start empty.", historyTable.getItems().isEmpty());
  }

  @After
  public void shutdownController() throws InterruptedException {
    if (intController != null) {
      intController.shutdown();
      intController = null;
    }
  }

  @After
  public void clearTable() {
    if (intController != null) {
      intController.clearDataList();
    }
  }

  protected ISubscriber getSubscriber() {
    return subscriber;
  }

  protected HistoryDataListJavaFX getDataList() {
    return intController.getDataList();
  }

  protected CirrusTableView<TableRowModel> getHistoryTable() {
    return historyTable;
  }

  protected ObservableList<TableRowModel> getHistoryTableItems() {
    return historyTable.getItems();
  }

  @SuppressWarnings("unchecked") //Nodes are not generic so no choice but to cast
  private CirrusTableView<TableRowModel> lookupHistoryTable() {
    Node n = root.lookup(CirrusTableView.class.getSimpleName());
    return (CirrusTableView<TableRowModel>) n;
  }

  private ISubscriber lookupSubscriberViaReflection() {
    HistoryDataListJavaFX dataList = getDataList();
    return lookupSubscriberViaReflection(dataList.getHistoryDataList());
  }

  private ISubscriber lookupSubscriberViaReflection(HistoryDataList dataList) {
    try {
      Field field = dataList.getClass().getDeclaredField("subscriber");
      field.setAccessible(true);

      ISubscriber sub = (ISubscriber) field.get(dataList);

      if (sub == null) {
        throw new IllegalStateException("Subscriber is null.");
      }

      return sub;
    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new IllegalStateException("Failed to lookup subscriber.", e);
    }
  }

  @SuppressWarnings("unchecked")  //Generic cast
  protected TableColumn<TableRowModel, Long> lookupTimestampColumn() {
    return (TableColumn<TableRowModel, Long>) historyTable.getColumns()
        .filtered(c -> ColumnDefinitions.TIMESTAMP_COLUMN_ID.equalsIgnoreCase(c.getId())).get(0);
  }
}

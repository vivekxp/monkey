/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ColumnWidthTests.java
 */
package com.cirrus.tools.history.integration.preferences;

import com.cirrus.tools.controls.CirrusTableView;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.integration.IntegrationTestController;
import com.cirrus.tools.history.javafx.HistoryControl;
import com.cirrus.tools.history.javafx.Preferences;
import com.cirrus.tools.history.javafx.table.ColumnPreferences;

import com.cirrus.tools.history.javafx.table.definition.ColumnDefinition;
import com.cirrus.tools.history.javafx.table.definition.ColumnDefinitions;
import com.cirrus.tools.test.CirrusApplicationTest;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ColumnWidthTests extends CirrusApplicationTest {
  /*
   * We inherit from CirrusApplicationTest rather than BaseIntegrationTest because each test below
   * needs to construct HistoryControl differently.  BaseIntegrationTest only allows a single
   * configuration of HistoryControl.
   */

  private static final double DELTA = 0.01;

  private final List<ColumnDefinition> columnDefinitions = new ColumnDefinitions().getColumns();

  @Test
  public void whenAllPreferredWidthsAreSet_allColumnsStartWithThatPreferredWidthValue() {
    //Given
    double expected = 200;  //Needs to be bigger than column minWidth

    //Arrange
    Preferences preferences = getNewPreferencesWithAllPreferredWidthsTo(expected);

    //Act
    initHistoryControl(preferences);

    //Assert
    getHistoryTable().getColumns().forEach(col -> {
      double actual = col.getPrefWidth();
      String failMsg = "Incorrect width for" + col.getText();
      assertEquals(failMsg, expected, actual, DELTA);
    });
  }

  @Test
  public void whenChangingColumnPreferredWidth_preferencesPreferredWidthIsUpdated() {
    //Arrange
    HistoryControl historyControl = initHistoryControl();
    Map<String, ColumnPreferences> columnPreferences = historyControl.getPreferences().getColumnPreferences();

    TableColumn<TableRowModel, ?> firstColumn = getHistoryTable().getColumns().get(0);
    String firstColumnId = firstColumn.getId();

    double preferenceValue = columnPreferences.get(firstColumnId).preferredWidthProperty().get();
    double expected = preferenceValue + 100;

    //Act
    Platform.runLater(() -> firstColumn.prefWidthProperty().setValue(expected));
    waitForFxThread();

    //Assert
    preferenceValue = columnPreferences.get(firstColumnId).preferredWidthProperty().get();
    assertEquals(expected, preferenceValue, DELTA);
  }

  private HistoryControl initHistoryControl() {
    return initHistoryControl(new Preferences());
  }

  private HistoryControl initHistoryControl(Preferences preferences) {
    IntegrationTestController intController = new IntegrationTestController(preferences);
    interact(() -> root.getChildren().setAll(intController.getHistoryControl()));
    waitForFxThread();

    return intController.getHistoryControl();
  }

  @SuppressWarnings("unchecked") //Nodes are not generic so no choice but to cast
  private CirrusTableView<TableRowModel> getHistoryTable() {
    Node n = root.lookup(CirrusTableView.class.getSimpleName());
    return (CirrusTableView<TableRowModel>) n;
  }

  private Preferences getNewPreferencesWithAllPreferredWidthsTo(double width) {
    Preferences preferences = new Preferences();
    setAllPreferredWidthsTo(width, preferences);
    return preferences;
  }

  private void setAllPreferredWidthsTo(double width, Preferences preferences) {
    Map<String, ColumnPreferences> cp = preferences.getColumnPreferences();
    columnDefinitions.forEach(colDef -> cp.get(colDef.getId()).preferredWidthProperty().set(width));
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file AutoScrollEnableDisableTests.java
 */
package com.cirrus.tools.history.integration.autoscroll;

import com.cirrus.tools.history.integration.BaseIntegrationTest;
import com.cirrus.tools.history.integration.autoscroll.helpers.ScrollTestHelper;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests enable/disable state of autoscroll feature
 */
public class AutoScrollEnableDisableTests extends BaseIntegrationTest {

  @Test
  public void autoscroll_byDefault_shouldBeEnabled() {
    //Given
    ScrollTestHelper helper = new ScrollTestHelper(getHistoryTable(), getDataList());
    helper.addBunchOfRows();

    //Verify
    assertTrue(helper.isAtBottom());
  }

  @Test
  public void autoscroll_whenDisabled_shouldNotAutoScroll() {
    //Given
    ScrollTestHelper helper = new ScrollTestHelper(getHistoryTable(), getDataList());
    helper.addBunchOfRows()
        .disableAutoScroll()
        .scrollToSomewhereInMiddle();

    //Act
    helper.addBunchOfRows();

    //Verify
    assertFalse(helper.isAtBottom());
  }

  @Test
  public void autoscroll_whenEnabledAgain_shouldResumeAutoScroll() {
    //Given
    ScrollTestHelper helper = new ScrollTestHelper(getHistoryTable(), getDataList());
    helper.addBunchOfRows()
        .disableAutoScroll() //disable
        .scrollToSomewhereInMiddle()
        .enableAutoScroll(); //re-enable

    //Act
    helper.addBunchOfRows();

    //Verify
    assertTrue(helper.isAtBottom());
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TableRowFieldPopulationTests.java
 */

package com.cirrus.tools.history.integration;

import com.cirrus.scs.studiolink.api.enums.ControlInterface.ControlInterfaceValues;
import com.cirrus.scs.studiolink.api.notifications.AsyncRequestMessageData;
import com.cirrus.scs.studiolink.api.notifications.PollingMessageData;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement.ElementType;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData.RegionEvent;
import com.cirrus.scs.studiolink.api.notifications.SecureMessageData;
import com.cirrus.scs.studiolink.api.notifications.TopicNotification;
import com.cirrus.tools.history.impl.table.TableRowModel;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * These tests verify that the actual data displayed in each field is correct.
 *
 * This is pretty much a full stack integration with the exception of a real Link instance.
 *
 * A separate test class (SubscriberUiIntegrationTests) verifies that the correct number of rows
 * is added for each message type.
 */
@SuppressWarnings("PMD.TooManyMethods") //Shouldn't apply to test classes
public class TableRowFieldPopulationTests extends BaseIntegrationTest {
  /*
   * For these tests we deliberately duplicate the values in TableRowFormatter rather than
   * reference them.  If we referenced the existing constants, the tests would pass even if there
   * was an error in the existing constants.
   */
  private static final String NONE = "N/A";
  private static final String MULTIPLE_ADDRESSES = "(...)";
  private static final String MULTIPLE_DATA = " (...)";
  private static final String ADDRESS_HEX_FORMAT = "0x%02x";
  private static final String DATA_HEX_FORMAT = "0x%08x";
  private static final String PAGE_HEX_FORMAT = "0x%02x";

  private static final String TIMESTAMP_REGEX =
      "\\d{4}-\\d{2}-\\d{2}_\\d{2}:\\d{2}:\\d{2}\\.\\d{3}"; //2020-05-21_15:40:10.019

  private final Verify verify = new Verify();

  @Test
  public void onMessageReceived_readEvent_allFieldsFormatted() {
    //Arrange
    TopicNotification tn = ExampleRegionChangeNotifications.newOnChangeReadMessage();
    RegionChangeMessageData msg = (RegionChangeMessageData) tn.getMessage();
    RegionChangeElement firstElement = msg.getChangedElements().get(0);

    //Act
    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    //Assert
    verifyTableRowCount();
    TableRowModel actual = getHistoryTableItems().get(0);

    //Comments to the right are the actual observed values
    verify.address(firstElement.getElementAddress(), actual.getAddress());                 //0x07
    verify.data(firstElement.getNewValue(), actual.getData());                             //0x00000000
    verify.notApplicable(actual.getPage());                                                //N/A
    verify.busProtocol(msg.getControlInterface(), actual.getBusProtocol());                //CTRL_I2C
    verify.deviceAddress(msg.getAddress(), actual.getDeviceAddress());                     //0xe8
    verify.systemName(msg.getSystemName(), actual.getSystemName());                        //Virtual CS40L60 A1 CDB
    verify.deviceName(msg.getDeviceName(), actual.getDeviceName());                        //CS40L60 Mode 0
    verify.operation(RegionEvent.READ, actual.getOperation());                             //R
    verify.regOrFieldName(firstElement.getElementName(), actual.getRegisterOrFieldName()); //ENABLE_CTRL1
    verify.elementType(firstElement.getElementType(), actual.getType());                   //REGISTER
    verify.timestamp(actual.getTimestamp());
  }

  @Test
  public void onMessageReceived_pagedReadEvent_allFieldsFormatted() {
    //Arrange
    TopicNotification tn = ExampleRegionChangeNotifications.newOnChangeReadPagedMessage();
    RegionChangeMessageData msg = (RegionChangeMessageData) tn.getMessage();
    RegionChangeElement firstElement = msg.getChangedElements().get(0);

    //Act
    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    //Assert
    verifyTableRowCount();
    TableRowModel actual = getHistoryTableItems().get(0);

    //Comments to the right are the actual observed values
    verify.address(firstElement.getElementAddress(), actual.getAddress());                 //0x09
    verify.data(firstElement.getNewValue(), actual.getData());                             //0x00000002
    verify.page(firstElement.getElementPage(), actual.getPage());                          //0x10
    verify.busProtocol(msg.getControlInterface(), actual.getBusProtocol());                //CTRL_I2C
    verify.deviceAddress(msg.getAddress(), actual.getDeviceAddress());                     //0x90
    verify.systemName(msg.getSystemName(), actual.getSystemName()); //Virtual CS42L42 Customer Demonstration Board
    verify.deviceName(msg.getDeviceName(), actual.getDeviceName());                        //cs42l42
    verify.operation(RegionEvent.READ, actual.getOperation());                             //R
    verify.regOrFieldName(firstElement.getElementName(), actual.getRegisterOrFieldName()); //MCLK Control
    verify.elementType(firstElement.getElementType(), actual.getType());                   //REGISTER
    verify.timestamp(actual.getTimestamp());
  }

  @Test
  public void onMessageReceived_writeEvent_allFieldsFormatted() {
    //Arrange
    TopicNotification tn = ExampleRegionChangeNotifications.newOnChangeWriteMessage();
    RegionChangeMessageData msg = (RegionChangeMessageData) tn.getMessage();
    RegionChangeElement firstElement = msg.getChangedElements().get(0);

    //Act
    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    //Assert
    verifyTableRowCount();
    TableRowModel actual = getHistoryTableItems().get(0);

    //Comments to the right are the actual observed values
    verify.address(firstElement.getElementAddress(), actual.getAddress());                  //0xc08
    verify.data(firstElement.getNewValue(), actual.getData());                              //0xe1000001
    verify.notApplicable(actual.getPage());                                                 //N/A
    verify.busProtocol(msg.getControlInterface(), actual.getBusProtocol());                 //CTRL_SPI
    verify.deviceAddress(msg.getAddress(), actual.getDeviceAddress());                      //0x01
    verify.deviceName(msg.getDeviceName(), actual.getDeviceName());                         //CS48LV40F
    verify.systemName(msg.getSystemName(), actual.getSystemName()); //Virtual CDB48LV40-M-1 Mini Board
    verify.operation(RegionEvent.WRITE, actual.getOperation());                             //W
    verify.regOrFieldName(firstElement.getElementName(), actual.getRegisterOrFieldName());  //GPI01_CTRL1
    verify.elementType(firstElement.getElementType(), actual.getType());                    //REGISTER
    verify.timestamp(actual.getTimestamp());
  }

  @Test
  public void onMessageReceived_blockReadEvent_allFieldsFormatted() {
    //Arrange
    TopicNotification tn = ExampleRegionChangeNotifications.newOnChangeBlockReadMessageWithTwoElements();
    RegionChangeMessageData msg = (RegionChangeMessageData) tn.getMessage();
    RegionChangeElement firstElement = msg.getChangedElements().get(0);

    //Act
    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    //Assert
    verifyTableRowCount();
    TableRowModel actual = getHistoryTableItems().get(0);

    //Comments to the right are the actual observed values
    verify.multipleAddresses(firstElement.getElementAddress(), actual.getAddress());       //0x20000000(...)
    verify.multipleData(firstElement.getNewValue(), actual.getData());                     //0x00000000 (...)
    verify.notApplicable(actual.getPage());                                                //N/A
    verify.busProtocol(msg.getControlInterface(), actual.getBusProtocol());                //CTRL_SPI
    verify.deviceAddress(firstElement.getDeviceAddress(), actual.getDeviceAddress());      //0x01
    verify.systemName(msg.getSystemName(), actual.getSystemName()); //Virtual CDB48LV40-M-1 Mini Board
    verify.deviceName(msg.getDeviceName(), actual.getDeviceName());                        //CS48LV40F
    verify.operation(RegionEvent.BLOCK_READ, actual.getOperation());                       //BR
    verify.regOrFieldName(firstElement.getElementName(), actual.getRegisterOrFieldName()); //VPU1_DATA_HEADER_COREID
    verify.elementType(firstElement.getElementType(), actual.getType());                   //REGISTER
    verify.timestamp(actual.getTimestamp());
  }

  @Test
  public void onMessageReceived_blockWriteEvent_allFieldsFormatted() {
    //Arrange
    TopicNotification tn = ExampleRegionChangeNotifications.newOnChangeBlockWriteMessageWithTwoElements();
    RegionChangeMessageData msg = (RegionChangeMessageData) tn.getMessage();
    RegionChangeElement firstElement = msg.getChangedElements().get(0);

    //Act
    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    //Assert
    verifyTableRowCount();
    TableRowModel actual = getHistoryTableItems().get(0);

    //Comments to the right are the actual observed values
    verify.multipleAddresses(firstElement.getElementAddress(), actual.getAddress());   //0x20000000(...)
    verify.multipleData(firstElement.getNewValue(), actual.getData());                 //0x00000000 (...)
    verify.notApplicable(actual.getPage());                                            //N/A
    verify.busProtocol(msg.getControlInterface(), actual.getBusProtocol());            //CTRL_APB_SPI
    verify.deviceAddress(firstElement.getDeviceAddress(), actual.getDeviceAddress());  //0x01
    verify.systemName(msg.getSystemName(), actual.getSystemName());                    //Virtual CS42L77 CDB
    verify.deviceName(msg.getDeviceName(), actual.getDeviceName());                    //CS42L77
    verify.operation(RegionEvent.BLOCK_WRITE, actual.getOperation());                  //BW
    verify.notApplicable(actual.getRegisterOrFieldName());                             //N/A
    verify.elementType(firstElement.getElementType(), actual.getType());               //REGISTER
    verify.timestamp(actual.getTimestamp());
  }

  @Test
  public void onMessageReceived_blockReadPagedEvent_allFieldsFormatted() {
    //Arrange
    TopicNotification tn = ExampleRegionChangeNotifications.newOnChangeBlockReadPagedMessageWithOneElement();
    RegionChangeMessageData msg = (RegionChangeMessageData) tn.getMessage();
    RegionChangeElement firstElement = msg.getChangedElements().get(0);

    //Act
    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    //Assert
    verifyTableRowCount();
    TableRowModel actual = getHistoryTableItems().get(0);

    //Comments to the right are the actual observed values
    verify.multipleAddresses(firstElement.getElementAddress(), actual.getAddress());       //0x00(...)
    verify.multipleData(firstElement.getNewValue(), actual.getData());                     //0x00000000 (...)
    verify.page(firstElement.getElementPage(), actual.getPage());                          //0x00
    verify.busProtocol(msg.getControlInterface(), actual.getBusProtocol());                //CTRL_SPI
    verify.deviceAddress(firstElement.getDeviceAddress(), actual.getDeviceAddress());      //0x90
    verify.systemName(msg.getSystemName(), actual.getSystemName()); //Virtual CS42L42 Customer Demonstration Board
    verify.deviceName(msg.getDeviceName(), actual.getDeviceName());                        //cs42l42
    verify.operation(RegionEvent.BLOCK_READ, actual.getOperation());                       //BR
    verify.regOrFieldName(firstElement.getElementName(), actual.getRegisterOrFieldName()); //Page Select
    verify.elementType(firstElement.getElementType(), actual.getType());                   //REGISTER
    verify.timestamp(actual.getTimestamp());
  }

  @Test
  public void onMessageReceived_asyncRequest_allFieldsFormatted() {
    //Arrange
    TopicNotification tn = ExampleNotifications.newAsyncRequestMessage();
    AsyncRequestMessageData msg = (AsyncRequestMessageData) tn.getMessage();

    //Act
    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    //Assert
    verifyTableRowCount();
    TableRowModel actual = getHistoryTableItems().get(0);

    //Comments to the right are the actual observed values
    verify.notApplicable(actual.getAddress());                                  //N/A
    verify.notApplicable(actual.getData());                                     //N/A
    verify.notApplicable(actual.getPage());                                     //N/A
    verify.busProtocol(msg.getControlInterfaceType(), actual.getBusProtocol()); //CTRL_SPI
    verify.deviceAddress(msg.getDeviceAddress(), actual.getDeviceAddress());    //0x01
    verify.deviceName(msg.getDeviceName(), actual.getDeviceName());             //CS48LV40F
    verify.systemName(msg.getSystemName(), actual.getSystemName());             //Virtual CDB48LV40-M-1 Mini Board
    verify.notApplicable(actual.getOperation());                                //N/A
    verify.notApplicable(actual.getRegisterOrFieldName());                      //N/A
    verify.notApplicable(actual.getType());                                     //N/A
    verify.timestamp(actual.getTimestamp());
  }

  @Test
  public void onMessageReceived_unsolicitedMessage_allFieldsFormatted() {
    //Arrange
    TopicNotification tn = ExampleNotifications.newUnsolicitedMessage();

    //Act
    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    //Assert
    verifyTableRowCount();
    TableRowModel actual = getHistoryTableItems().get(0);

    //Comments to the right are the actual observed values
    verify.notApplicable(actual.getAddress());                                  //N/A
    verify.notApplicable(actual.getData());                                     //N/A
    verify.notApplicable(actual.getPage());                                     //N/A
    verify.notApplicable(actual.getBusProtocol());                              //N/A
    verify.notApplicable(actual.getDeviceAddress());                            //N/A
    verify.notApplicable(actual.getDeviceName());                               //N/A
    verify.notApplicable(actual.getSystemName());                               //N/A
    verify.operation("Unsolicited", actual.getOperation());                     // Unsolicited
    verify.regOrFieldName("01 02 03 04", actual.getRegisterOrFieldName());      // 01 02 03 04
    verify.notApplicable(actual.getType());                                     //N/A
    verify.timestamp(actual.getTimestamp());
  }

  @Test
  public void onMessageReceived_pollingMessage_allFieldsFormatted() {
    //Arrange
    TopicNotification tn = ExampleNotifications.newPollingMessage();
    PollingMessageData msg = (PollingMessageData) tn.getMessage();

    //Act
    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    //Assert
    verifyTableRowCount();
    TableRowModel actual = getHistoryTableItems().get(0);

    //Comments to the right are the actual observed values
    verify.address(msg.getRegisterAddress(), actual.getAddress());              //0xed8
    verify.notApplicable(actual.getData());                                     //N/A
    verify.notApplicable(actual.getPage());                                     //N/A
    verify.busProtocol(msg.getControlInterfaceType(), actual.getBusProtocol()); //CTRL_UNKNOWN
    verify.deviceAddress(msg.getDeviceAddress(), actual.getDeviceAddress());    //0x00
    verify.deviceName(msg.getDeviceName(), actual.getDeviceName());             //CS48LV40F
    verify.systemName(msg.getSystemName(), actual.getSystemName());             //Virtual CDB48LV40-M-1 Mini Board
    verify.notApplicable(actual.getOperation());                                //N/A
    verify.regOrFieldName(msg.getMessageInterruptRegister(), actual.getRegisterOrFieldName()); //GPIO1_CTRL1
    verify.elementType("Polling: 3/4", actual.getType());                       //Polling: 3/4
    verify.timestamp(actual.getTimestamp());
  }

  @Test
  /**
   * Test the data is formatted and looks logical. This test may need to be reworked as
   * part of https://tracker.cirrus.com/browse/SCSSTU-972.
   */
  public void onMessageReceived_secureMessage_allFieldsFormatted() {
    //Arrange
    TopicNotification tn = ExampleNotifications.newSecureMessage();
    SecureMessageData msg = (SecureMessageData) tn.getMessage();

    //Act
    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    //Assert
    verifyTableRowCount();
    TableRowModel actual = getHistoryTableItems().get(0);

    //Comments to the right are the actual observed values
    verify.address(msg.getRegisterAddress(), actual.getAddress());              //0xed8
    verify.notApplicable(actual.getData());                                     //N/A
    verify.notApplicable(actual.getPage());                                     //N/A
    verify.busProtocol(msg.getControlInterfaceType(), actual.getBusProtocol()); //CTRL_UNKNOWN
    verify.deviceAddress(msg.getDeviceAddress(), actual.getDeviceAddress());    //0x00
    verify.deviceName(msg.getDeviceName(), actual.getDeviceName());             //CS48LV40F
    verify.systemName(msg.getSystemName(), actual.getSystemName());             //Virtual CDB48LV40-M-1 Mini Board
    verify.notApplicable(actual.getOperation());                                //N/A
    verify.regOrFieldName("40,false,NOTIFICATION,20,0,Unknown,CS48LV40F", actual.getRegisterOrFieldName());
    verify.notApplicable(actual.getType());                                    //N/A
    verify.timestamp(actual.getTimestamp());
  }

  /**
   * The other tests in this file need to be updated if additional columns are added to the table.
   * This test ensures that we do not forget to do so.
   */
  @Test
  public void verifyColumnCount() {
    String msg = "Set of columns has changed. Please update tests.";
    int colCount = getHistoryTable().getColumns().size();
    assertEquals(msg, 11, colCount);
  }

  private void verifyTableRowCount() {
    int expected = 1;
    int actual = getHistoryTableItems().size();
    assertEquals("Unexpected row count.", expected, actual);
  }

  /**
   * This class exists to satisfy PMD warning.  Without this class PMD warned that the parent class
   * had too many methods.
   */
  /*
   * PMD still complains that this new class has too many methods.  Cannot think of any
   * meaningful way to reduce the number of methods as there needs to be at least one method for
   * each column in the table.
   */
  @SuppressWarnings("PMD.TooManyMethods")
  private static class Verify {
    private void timestamp(String actual) {
      assertTrue("Incorrect timestamp format: " + actual, actual.matches(TIMESTAMP_REGEX));
    }

    public void elementType(String expected, String actual) {
      assertEquals("Incorrect element.", expected, actual);
    }

    private void elementType(ElementType expected, String actual) {
      assertEquals("Incorrect element.", expected.toString(), actual);
    }

    private void regOrFieldName(String expected, String actual) {
      assertEquals("Incorrect register or field name.", expected, actual);
    }

    private void operation(RegionEvent expected, String actual) {
      String expectedOp;
      switch(expected) {
        case BLOCK_READ:
          expectedOp = "BR";
          break;
        case READ:
          expectedOp = "R";
          break;
        case VALUE_CHANGE:
          expectedOp = "VC";
          break;
        case WRITE:
          expectedOp = "W";
          break;
        case BLOCK_WRITE:
          expectedOp = "BW";
          break;
        default:
          throw new IllegalStateException("Unexpected event: " + expected.toString());
      }

      assertEquals("Incorrect operation.", expectedOp, actual);
    }

    private void operation(String expected, String actual) {
      assertEquals("Incorrect operation.", expected, actual);
    }

    private void systemName(String expected, String actual) {
      assertEquals("Incorrect system name.", expected, actual);
    }

    private void deviceName(String expected, String actual) {
      assertEquals("Incorrect device name.", expected, actual);
    }

    private void deviceAddress(long expected, String actual) {
      assertEquals("Incorrect device address.", formattedAddress(expected), actual);
    }

    private void busProtocol(ControlInterfaceValues expected, String actual) {
      assertEquals("Incorrect bus protocol.", expected.toString(), actual);
    }

    private void notApplicable(String s) {
      assertEquals("String not " + NONE, NONE, s);
    }

    private void data(long expected, String actual) {
      assertEquals("Incorrect data.", formattedData(expected), actual);
    }

    private void page(long expected, String actual) {
      assertEquals("Incorrect page.", formattedPage(expected), actual);
    }

    private void multipleData(long expected, String actual) {
      assertEquals("Incorrect multiple data.", formattedData(expected) + MULTIPLE_DATA, actual);
    }

    private void address(long expected, String actual) {
      assertEquals("Incorrect address.", formattedAddress(expected), actual);
    }

    private void multipleAddresses(long expected, String actual) {
      assertEquals(
          "Incorrect multiple addresses.",
          formattedAddress(expected) + MULTIPLE_ADDRESSES,
          actual);
    }

    private String formattedData(long data) {
      return String.format(DATA_HEX_FORMAT, data);
    }

    private String formattedAddress(long address) {
      return String.format(ADDRESS_HEX_FORMAT, address);
    }

    private String formattedPage(long page) {
      return String.format(PAGE_HEX_FORMAT, page);
    }
  }
}

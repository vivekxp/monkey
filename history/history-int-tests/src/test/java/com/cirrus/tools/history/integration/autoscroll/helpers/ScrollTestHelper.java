/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ScrollTestHelper.java
 */
package com.cirrus.tools.history.integration.autoscroll.helpers;

import com.cirrus.tools.controls.CirrusCheckBox;
import com.cirrus.tools.controls.CirrusTableView;
import com.cirrus.tools.history.impl.table.ImmutableTableRowData;
import com.cirrus.tools.history.impl.table.TableRowFormatter;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.javafx.HistoryControlSkin;
import com.cirrus.tools.history.javafx.HistoryDataListJavaFX;
import com.cirrus.tools.history.javafx.table.HistoryScrollBar;
import com.cirrus.tools.test.CirrusApplicationTest;
import javafx.application.Platform;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.ScrollBar;

import java.util.Random;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class ScrollTestHelper extends CirrusApplicationTest {

  private static final String SELECTOR_SCROLL_BAR = ".scroll-bar";

  private final CirrusTableView<TableRowModel> historyTable;
  private final HistoryDataListJavaFX historyData;
  private CirrusCheckBox autoscrollCheckBox;

  private HistoryScrollBar historyScrollBar;

  public ScrollTestHelper(CirrusTableView<TableRowModel> historyTable,
      HistoryDataListJavaFX historyData) {
    this.historyTable = historyTable;
    this.historyData = historyData;
  }

  public ScrollTestHelper scrollToSomewhereInMiddle() {
    getScrollBar().scrollToValueForTest(getRandomMiddlePosition());
    waitForFxThread();
    return this;
  }

  public boolean isAtBottom() {
    waitForFxThread();
    return getScrollBar().isAtBottom();
  }

  public ScrollTestHelper addBunchOfRows() {
    Platform.runLater(() -> {
      for (int i = 0; i < getRandomBunchSize(); i++) {
        addRowToTableAndVerify();
      }
    });
    waitForFxThread();
    return this;
  }

  public ScrollTestHelper enableAutoScroll() {
    if (!getAutoScrollCheckbox().getSelected()) {
      autoscrollCheckBox.setSelected(true);
    }
    waitForFxThread();
    return this;
  }

  public ScrollTestHelper disableAutoScroll() {
    if (getAutoScrollCheckbox().getSelected()) {
      autoscrollCheckBox.setSelected(false);
    }
    waitForFxThread();
    return this;
  }

  private CirrusCheckBox getAutoScrollCheckbox() {
    return autoscrollCheckBox != null ? autoscrollCheckBox : lookUpAutoscrollCheckbox();
  }

  private CirrusCheckBox lookUpAutoscrollCheckbox() {
    return this.autoscrollCheckBox = lookup("#" + HistoryControlSkin.AUTOSCROLL_CHECKBOX_ID).query();
  }

  private HistoryScrollBar getScrollBar() {
    return historyScrollBar != null ? historyScrollBar : lookUpScrollBar();
  }

  private HistoryScrollBar lookUpScrollBar() {
    // if empty add one row to ensure we can get handle to scrollBar
    if (historyTable.getItems().isEmpty()) {
      addRowToTableAndVerify();
    }
    waitForNode(SELECTOR_SCROLL_BAR);
    Set<Node> scrollBarNodes = historyTable.lookupAll(SELECTOR_SCROLL_BAR);
    ScrollBar scrollBar = (ScrollBar) scrollBarNodes.stream()
        .filter(node -> ((ScrollBar) node).getOrientation().equals(Orientation.VERTICAL))
        .findFirst().get();
    return this.historyScrollBar = new HistoryScrollBar(historyTable, scrollBar);
  }

  private int getRandomBunchSize() {
    return getRandomInteger(30, 50);
  }

  private double getRandomMiddlePosition() {
    return getRandomInteger(1, 9) / 10.0;
  }

  private int getRandomInteger(int min, int max) {
    return new Random().nextInt(max - min) + min;
  }

  private void addRowToTableAndVerify() {
    ImmutableTableRowData rowData = new ImmutableTableRowData.Builder().build();
    TableRowModel rowModel = new TableRowModel(rowData, new TableRowFormatter());
    int expectedCount = historyData.getObservableList().size() + 1;
    historyData.addRowToList(rowModel);
    assertEquals("Row not added.", expectedCount, historyTable.getItems().size());
  }
}

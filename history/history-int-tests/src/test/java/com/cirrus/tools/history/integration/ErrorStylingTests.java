/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ErrorStylingTests.java
 */

package com.cirrus.tools.history.integration;

import com.cirrus.scs.studiolink.api.enums.ControlInterface.ControlInterfaceValues;
import com.cirrus.scs.studiolink.api.enums.FieldRegisterOperationStatus;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement.ElementType;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData.RegionEvent;
import com.cirrus.scs.studiolink.api.notifications.TopicNotification;
import com.cirrus.tools.history.javafx.table.RowFactory;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ErrorStylingTests extends BaseIntegrationTest {

  @Test
  public void onMessageReceived_noErrorReadingFromDevice_rowIsNotStyledForError() {
    boolean hasError = false;
    TopicNotification tn = newMessage(hasError);

    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    String actual = getFirstRowStyle();
    assertNotEquals(RowFactory.ERROR_STYLE, actual);
  }

  @Test
  public void onMessageReceived_errorReadingFromFirstElement_rowIsStyledForError() {
    boolean hasError = true;
    TopicNotification tn = newMessage(hasError);

    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    String actual = getFirstRowStyle();
    assertEquals(RowFactory.ERROR_STYLE, actual);
  }

  /**
   * This test verifies that if the second (or subsequent) element of a multi-element change
   * request message is marked as having an error, the row is highlighted as having an error,
   * even if the first/top element did not have an error.
   */
  @Test
  public void onMessageReceived_errorReadingFromSecondElementOnly_rowIsStyledForError() {
    TopicNotification tn = newMessageWithErrorInSecondElement();

    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    String actual = getFirstRowStyle();
    assertEquals(RowFactory.ERROR_STYLE, actual);
  }

  @Test
  public void addNullRowToTable_rowIsNotStyledForError() {
    //Act
    getDataList().addRowToList(null);
    waitForFxThread();

    //Assert
    String actual = getFirstRowStyle();
    assertNotEquals(RowFactory.ERROR_STYLE, actual);
  }

  private String getFirstRowStyle() {
    return getHistoryTable().lookup(".table-row-cell").getStyle();
  }

  private TopicNotification newMessageWithErrorInSecondElement() {
    List<RegionChangeElement> elements = Arrays.asList(
        newElement(false),
        newElement(true));

    String topicUri = RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE;
    RegionChangeMessageData msg = newMessage(elements);

    return new TopicNotification(topicUri, msg);
  }

  private static TopicNotification newMessage(boolean withError) {
    String topicUri = RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE;
    RegionChangeElement element = newElement(withError);
    List<RegionChangeElement> elements = Collections.singletonList(element);
    RegionChangeMessageData msg = newMessage(elements);

    return new TopicNotification(topicUri, msg);
  }

  private static RegionChangeMessageData newMessage(List<RegionChangeElement> elements) {
    return new RegionChangeMessageData(
        RegionEvent.BLOCK_WRITE,
        "systemName",
        "deviceName",
        ControlInterfaceValues.CTRL_SPI,
        1,                       //address,
        elements);
  }

  private static RegionChangeElement newElement(boolean withError) {
    return new RegionChangeElement(
        "elementName",
        3800,                   //elementAddress,
        0,                      //elementPage,
        ElementType.REGISTER,
        3774873601L,            //newValue,
        withError,
        FieldRegisterOperationStatus.SUCCESS,
        false, //secure,
        "protocolName",
        1,                      //deviceAddress
        32,                     //bitWidth
        false                   //isPaged
    );
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ClearTableIntegrationTests.java
 */

package com.cirrus.tools.history.integration;

import com.cirrus.tools.history.impl.table.ImmutableTableRowData;
import com.cirrus.tools.history.impl.table.TableRowFormatter;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.javafx.HistoryControlSkin;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ClearTableIntegrationTests extends BaseIntegrationTest {

  @Test
  public void clickClearButton_tableNotEmpty_tableCleared() {
    addRowToTableAndVerify();

    clickOn("#" + HistoryControlSkin.CLEAR_BUTTON_ID);
    waitForFxThread();

    assertEquals("Table not emptied.", 0, getHistoryTable().getItems().size());
  }

  @Test
  public void apiCall_tableNotEmpty_tableCleared() {
    addRowToTableAndVerify();

    clearTable();
    waitForFxThread();

    assertEquals("Table not emptied.", 0, getHistoryTable().getItems().size());
  }

  private void addRowToTableAndVerify() {
    ImmutableTableRowData rowData = new ImmutableTableRowData.Builder().build();
    TableRowModel rowModel = new TableRowModel(rowData, new TableRowFormatter());
    getDataList().addRowToList(rowModel);
    assertFalse("Table did not start empty.", getHistoryTable().getItems().isEmpty());
  }
}

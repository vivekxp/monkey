/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ExampleRegionChangeNotifications.java
 */

package com.cirrus.tools.history.integration;

import com.cirrus.scs.studiolink.api.enums.ControlInterface.ControlInterfaceValues;
import com.cirrus.scs.studiolink.api.enums.FieldRegisterOperationStatus;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement.ElementType;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData.RegionEvent;
import com.cirrus.scs.studiolink.api.notifications.TopicNotification;

import java.util.LinkedList;
import java.util.List;

/**
 * Test helper to supply prebuilt non-mocked Link notifications.
 */
public final class ExampleRegionChangeNotifications {

  private ExampleRegionChangeNotifications() {
    throw new UnsupportedOperationException("Util class only.");
  }

  /**
   * Real notification data received after updating a field in SCS.
   */
  public static TopicNotification newOnChangeWriteMessage() {
    String topicUri = RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE;
    List<RegionChangeElement> changedElements = new LinkedList<>();

    changedElements.add(new RegionChangeElement(
        "GPIO1_CTRL1",          //elementName,
        3800,                   //elementAddress,
        0,                      //elementPage,
        ElementType.REGISTER,
        3774873601L,            //newValue,
        false,                  //errorReadingFromDevice,
        FieldRegisterOperationStatus.SUCCESS,
        false, //secure,
        "4wireSPI_32inx_32dat", //protocolName,
        1,                      //deviceAddress
        32,                     //bitWidth
        false                   //isPaged
    ));

    RegionChangeMessageData msg = new RegionChangeMessageData(
        RegionEvent.WRITE,
        "Virtual CDB48LV40-M-1 Mini Board", //systemName
        "CS48LV40F",                        //deviceName,
        ControlInterfaceValues.CTRL_SPI,
        1,                                  //address,
        changedElements);

    return new TopicNotification(topicUri, msg);
  }

  /**
   * Real notification data received after reading a field in SCS.
   */
  public static TopicNotification newOnChangeReadMessage() {
    String topicUri = RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE;
    List<RegionChangeElement> changedElements = new LinkedList<>();

    changedElements.add(new RegionChangeElement(
        "ENABLE_CTRL1",       //elementName,
        7,                    //elementAddress,
        0,                    //elementPage,
        ElementType.REGISTER,
        0L,                   //newValue,
        false,                //errorReadingFromDevice,
        FieldRegisterOperationStatus.SUCCESS,
        false,                //secure,
        "SMbus_16inx_16dat",  //protocolName,
        232,                  //deviceAddress
        16,                   //bitWidth
        false                 //isPaged
    ));

    RegionChangeMessageData msg = new RegionChangeMessageData(
        RegionEvent.READ,
        "Virtual CS40L60 A1 CDB", //systemName
        "CS40L60 Mode 0",         //deviceName,
        ControlInterfaceValues.CTRL_I2C,
        232,                      //address,
        changedElements);

    return new TopicNotification(topicUri, msg);
  }

  /**
   * BASED ON real notification data received after refreshing a device in SCS.
   * Real notification had nine change elements.
   */
  public static TopicNotification newOnChangeBlockReadMessageWithTwoElements() {
    String topicUri = RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE;
    List<RegionChangeElement> changedElements = new LinkedList<>();

    changedElements.add(new RegionChangeElement(
        "VPU1_DATA_HEADER_COREID",  //elementName,
        536870912,                  //elementAddress,
        0,                          //elementPage,
        ElementType.REGISTER,
        0L,                         //newValue,
        false,                      //errorReadingFromDevice,
        FieldRegisterOperationStatus.SUCCESS,
        false,                      //secure,
        "4wireSPI_32inx_32dat",     //protocolName,
        1,                          //deviceAddress
        32,                         //bitWidth
        false                       //isPaged
    ));

    changedElements.add(new RegionChangeElement(
        "VPU1_DATA_HEADER_API_REVISION",  //elementName,
        536870916,                        //elementAddress,
        0,                                //elementPage,
        ElementType.REGISTER,
        0L,                               //newValue,
        false,                            //errorReadingFromDevice,
        FieldRegisterOperationStatus.SUCCESS,
        false,                            //secure,
        "4wireSPI_32inx_32dat",           //protocolName,
        1,                                //deviceAddress
        32,                               //bitWidth
        false                             //isPaged
    ));

    RegionChangeMessageData msg = new RegionChangeMessageData(
        RegionEvent.BLOCK_READ,
        "Virtual CDB48LV40-M-1 Mini Board", //systemName
        "CS48LV40F",                        //deviceName,
        ControlInterfaceValues.CTRL_SPI,
        1,                                  //address,
        changedElements);

    return new TopicNotification(topicUri, msg);
  }

  /**
   * BASED ON real notification data received after refreshing a device in SCS.
   * Real notification had five change elements.
   */
  public static TopicNotification newOnChangeBlockWriteMessageWithTwoElements() {
    String topicUri = RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE;
    List<RegionChangeElement> changedElements = new LinkedList<>();

    changedElements.add(new RegionChangeElement(
        "",  //elementName,
        939524096,                  //elementAddress,
        0,                          //elementPage,
        ElementType.REGISTER,
        2147483776L,                //newValue,
        false,                      //errorReadingFromDevice,
        FieldRegisterOperationStatus.SUCCESS,
        false,                      //secure,
        "APB_SPI_32BIT",            //protocolName,
        2,                          //deviceAddress
        32,                         //bitWidth
        false                       //isPaged
    ));

    changedElements.add(new RegionChangeElement(
        "939524100",                      //elementName,
        939524100,                        //elementAddress,
        0,                                //elementPage,
        ElementType.REGISTER,
        2327838876L,                      //newValue,
        false,                            //errorReadingFromDevice,
        FieldRegisterOperationStatus.SUCCESS,
        false,                            //secure,
        "APB_SPI_32BIT",                  //protocolName,
        2,                                //deviceAddress
        32,                               //bitWidth
        false                             //isPaged
    ));

    RegionChangeMessageData msg = new RegionChangeMessageData(
        RegionEvent.BLOCK_WRITE,
        "Virtual CS42L77 CDB",              //systemName
        "CS42L77",                          //deviceName,
        ControlInterfaceValues.CTRL_APB_SPI,
        2,                                  //address,
        changedElements);

    return new TopicNotification(topicUri, msg);
  }

  /**
   * Real notification data received after updating a field in SCS.
   */
  public static TopicNotification newOnChangeValueChangeMessage() {
    String topicUri = RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE;
    List<RegionChangeElement> changedElements = new LinkedList<>();

    changedElements.add(new RegionChangeElement(
        "GP1_DB",               //elementName,
        3800,                   //elementAddress,
        0,                      //elementPage,
        ElementType.FIELD,
        0L,                     //newValue,
        false,                  //errorReadingFromDevice,
        FieldRegisterOperationStatus.SUCCESS,
        false,                  //secure,
        "4wireSPI_32inx_32dat", //protocolName,
        1                       //deviceAddress
    ));

    changedElements.add(new RegionChangeElement(
        "GPIO1_CTRL1",          //elementName,
        3800,                   //elementAddress,
        0,                      //elementPage,
        ElementType.REGISTER,
        3774873601L,            //newValue,
        false,                  //errorReadingFromDevice,
        FieldRegisterOperationStatus.SUCCESS,
        false,                  //secure,
        "4wireSPI_32inx_32dat", //protocolName,
        1                       //deviceAddress
    ));

    RegionChangeMessageData msg = new RegionChangeMessageData(
        RegionEvent.VALUE_CHANGE,
        "Virtual CDB48LV40-M-1 Mini Board", //systemName
        "CS48LV40F",                        //deviceName,
        ControlInterfaceValues.CTRL_SPI,
        1,                                  //address,
        changedElements);

    return new TopicNotification(topicUri, msg);
  }

  /**
   * BASED ON real notification data received after refreshing a device in SCS.
   */
  public static TopicNotification newOnChangeBlockReadPagedMessageWithOneElement() {
    String topicUri = RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE;
    List<RegionChangeElement> changedElements = new LinkedList<>();

    changedElements.add(new RegionChangeElement(
        "Page Select",      //elementName,
        0,                  //elementAddress,
        0,                  //elementPage,
        ElementType.REGISTER,
        0L,                 //newValue,
        false,              //errorReadingFromDevice,
        FieldRegisterOperationStatus.SUCCESS,
        false,              //secure,
        "SMbus_8inx_8dat",  //protocolName,
        144,                //deviceAddress
        8,                  //bitWidth
        true                //isPaged
    ));

    changedElements.add(new RegionChangeElement(
        "VPU1_DATA_HEADER_API_REVISION",  //elementName,
        536870916,                        //elementAddress,
        0,                                //elementPage,
        ElementType.REGISTER,
        0L,                               //newValue,
        false,                            //errorReadingFromDevice,
        FieldRegisterOperationStatus.SUCCESS,
        false,                            //secure,
        "4wireSPI_32inx_32dat",           //protocolName,
        1,                                //deviceAddress
        32,                               //bitWidth
        false                             //isPaged
    ));

    RegionChangeMessageData msg = new RegionChangeMessageData(
        RegionEvent.BLOCK_READ,
        "Virtual CS42L42 Customer Demonstration Board", //systemName
        "cs42l42",                                      //deviceName,
        ControlInterfaceValues.CTRL_I2C,
        144,                                            //address,
        changedElements);

    return new TopicNotification(topicUri, msg);
  }

  /**
   * Real notification data received after reading a field in SCS.
   */
  public static TopicNotification newOnChangeReadPagedMessage() {
    String topicUri = RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE;
    List<RegionChangeElement> changedElements = new LinkedList<>();

    changedElements.add(new RegionChangeElement(
        "MCLK Control",     //elementName,
        9,                  //elementAddress,
        16,                 //elementPage,
        ElementType.REGISTER,
        2L,                 //newValue,
        false,              //errorReadingFromDevice,
        FieldRegisterOperationStatus.SUCCESS,
        false,              //secure,
        "SMbus_8inx_8dat",  //protocolName,
        144,                //deviceAddress
        8,                  //bitWidth
        true                //isPaged
    ));

    RegionChangeMessageData msg = new RegionChangeMessageData(
        RegionEvent.READ,
        "Virtual CS42L42 Customer Demonstration Board", //systemName
        "cs42l42",                                      //deviceName,
        ControlInterfaceValues.CTRL_I2C,
        144,                                            //address,
        changedElements);

    return new TopicNotification(topicUri, msg);
  }
}

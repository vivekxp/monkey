/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file DisplayDialogPaneTests.java
 */

package com.cirrus.tools.history.integration;

import com.cirrus.tools.controls.Theme;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.javafx.DisplayDialogPane;

import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.VBox;
import org.junit.Before;
import org.junit.Test;
import org.testfx.service.query.NodeQuery;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DisplayDialogPaneTests extends BaseIntegrationTest {
  private static final Theme THEME = Theme.DARK;

  private DisplayDialogPane sut;

  @Before
  public void arrangeAndAct() {
    ObjectProperty<Theme> themeProperty = newMockObjectThemeProperty();
    when(themeProperty.getValue()).thenReturn(THEME);

    //Act
    sut = new DisplayDialogPane(getHistoryTable().getColumns(), themeProperty);
    interact(() -> root.getChildren().setAll(sut));
  }

  @Test
  public void constructor_tableHasColumns_checkboxesAddedForEachColumn() {
    //Arrange & Act in beforeEach()

    //Assert
    ObservableList<TableColumn<TableRowModel, ?>> columns = getHistoryTable().getColumns();
    assertFalse("Pre-condition fail - table has no columns.", columns.isEmpty());

    columns.forEach(col -> {
      NodeQuery query = lookup(col.getText());
      assertNotNull(query.queryAs(CheckBox.class));
    });
  }

  @Test
  public void constructor_themeNotNull_themeSet() {
    //Arrange & Act in beforeEach()

    //Assert
    VBox vbox = lookup("#checkboxGroup").queryAs(VBox.class);
    String expected = THEME.getCssFilenames().stream().findFirst().orElse(null);
    String actual = vbox.getParent().getStylesheets().get(0);

    assertEquals(expected, actual);
  }

  @SuppressWarnings("unchecked")  //Generic mock
  private ObjectProperty<Theme> newMockObjectThemeProperty() {
    return mock(ObjectProperty.class);
  }
}

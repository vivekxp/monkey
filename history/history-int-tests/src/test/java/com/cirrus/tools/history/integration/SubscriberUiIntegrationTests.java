/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file SubscriberUiIntegrationTests.java
 */

package com.cirrus.tools.history.integration;

import com.cirrus.scs.studiolink.api.notifications.TopicNotification;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * This class tests that History's Link Subscriber correctly updates the
 * History table with the correct number of rows when various Link
 * notifications are received.
 */
public class SubscriberUiIntegrationTests extends BaseIntegrationTest {
  /**
   * Length of time to sleep during integration tests that update a UI
   * component asynchronously via Platform.runLater() method.  At least one
   * test in this class asserts that a component is NOT updated, so those tests
   * have no choice but to wait a reasonable amount of time before asserting the
   * component was not updated.
   */
  private static final long PLATFORM_RUN_LATER_SLEEP_MILLIS = 3000;

  @Test
  public void onMessageReceived_regionChangeMessageDataWithWriteEvent_oneRowAddedToHistoryTable() {
    TopicNotification tn = ExampleRegionChangeNotifications.newOnChangeWriteMessage();

    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    int expected = 1;
    int actual = getHistoryTableItems().size();
    assertEquals(expected, actual);
  }

  @Test
  public void onMessageReceived_regionChangeMessageDataWithBlockReadEvent_oneRowAddedToHistoryTable() {
    TopicNotification tn = ExampleRegionChangeNotifications.newOnChangeBlockReadMessageWithTwoElements();

    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    int expected = 1;
    int actual = getHistoryTableItems().size();
    assertEquals(expected, actual);
  }

  @Test
  public void onMessageReceived_regionChangeMessageDataWithBlockWriteEvent_oneRowAddedToHistoryTable() {
    TopicNotification tn = ExampleRegionChangeNotifications.newOnChangeBlockWriteMessageWithTwoElements();

    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    int expected = 1;
    int actual = getHistoryTableItems().size();
    assertEquals(expected, actual);
  }

  @Test
  public void onMessageReceived_regionChangeMessageDataWithValueChangeEvent_zeroRowAddedToHistoryTable() {
    TopicNotification tn = ExampleRegionChangeNotifications.newOnChangeValueChangeMessage();

    getSubscriber().onMessageReceived(tn);
    platformRunLaterSleep();

    assertTrue(getHistoryTableItems().isEmpty());
  }

  @Test
  public void onMessageReceived_asyncRequestMessageData_oneRowAddedToHistoryTable() {
    TopicNotification tn = ExampleNotifications.newAsyncRequestMessage();

    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    int expected = 1;
    int actual = getHistoryTableItems().size();
    assertEquals(expected, actual);
  }

  @Test
  public void onMessageReceived_unsolicitedMessageData_oneRowAddedToHistoryTable() {
    TopicNotification tn = ExampleNotifications.newUnsolicitedMessage();

    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    int expected = 1;
    int actual = getHistoryTableItems().size();
    assertEquals(expected, actual);
  }

  @Test
  public void onMessageReceived_pollingMessageData_oneRowAddedToHistoryTable() {
    TopicNotification tn = ExampleNotifications.newPollingMessage();

    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    int expected = 1;
    int actual = getHistoryTableItems().size();
    assertEquals(expected, actual);
  }

  @Test
  public void onMessageReceived_secureMessageData_oneRowAddedToHistoryTable() {
    TopicNotification tn = ExampleNotifications.newSecureMessage();

    getSubscriber().onMessageReceived(tn);
    waitForFxThread();

    int expected = 1;
    int actual = getHistoryTableItems().size();
    assertEquals(expected, actual);
  }

  @Test
  public void onMessageReceived_unexpectedMessageObject_zeroRowsAddedToHistoryTable() {
    Object unexpectedMsg = new Object();
    TopicNotification tn = new TopicNotification("any topic", unexpectedMsg);

    getSubscriber().onMessageReceived(tn);
    platformRunLaterSleep();

    assertTrue(getHistoryTableItems().isEmpty());
  }

  private void platformRunLaterSleep() {
    sleep(PLATFORM_RUN_LATER_SLEEP_MILLIS, TimeUnit.MILLISECONDS);
  }
}

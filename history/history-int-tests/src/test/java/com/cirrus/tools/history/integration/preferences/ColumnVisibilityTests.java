/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ColumnVisibilityTests.java
 */
package com.cirrus.tools.history.integration.preferences;

import com.cirrus.tools.controls.CirrusTableView;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.integration.IntegrationTestController;
import com.cirrus.tools.history.javafx.HistoryControl;
import com.cirrus.tools.history.javafx.Preferences;
import com.cirrus.tools.history.javafx.table.ColumnPreferences;

import com.cirrus.tools.history.javafx.table.definition.ColumnDefinition;
import com.cirrus.tools.history.javafx.table.definition.ColumnDefinitions;
import com.cirrus.tools.test.CirrusApplicationTest;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.scene.Node;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ColumnVisibilityTests extends CirrusApplicationTest {
  /*
   * We inherit from CirrusApplicationTest rather than BaseIntegrationTest because each test below
   * needs to construct HistoryControl differently.  BaseIntegrationTest only allows a single
   * configuration of HistoryControl.
   */

  private final List<ColumnDefinition> columnDefinitions = new ColumnDefinitions().getColumns();

  @Test
  public void whenAllPreferencesAreSetToHidden_allColumnsStartHidden() {
    Preferences preferences = getNewPreferencesWithAllColumnsVisibleSetTo(false);
    initHistoryControl(preferences);

    getHistoryTable().getColumns().forEach(col ->
        assertFalse("Not hidden: " + col.getText(), col.isVisible()));
  }

  @Test
  public void whenAllPreferencesAreSetToVisible_allColumnsStartVisible() {
    Preferences preferences = getNewPreferencesWithAllColumnsVisibleSetTo(true);
    HistoryControl historyControl = initHistoryControl(preferences);

    assertAllColumnsPreferencesAreSetToVisible(historyControl);
  }

  @Test
  public void whenChangingPreferencesToVisible_columnsAppear() {
    Preferences preferences = getNewPreferencesWithAllColumnsVisibleSetTo(false);
    initHistoryControl(preferences);

    Platform.runLater(() -> setAllColumnPreferencesTo(true, preferences));
    waitForFxThread();

    //Assert
    assertAllColumnsVisibility(true);
  }

  @Test
  public void whenChangingPreferencesToHide_columnsDisappear() {
    Preferences preferences = getNewPreferencesWithAllColumnsVisibleSetTo(true);
    initHistoryControl(preferences);

    Platform.runLater(() -> setAllColumnPreferencesTo(false, preferences));
    waitForFxThread();

    //Assert
    assertAllColumnsVisibility(false);
  }

  @Test
  public void whenSettingColumnsToHidden_preferencesAreSetToHidden() {
    //Columns start visible by default
    HistoryControl historyControl = initHistoryControl();

    Platform.runLater(this::setAllColumnVisible);
    waitForFxThread();

    //Assert
    assertAllColumnsPreferencesAreSetToVisible(historyControl);
  }

  private HistoryControl initHistoryControl() {
    return initHistoryControl(new Preferences());
  }

  private HistoryControl initHistoryControl(Preferences preferences) {
    IntegrationTestController intController = new IntegrationTestController(preferences);
    interact(() -> root.getChildren().setAll(intController.getHistoryControl()));
    waitForFxThread();

    return intController.getHistoryControl();
  }

  @SuppressWarnings("unchecked") //Nodes are not generic so no choice but to cast
  private CirrusTableView<TableRowModel> getHistoryTable() {
    Node n = root.lookup(CirrusTableView.class.getSimpleName());
    return (CirrusTableView<TableRowModel>) n;
  }

  private Preferences getNewPreferencesWithAllColumnsVisibleSetTo(boolean isShowing) {
    Preferences preferences = new Preferences();
    setAllColumnPreferencesTo(isShowing, preferences);
    return preferences;
  }

  private void setAllColumnPreferencesTo(boolean isVisible, Preferences preferences) {
    Map<String, ColumnPreferences> cp = preferences.getColumnPreferences();
    columnDefinitions.forEach(colDef -> cp.get(colDef.getId()).visibleProperty().set(isVisible));
  }

  private void setAllColumnVisible() {
    getHistoryTable().getColumns().forEach(col -> col.setVisible(true));
  }

  private void assertAllColumnsVisibility(boolean expected) {
    getHistoryTable().getColumns().forEach(col ->
        assertEquals("Inverted visibility for: " + col.getText(), expected, col.isVisible()));
  }

  private void assertAllColumnsPreferencesAreSetToVisible(HistoryControl historyControl) {
    Map<String, ColumnPreferences> columnPreferences = historyControl.getPreferences().getColumnPreferences();

    columnDefinitions.forEach(colDef -> {
      String columnId = colDef.getId();
      BooleanProperty visibleProperty = columnPreferences.get(columnId).visibleProperty();
      boolean isVisible = visibleProperty.get();

      assertTrue("Column not visible: " + columnId, isVisible);
    });
  }
}

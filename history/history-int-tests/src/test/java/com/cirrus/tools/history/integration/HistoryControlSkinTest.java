/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file HistoryControlSkinTest.java
 */

package com.cirrus.tools.history.integration;

import com.cirrus.tools.controls.CirrusLabel;
import com.cirrus.tools.history.javafx.HistoryControlSkin;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HistoryControlSkinTest extends BaseIntegrationTest {
  @Test
  public void constructor_onStartUp_noHistoryLabelIsVisible() {
    waitForFxThread();

    CirrusLabel historyLabel = lookup("#" + HistoryControlSkin.NO_HISTORY_LABEL_ID).query();
    assertTrue(historyLabel.isVisible());
  }

  @Test
  public void constructor_onStartUp_noHistoryLabelHasCorrectText() {
    waitForFxThread();

    CirrusLabel historyLabel = lookup("#" + HistoryControlSkin.NO_HISTORY_LABEL_ID).query();
    assertEquals("No history", historyLabel.getText());
  }
}

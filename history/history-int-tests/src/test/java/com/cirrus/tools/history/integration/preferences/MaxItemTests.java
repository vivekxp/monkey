/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file MaxItemTests.java
 */
package com.cirrus.tools.history.integration.preferences;

import com.cirrus.tools.history.impl.HistoryDataList;
import com.cirrus.tools.history.impl.table.TableRowConsumer;
import com.cirrus.tools.history.integration.IntegrationTestController;
import com.cirrus.tools.history.javafx.Preferences;
import com.cirrus.tools.test.CirrusApplicationTest;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

public class MaxItemTests extends CirrusApplicationTest {
  /*
   * We inherit from CirrusApplicationTest rather than BaseIntegrationTest because each test below
   * needs to construct integTestControl differently.  BaseIntegrationTest only allows a single
   * configuration of HistoryControl.
   */

  @Test
  public void onStartup_maxItemsIsSetFromPreferences() {
    int expected = TableRowConsumer.DEFAULT_MAX_NUMBER_OF_ITEMS - 10; //Anything other than default

    //Arrange
    Preferences preferences = new Preferences();
    preferences.maxItemsProperty().set(expected);

    //Act
    IntegrationTestController integTestControl = initIntegrationTestControler(preferences);

    //Arrange
    int actual = lookupMaxItems(integTestControl);
    assertEquals(expected, actual);
  }

  @Test
  public void whenUpdatingMaxItemsPreferences_consumerMaxEntriesUpdated() {
    //Arrange
    Preferences preferences = new Preferences();
    IntegrationTestController integTestControl = initIntegrationTestControler(preferences);
    int startValue = lookupMaxItems(integTestControl);

    //Act
    preferences.maxItemsProperty().set(startValue + 10);

    //Assert
    int actual = lookupMaxItems(integTestControl);
    assertEquals(startValue + 10, actual);
  }

  @Test
  public void whenUpdatingMaxItemsViaApi_preferencesAreUpdated() {
    //Arrange
    Preferences preferences = new Preferences();
    IntegrationTestController integTestControl = initIntegrationTestControler(preferences);
    int startValue = lookupMaxItems(integTestControl);

    //Act
    integTestControl.getDataList().setMaxItems(startValue - 10);

    //Assert
    int actual = preferences.maxItemsProperty().get();
    assertEquals(startValue - 10, actual);
  }

  private IntegrationTestController initIntegrationTestControler(Preferences preferences) {
    IntegrationTestController intController = new IntegrationTestController(preferences);
    interact(() -> root.getChildren().setAll(intController.getHistoryControl()));
    waitForFxThread();

    return intController;
  }

  private int lookupMaxItems(IntegrationTestController integTestControl) {
    HistoryDataList historyDataList = integTestControl.getDataList().getHistoryDataList();

    try {
      Field field = historyDataList.getClass().getDeclaredField("rowConsumer");
      field.setAccessible(true);

      TableRowConsumer rowConsumer = (TableRowConsumer) field.get(historyDataList);

      field = rowConsumer.getClass().getDeclaredField("maxItems");
      field.setAccessible(true);

      return (int) field.get(rowConsumer);
    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new IllegalStateException("Could not lookup maxItems.", e);
    }
  }
}

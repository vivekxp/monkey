/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ColumnDefinitionTest.java
 */
package com.cirrus.tools.history.javafx.table.definition;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ColumnDefinitionTest {

  @Test(expected = NullPointerException.class)
  public void constructor_idNotSupplied_nullPointerExceptionThrown() {
    new ColumnDefinition.Builder()
        .setDisplayName("any")
        .setBindParameter("any")
        .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void constructor_idIsWhitespace_illegalArgumentExceptionThrown() {
    new ColumnDefinition.Builder()
        .setId(" ")
        .setDisplayName("any")
        .setBindParameter("any")
        .build();
  }

  @Test(expected = NullPointerException.class)
  public void constructor_displayNameNotSupplied_nullPointerExceptionThrown() {
    new ColumnDefinition.Builder()
        .setId("any")
        .setBindParameter("any")
        .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void constructor_displayNameIsWhitespace_illegalArgumentExceptionThrown() {
    new ColumnDefinition.Builder()
        .setId("any")
        .setDisplayName(" ")
        .setBindParameter("any")
        .build();
  }

  @Test(expected = NullPointerException.class)
  public void constructor_bindParameterNotSupplied_nullPointerExceptionThrown() {
    new ColumnDefinition.Builder()
        .setId("any")
        .setDisplayName("any")
        .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void constructor_bindParameterIsWhitespace_illegalArgumentExceptionThrown() {
    new ColumnDefinition.Builder()
        .setId("any")
        .setDisplayName("any")
        .setBindParameter(" ")
        .build();
  }

  @Test
  public void getId_idSupplied_sameIdReturned() {
    String expected = "id";
    ColumnDefinition sut = new ColumnDefinition.Builder()
        .setId(expected)
        .setDisplayName("any")
        .setBindParameter("any")
        .build();

    String actual = sut.getId();

    assertEquals(expected, actual);
  }

  @Test
  public void getDisplayName_displayNameSupplied_sameDisplayNameReturned() {
    String expected = "display name";
    ColumnDefinition sut = new ColumnDefinition.Builder()
        .setId("any")
        .setDisplayName(expected)
        .setBindParameter("any")
        .build();

    String actual = sut.getDisplayName();

    assertEquals(expected, actual);
  }

  @Test
  public void getBindParameter_bindParameterSupplied_sameBindParameterNameReturned() {
    String expected = "bindParam";
    ColumnDefinition sut = new ColumnDefinition.Builder()
        .setId("any")
        .setDisplayName("any")
        .setBindParameter(expected)
        .build();

    String actual = sut.getBindParameter();

    assertEquals(expected, actual);
  }

  @Test
  public void isSortable_isSortableSupplied_sameIsSortableReturned() {
    ColumnDefinition sut = new ColumnDefinition.Builder()
        .setId("any")
        .setDisplayName("any")
        .setBindParameter("any")
        .setSortable(true)
        .build();

    boolean actual = sut.isSortable();

    assertTrue(actual);
  }

  @Test
  public void isSortable_isSortableNotSupplied_falseReturned() {
    ColumnDefinition sut = new ColumnDefinition.Builder()
        .setId("any")
        .setDisplayName("any")
        .setBindParameter("any")
        .build();

    boolean actual = sut.isSortable();

    assertFalse(actual);
  }

  @Test
  public void getMinWidth_valueSupplied_sameValueReturned() {
    int expected = 20;
    ColumnDefinition sut = new ColumnDefinition.Builder()
        .setId("any")
        .setDisplayName("any")
        .setBindParameter("any")
        .setMinWidth(expected)
        .build();

    assertEquals(expected, sut.getMinWidth());
  }

  @Test
  public void getMinWidth_valueNotSupplied_zeroReturned() {
    int expected = 0;
    ColumnDefinition sut = new ColumnDefinition.Builder()
        .setId("any")
        .setDisplayName("any")
        .setBindParameter("any")
        .build();

    assertEquals(expected, sut.getMinWidth());
  }
}

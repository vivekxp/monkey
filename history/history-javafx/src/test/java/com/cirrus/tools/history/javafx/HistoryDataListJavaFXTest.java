//  Copyright (c) 2020 Cirrus Logic, Inc and
//  Cirrus Logic International Semiconductor Ltd.  All rights reserved.
//
//  This software as well as any related documentation is furnished under
//  license and may only be used or copied in accordance with the terms of the
//  license.  The information in this file is furnished for informational use
//  only, is subject to change without notice, and should not be construed as
//  a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
//  liability for any errors or inaccuracies that may appear in this document
//  or any software that may be provided in association with this document.
//
//  Except as permitted by such license, no part of this document may be
//  reproduced, stored in a retrieval system, or transmitted in any form or by
//  any means without the express written consent of Cirrus Logic.
//
//  Warning
//    This software is specifically written for Cirrus Logic devices.
//    It may not be used with other devices.
//
//! @file   HistoryDataListTest.java
package com.cirrus.tools.history.javafx;

import com.cirrus.scs.studiolink.api.notifications.INotificationManager;
import com.cirrus.tools.history.api.HistoryConfig;
import com.cirrus.tools.history.impl.table.TableRowModel;

import javafx.concurrent.Task;

import org.junit.Rule;
import org.junit.Test;

import java.util.Collections;
import java.util.concurrent.ExecutorService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings("PMD.TooManyMethods") //Shouldn't apply to test classes
public class HistoryDataListJavaFXTest {
  @Rule public final JavaFXInitialisedRule jfxRule = new JavaFXInitialisedRule();

  @Test(expected = NullPointerException.class)
  public void constructor_historyConfigIsNull_throwsException() {
    ExecutorService executor = mock(ExecutorService.class);
    Preferences preferences = mock(Preferences.class);

    new HistoryDataListJavaFX(null, executor, preferences);
  }

  @Test(expected = NullPointerException.class)
  public void constructor_executorIsNull_throwsException() {
    HistoryConfig<TableRowModel> config = newMockHistoryConfig();
    Preferences preferences = mock(Preferences.class);

    new HistoryDataListJavaFX(config, null, preferences);
  }

  @Test
  public void constructor_preferencesIsNull_noExceptions() {
    ExecutorService executor = mock(ExecutorService.class);
    HistoryConfig<TableRowModel> config = newMockHistoryConfig();

    new HistoryDataListJavaFX(config, executor, null);
  }

  @Test
  public void constructor_preferencesNotNull_preferencesBoundToMaxItems() {
    //Arrange
    HistoryConfig<TableRowModel> config = newMockHistoryConfig();
    ExecutorService executor = mock(ExecutorService.class);

    Preferences preferences = new Preferences();
    preferences.maxItemsProperty().set(10);

    //Act
    HistoryDataListJavaFX sut = new HistoryDataListJavaFX(config, executor, preferences);
    sut.setMaxItems(20);

    //Assert
    assertEquals(20, preferences.maxItemsProperty().get());
  }

  @Test
  public void constructor_preferencesNotNull_preferencesBoundToMinRemoval() {
    //Arrange
    HistoryConfig<TableRowModel> config = newMockHistoryConfig();
    ExecutorService executor = mock(ExecutorService.class);

    Preferences preferences = new Preferences();
    preferences.minRemovalProperty().set(5);

    //Act
    HistoryDataListJavaFX sut = new HistoryDataListJavaFX(config, executor, preferences);
    sut.setMinRemoval(20);

    //Assert
    assertEquals(20, preferences.minRemovalProperty().get());
  }

  @Test(expected = NullPointerException.class)
  public void  startSubscribeToTopics_notificationManagerIsNull_throwsException() {
    HistoryConfig<TableRowModel> config = newMockHistoryConfig();
    ExecutorService executor = mock(ExecutorService.class);
    Preferences preferences = mock(Preferences.class);

    HistoryDataListJavaFX historyData = new HistoryDataListJavaFX(config, executor, preferences);
    historyData.startAsync(null);
  }

  @Test
  public void stopAsync_happyPath_voidTaskReturned() {
    ExecutorService mockExecutorService = mock(ExecutorService.class);
    HistoryDataListJavaFX historyData = new HistoryDataListJavaFX(mockExecutorService);

    Task<Void> t = historyData.stopAsync();

    assertNotNull(t);
  }

  @Test
  public void startAsync_happyPath_taskReturned() {
    INotificationManager mockNotificationManager = mock(INotificationManager.class);
    ExecutorService mockExecutorService = mock(ExecutorService.class);
    HistoryDataListJavaFX historyData = new HistoryDataListJavaFX(mockExecutorService);

    Task<Boolean> t = historyData.startAsync(mockNotificationManager);

    assertNotNull(t);
  }

  @Test
  public void getObservableList_happyPath_returnsNonNull() {
    ExecutorService mockExecutorService = mock(ExecutorService.class);
    HistoryDataListJavaFX historyData = new HistoryDataListJavaFX(mockExecutorService);

    assertNotNull(historyData.getObservableList());
  }

  @Test
  public void addRowToList_addRow_rowIsInList() {
    ExecutorService mockExecutorService = mock(ExecutorService.class);
    TableRowModel mockTableRow = mock(TableRowModel.class);
    HistoryDataListJavaFX historyData = new HistoryDataListJavaFX(mockExecutorService);

    historyData.addRowToList(mockTableRow);

    assertTrue(historyData.getObservableList().contains(mockTableRow));
  }

  @Test
  public void clearList_addRowAndClear_listIsEmpty() {
    ExecutorService mockExecutorService = mock(ExecutorService.class);
    TableRowModel mockTableRow = mock(TableRowModel.class);
    HistoryDataListJavaFX historyData = new HistoryDataListJavaFX(mockExecutorService);

    historyData.addRowToList(mockTableRow);
    historyData.clearList();

    assertTrue(historyData.getObservableList().isEmpty());
  }

  @Test
  public void getHistoryDataList_happyPath_returnsNonNull() {
    ExecutorService mockExecutorService = mock(ExecutorService.class);
    HistoryDataListJavaFX historyData = new HistoryDataListJavaFX(mockExecutorService);

    assertNotNull(historyData.getHistoryDataList());
  }

  /*
   * Unit tests for:
   * - getColumnManager()
   * - setDisplayButtonClickHandler()
   * - clearTable()
   * to be added as part of:
   * https://tracker.cirrus.com/browse/SCSSTU-897
   *
   * These methods are currently tested via integration tests.
   */

  @SuppressWarnings("unchecked") //Mocks are not generic so no choice but to cast
  private HistoryConfig<TableRowModel> newMockHistoryConfig() {
    HistoryConfig<TableRowModel> mockConfig = mock(HistoryConfig.class);
    when(mockConfig.getTopics()).thenReturn(Collections.singleton("topic"));
    return mockConfig;
  }
}

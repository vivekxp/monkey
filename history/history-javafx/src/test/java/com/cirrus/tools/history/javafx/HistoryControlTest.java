//  Copyright (c) 2020 Cirrus Logic, Inc and
//  Cirrus Logic International Semiconductor Ltd.  All rights reserved.
//
//  This software as well as any related documentation is furnished under
//  license and may only be used or copied in accordance with the terms of the
//  license.  The information in this file is furnished for informational use
//  only, is subject to change without notice, and should not be construed as
//  a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
//  liability for any errors or inaccuracies that may appear in this document
//  or any software that may be provided in association with this document.
//
//  Except as permitted by such license, no part of this document may be
//  reproduced, stored in a retrieval system, or transmitted in any form or by
//  any means without the express written consent of Cirrus Logic.
//
//  Warning
//    This software is specifically written for Cirrus Logic devices.
//    It may not be used with other devices.
//
//! @file   HistoryControlTest.java
//! @brief
package com.cirrus.tools.history.javafx;

import com.cirrus.tools.controls.Theme;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class HistoryControlTest {
  @Rule
  public JavaFXThreadingRule jfxRule = new JavaFXThreadingRule();

  @Test
  public void constructor_paramsNotNull_noExceptions() {
    newHistoryControlWithMockedMembers();
  }

  @Test(expected = NullPointerException.class)
  public void constructor_dataListIsNull_throwsException() {
    ObjectProperty<Theme> themeProperty = newMockThemeProperty();
    new HistoryControl(null, themeProperty);
  }

  @Test(expected = NullPointerException.class)
  public void constructor_themePropertyIsNull_throwsException() {
    new HistoryControl(FXCollections.observableArrayList(), null);
  }

  @Test
  public void constructor_themeAndDataSource_themeAndDataSet() {
    HistoryControl sut = new HistoryControl(FXCollections.observableArrayList(),
        new SimpleObjectProperty<>(Theme.LIGHT));

    assertEquals(Theme.LIGHT, sut.getTheme());
  }

  @Test
  public void themeProperty_themeIsDark_returnsDarkTheme() {
    HistoryControl sut = new HistoryControl(FXCollections.observableArrayList(),
        new SimpleObjectProperty<>(Theme.DARK));

    sut.setTheme(Theme.LIGHT);
    assertEquals(Theme.LIGHT, sut.themeProperty().get());
  }

  @Test
  public void createDefaultSkin_happyFlow_skinIsCreated() {
    HistoryControl sut = new HistoryControl(FXCollections.observableArrayList(),
        new SimpleObjectProperty<>(Theme.DARK));

    assertNotNull(sut.createDefaultSkin());
  }

  @Test
  public void createDefaultSkin_happyFlow_skinIsCreatedVGrowSetToAlways() {
    HistoryControl sut = new HistoryControl(FXCollections.observableArrayList(),
        new SimpleObjectProperty<>(Theme.DARK));

    sut.createDefaultSkin();
    assertEquals(Priority.ALWAYS, VBox.getVgrow(sut));
  }

  private HistoryControl newHistoryControlWithMockedMembers() {
    return new HistoryControl(FXCollections.observableArrayList(), newMockThemeProperty());
  }

  @SuppressWarnings("unchecked")  //Generic assignment
  private ObjectProperty<Theme> newMockThemeProperty() {
    return mock(ObjectProperty.class);
  }
}

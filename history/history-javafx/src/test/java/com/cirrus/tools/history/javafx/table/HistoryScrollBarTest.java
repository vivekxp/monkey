/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file HistoryScrollBarTest.java
 */

package com.cirrus.tools.history.javafx.table;

import com.cirrus.tools.controls.CirrusTableView;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.javafx.JavaFXInitialisedRule;

import javafx.collections.ObservableList;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TableView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("PMD.TooManyMethods") //Shouldn't apply to test classes
@RunWith(MockitoJUnitRunner.class)
public class HistoryScrollBarTest {
  private static final int TABLE_VIEW_LIST_SIZE = 100;
  @Rule
  public final JavaFXInitialisedRule jfxRule = new JavaFXInitialisedRule();

  @Spy
  private TableView<TableRowModel> historyTable;

  private ScrollBar scrollBar;
  private HistoryScrollBar testSubject;

  @Before
  @SuppressWarnings("unchecked") //Mocks are not generic so no choice but to cast
  public void setUp() {
    ObservableList<TableRowModel> list = mock(ObservableList.class);
    when(list.size()).thenReturn(TABLE_VIEW_LIST_SIZE);

    historyTable.setItems(list);

    scrollBar = new ScrollBar();
    testSubject = new HistoryScrollBar(historyTable, scrollBar);
  }

  @Test(expected = NullPointerException.class)
  public void constructor_whenHistoryTableIsNull_shouldThrowNullPointerException() {
    new HistoryScrollBar((CirrusTableView<TableRowModel>) null);
  }

  @Test(expected = NullPointerException.class)
  public void constructor_whenScrollbarIsNull_shouldThrowNullPointerException() {
    new HistoryScrollBar(historyTable, (ScrollBar) null);
  }

  @Test
  public void constructor_whenHistoryTableIsValid_shouldNotThrowAnyException() {
    new HistoryScrollBar(historyTable);
  }

  @Test
  public void constructor_whenScrollBarIsValid_shouldNotThrowAnyException() {
    new HistoryScrollBar(historyTable, scrollBar);
  }

  @Test
  public void scrollBarProperty_whenCalled_shouldReturnProjectObject() {
    assertNotNull(testSubject.scrollBarProperty());
  }

  @Test
  public void isPresent_whenPresent_shouldReturnTrue() {
    assertTrue(testSubject.isPresent());
  }

  @Test
  public void isPresent_whenNotPresent_shouldReturnFalse() {
    assertFalse(new HistoryScrollBar(historyTable).isPresent());
  }

  @Test
  public void isVisible_whenNotPresent_shouldReturnFalse() {
    assertFalse(new HistoryScrollBar(historyTable).isVisible());
  }

  @Test
  public void isVisible_whenPresentButNotVisible_shouldReturnFalse() {
    //Arrange
    scrollBar.setVisible(false);

    //Assert
    assertFalse(testSubject.isVisible());
  }

  @Test
  public void isVisible_whenPresentButAndVisible_shouldReturnTrue() {
    //Arrange
    scrollBar.setVisible(true);

    //Assert
    assertTrue(testSubject.isVisible());
  }

  @Test
  public void isAtBottom_whenVisibleAndValueIsMax_shouldReturnTrue() {
    //Arrange
    scrollBar.setVisible(true);
    scrollBar.setValue(scrollBar.getMax());

    //Assert
    assertTrue(testSubject.isAtBottom());
  }

  @Test
  public void isAtBottom_whenNotVisibleButValueIsMax_shouldReturnFalse() {
    //Arrange
    scrollBar.setVisible(false);
    scrollBar.setValue(scrollBar.getMax());

    //Assert
    assertFalse(testSubject.isAtBottom());
  }

  @Test
  public void isAtBottom_whenVisibleButValueIsNotMax_shouldReturnFalse() {
    //Arrange
    scrollBar.setVisible(true);
    scrollBar.setValue(scrollBar.getMax() - 1);

    //Assert
    assertFalse(testSubject.isAtBottom());
  }

  @Test
  public void scrollToBottom_whenVisible_shouldScrollToLastItemInList() {
    //Arrange
    scrollBar.setVisible(true);

    //Act
    testSubject.scrollToBottom();

    //Assert
    verify(historyTable).scrollTo(TABLE_VIEW_LIST_SIZE - 1);
  }

  @Test
  public void scrollToBottom_whenNotVisible_shouldNotScrollToLastItemInList() {
    //Arrange
    scrollBar.setVisible(false);

    //Act
    testSubject.scrollToBottom();

    //Assert
    verify(historyTable, never()).scrollTo(anyInt());
  }

  @Test
  public void scrollToValue_whenVisible_shouldSetGivenValue() {
    //Given
    double scrollValue = 0.5;

    //Arrange
    scrollBar.setVisible(true);
    scrollBar.setValue(0);

    //Act
    testSubject.scrollToValueForTest(scrollValue);

    //Assert
    assertEquals(scrollValue, scrollBar.getValue(), 0);
  }

  @Test
  public void scrollToValue_whenNotVisible_shouldNotSetGivenValue() {
    //Given
    double scrollValue = 0.5;

    //Arrange
    scrollBar.setVisible(false);
    scrollBar.setValue(0);

    //Act
    testSubject.scrollToValueForTest(scrollValue);

    //Assert
    assertNotEquals(scrollValue, scrollBar.getValue(), 0);
  }
}

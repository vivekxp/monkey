/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file PreferencesTest.java
 */
package com.cirrus.tools.history.javafx;

import com.cirrus.tools.history.javafx.table.ColumnPreferences;
import com.cirrus.tools.history.javafx.table.definition.ColumnDefinitions;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import org.junit.Test;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;

@SuppressWarnings("PMD.TooManyMethods") //Shouldn't apply to test classes
public class PreferencesTest {

  @Test
  public void defaultConstructor_happyPath_maxItemsPropertyDefined() {
    Preferences sut = new Preferences();

    assertNotNull(sut.maxItemsProperty());
  }

  @Test
  public void defaultConstructor_happyPath_minRemovalPropertyDefined() {
    Preferences sut = new Preferences();

    assertNotNull(sut.minRemovalProperty());
  }

  @Test
  public void defaultConstructor_happyPath_oneColumnPreferenceForEachDefinedColumn() {
    //Arrange and Act
    Preferences sut = new Preferences();

    //Assert
    Set<String> expected = getSortedSetOfAllDefinedColumnIds();
    Set<String> actual = getSortedSetOfAllColumnPreferenceIds(sut);

    assertEquals(expected, actual);
  }

  @Test(expected = NullPointerException.class)
  public void constructorWithParams_maxItemsIsNull_nullPointerExceptionThrown() {
    IntegerProperty minRemoval = mock(IntegerProperty.class);
    Map<String, ColumnPreferences> columnPrefs = Collections.emptyMap();

    new Preferences(null, minRemoval, columnPrefs);
  }

  @Test(expected = NullPointerException.class)
  public void constructorWithParams_minRemovalIsNull_nullPointerExceptionThrown() {
    IntegerProperty maxItems = mock(IntegerProperty.class);
    Map<String, ColumnPreferences> columnPrefs = Collections.emptyMap();

    new Preferences(maxItems, null, columnPrefs);
  }

  @Test(expected = NullPointerException.class)
  public void constructorWithParams_columnPrefsIsNull_nullPointerExceptionThrown() {
    IntegerProperty minRemoval = mock(IntegerProperty.class);
    IntegerProperty maxItems = mock(IntegerProperty.class);

    new Preferences(maxItems, minRemoval, null);
  }

  @Test
  public void constructorWithParams_validMaxItems_samePropertyReturned() {
    //Given
    IntegerProperty expected = mock(IntegerProperty.class);

    //Arrange
    IntegerProperty minRemoval = mock(IntegerProperty.class);
    Map<String, ColumnPreferences> columnPrefs = Collections.emptyMap();

    //Act
    Preferences sut = new Preferences(expected, minRemoval, columnPrefs);

    //Assert
    assertSame(expected, sut.maxItemsProperty());
  }

  @Test
  public void constructorWithParams_validMinRemoval_samePropertyReturned() {
    //Given
    IntegerProperty expected = mock(IntegerProperty.class);

    //Arrange
    IntegerProperty maxItems = mock(IntegerProperty.class);
    Map<String, ColumnPreferences> columnPrefs = Collections.emptyMap();

    //Act
    Preferences sut = new Preferences(maxItems, expected, columnPrefs);

    //Assert
    assertSame(expected, sut.minRemovalProperty());
  }

  @Test
  public void constructorWithParams_validColumnPrefsEntry_sameColumnPreferenceReturned() {
    //Given
    ColumnPreferences expected = mock(ColumnPreferences.class);

    //Arrange
    IntegerProperty maxItems = mock(IntegerProperty.class);
    IntegerProperty minRemoval = mock(IntegerProperty.class);

    Map<String, ColumnPreferences> map =
        Collections.singletonMap(ColumnDefinitions.SYSTEM_COLUMN_ID, expected);

    //Act
    Preferences sut = new Preferences(maxItems, minRemoval, map);

    //Assert
    assertSame(expected, sut.getColumnPreferences().get(ColumnDefinitions.SYSTEM_COLUMN_ID));
  }

  @Test
  public void constructorWithParams_columnPrefsHasIncorrectIds_setOfColumnPrefsEqualsColumnDefinitions() {
    //Arrange
    IntegerProperty maxItems = mock(IntegerProperty.class);
    IntegerProperty minRemoval = mock(IntegerProperty.class);
    ColumnPreferences colPrefs = mock(ColumnPreferences.class);
    Map<String, ColumnPreferences> map = Collections.singletonMap("invalidColumnId", colPrefs);

    //Act
    Preferences sut = new Preferences(maxItems, minRemoval, map);

    //Assert
    Set<String> expected = getSortedSetOfAllDefinedColumnIds();
    Set<String> actual = getSortedSetOfAllColumnPreferenceIds(sut);

    assertEquals(expected, actual);
  }

  @Test
  public void constructorWithParams_columnPrefsHasNullKeys_setOfColumnPrefsEqualsColumnDefinitions() {
    //Arrange
    IntegerProperty maxItems = mock(IntegerProperty.class);
    IntegerProperty minRemoval = mock(IntegerProperty.class);
    ColumnPreferences colPrefs = mock(ColumnPreferences.class);
    Map<String, ColumnPreferences> map = Collections.singletonMap(null, colPrefs);

    //Act
    Preferences sut = new Preferences(maxItems, minRemoval, map);

    //Assert
    Set<String> expected = getSortedSetOfAllDefinedColumnIds();
    Set<String> actual = getSortedSetOfAllColumnPreferenceIds(sut);

    assertEquals(expected, actual);
  }

  @Test
  public void constructorWithParams_columnPrefsHasNullValues_setOfColumnPrefsEqualsColumnDefinitions() {
    //Arrange
    IntegerProperty maxItems = mock(IntegerProperty.class);
    IntegerProperty minRemoval = mock(IntegerProperty.class);
    Map<String, ColumnPreferences> map = Collections.singletonMap("invalidColumnId", null);

    //Act
    Preferences sut = new Preferences(maxItems, minRemoval, map);

    //Assert
    Set<String> expected = getSortedSetOfAllDefinedColumnIds();
    Set<String> actual = getSortedSetOfAllColumnPreferenceIds(sut);

    assertEquals(expected, actual);
  }

  @Test
  public void getColumnPreferences_happyPath_notNull() {
    Preferences sut = new Preferences();

    Map<String, ColumnPreferences> actual = sut.getColumnPreferences();

    assertNotNull(actual);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void getColumnPreferences_happyPath_unmodifiable() {
    Preferences sut = new Preferences();
    Map<String, ColumnPreferences> actual = sut.getColumnPreferences();

    actual.clear();
  }

  @Test
  public void maxItemsProperty_happyPath_samePropertyReturned() {
    IntegerProperty maxItems = new SimpleIntegerProperty(100); //any value
    IntegerProperty minRemoval = mock(IntegerProperty.class);
    Map<String, ColumnPreferences> columnPrefs = Collections.emptyMap();

    Preferences sut = new Preferences(maxItems, minRemoval, columnPrefs);

    assertSame(maxItems, sut.maxItemsProperty());
  }

  @Test
  public void minRemovalProperty_happyPath_samePropertyReturned() {
    IntegerProperty maxItems = mock(IntegerProperty.class);
    IntegerProperty minRemoval = new SimpleIntegerProperty(100); //any value
    Map<String, ColumnPreferences> columnPrefs = Collections.emptyMap();

    Preferences sut = new Preferences(maxItems, minRemoval, columnPrefs);

    assertSame(minRemoval, sut.minRemovalProperty());
  }

  private Set<String> getSortedSetOfAllDefinedColumnIds() {
    return new TreeSet<>(new ColumnDefinitions().getColumnIds());
  }

  private Set<String> getSortedSetOfAllColumnPreferenceIds(Preferences prefs) {
    return new TreeSet<>(prefs.getColumnPreferences().keySet());
  }
}

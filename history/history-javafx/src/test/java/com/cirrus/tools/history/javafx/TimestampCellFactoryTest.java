/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TimestampCellFactoryTest.java
 */
package com.cirrus.tools.history.javafx;

import com.cirrus.tools.history.api.StringUtil;
import com.cirrus.tools.history.impl.table.TableRowModel;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableRow;
import org.junit.Rule;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TimestampCellFactoryTest {
  @Rule public JavaFXInitialisedRule jfxRule = new JavaFXInitialisedRule();

  @Test
  public void call_happyPath_returnsTableCell() {
    TimestampCellFactory sut = new TimestampCellFactory();

    TableCell<TableRowModel, Long> actual = sut.call(null); //Even null is valid

    assertNotNull(actual);
  }

  @Test
  public void tableCellUpdateItem_rowModelIsNull_textSetToEmptyString() {
    //Arrange
    TableRow<TableRowModel> tableRow = new TableRow<>();
    TimestampCellFactory factory = new TimestampCellFactory();

    TableCell<TableRowModel, Long> sut = factory.call(null); //Even null is valid
    sut.updateTableRow(tableRow);

    //Act
    long item = 99L;  //Any value will do
    boolean empty = false;
    invokeUpdateItem(sut, item, empty);

    //Assert
    String actual = sut.getText();
    assertEquals(StringUtil.EMPTY, actual);
  }

  @Test
  public void tableCellUpdateItem_elapsedTimeExists_textSetToTimestamp() {
    //Given
    String timestamp = "2020-07-03-16:04";

    //Arrange
    TableRowModel row = mock(TableRowModel.class);
    when(row.getTimestamp()).thenReturn(timestamp);

    TableRow<TableRowModel> tableRow = new TableRow<>();
    tableRow.setItem(row);

    TimestampCellFactory factory = new TimestampCellFactory();
    TableCell<TableRowModel, Long> sut = factory.call(null); //Even null is valid
    sut.updateTableRow(tableRow);
    sut.updateIndex(0);

    //Act
    invokeUpdateItem(sut, 99L, false);

    //Assert
    assertEquals(timestamp, sut.getText());
  }

  /**
   * The method we want to test is protected so for this test we invoke via reflection.
   */
  private void invokeUpdateItem(
      TableCell<TableRowModel, Long> tableCell, Long item, boolean isEmpty) {
    try {
      Method method = tableCell.getClass()
          .getDeclaredMethod("updateItem", Long.class, boolean.class);

      method.setAccessible(true);
      method.invoke(tableCell, item, isEmpty);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      throw new IllegalStateException("Could not invoke updateItem via reflection.", e);
    }
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file JavaFXThreadingRule.java
 */

package com.cirrus.tools.history.javafx;

import javafx.application.Platform;

import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.testfx.api.FxToolkit;

import java.util.concurrent.CountDownLatch;

/**
 * A JUnit {@link Rule} for running tests on the JavaFX thread and performing
 * JavaFX initialisation.  To include in your test case, add the following code:
 *
 * <pre>
 * {@literal @}Rule
 * public JavaFXThreadingRule jfxRule = new JavaFXThreadingRule();
 * </pre>
 *
 * @author Andy Till. See https://gist.github.com/andytill/3835914
 */
public class JavaFXThreadingRule implements TestRule {

  /**
   * Flag for setting up the JavaFX, we only need to do this once for all tests.
   */
  private static boolean jfxIsSetup;

  @Override
  public Statement apply(Statement statement, Description description) {
    return new OnJFXThreadStatement(statement);
  }

  private static class OnJFXThreadStatement extends Statement {
    private final Statement statement;
    private Throwable rethrownException;

    OnJFXThreadStatement(Statement aStatement) {
      statement = aStatement;
    }

    @Override
    @SuppressWarnings("PMD.AvoidCatchingThrowable")
    public void evaluate() throws Throwable {
      if (!jfxIsSetup) {
        FxToolkit.registerPrimaryStage();
        jfxIsSetup = true;
      }

      final CountDownLatch countDownLatch = new CountDownLatch(1);
      Platform.runLater(() -> {
        try {
          statement.evaluate();
        } catch (Throwable e) { // SUPPRESS CHECKSTYLE IllegalCatch
          rethrownException = e;
        }
        countDownLatch.countDown();
      });

      countDownLatch.await();

      // if an exception was thrown by the statement during evaluation,
      // then re-throw it to fail the test
      if (rethrownException != null) {
        throw rethrownException;
      }
    }
  }
}


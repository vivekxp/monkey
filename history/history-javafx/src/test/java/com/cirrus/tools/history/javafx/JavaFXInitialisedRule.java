/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file JavaFXInitialisedRule.java
 */
package com.cirrus.tools.history.javafx;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.testfx.api.FxToolkit;

public class JavaFXInitialisedRule implements TestRule {

  private static boolean isInitialised;

  @Override
  public Statement apply(Statement statement, Description description) {
    return new InitialisedStatement(statement);
  }

  private static class InitialisedStatement extends Statement {
    private final Statement statement;

    InitialisedStatement(Statement statement) {
      this.statement = statement;
    }

    @Override
    public void evaluate() throws Throwable {
      if (!isInitialised) {
        FxToolkit.registerPrimaryStage();
        isInitialised = true;
      }
      statement.evaluate();
    }
  }
}

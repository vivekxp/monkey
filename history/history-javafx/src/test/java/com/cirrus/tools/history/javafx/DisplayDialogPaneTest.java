/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file DisplayDialogPaneTest.java
 */

package com.cirrus.tools.history.javafx;

import com.cirrus.tools.controls.CirrusCheckBox;
import com.cirrus.tools.controls.Theme;
import com.cirrus.tools.history.impl.table.TableRowModel;

import javafx.beans.property.ObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.VBox;
import org.junit.Rule;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class DisplayDialogPaneTest {
  @Rule public JavaFXInitialisedRule jfxRule = new JavaFXInitialisedRule();

  @Test(expected = NullPointerException.class)
  public void constructor_columnListIsNull_nullPointerExceptionThrown() {
    new DisplayDialogPane(null, getNewMockObjectThemeProperty());
  }

  @Test(expected = NullPointerException.class)
  public void constructor_themePropertyIsNull_nullPointerExceptionThrown() {
    List<TableColumn<TableRowModel, ?>> columnList = getNewMockColumnList();
    new DisplayDialogPane(columnList, null);
  }

  @Test
  public void constructor_twoColumns_twoCheckboxesAddedInCorrectOrderWithCorrectNames() {
    //Given
    List<String> colNames = Arrays.asList("Col 1", "Col 2");

    //Arrange
    List<TableColumn<TableRowModel, ?>> columns = getNewListOfColumns(colNames);

    //Act
    DisplayDialogPane sut = new DisplayDialogPane(columns, getNewMockObjectThemeProperty());

    //Assert
    List<Node> actual = getCheckboxes(sut);

    //First two widget group children (zero and one) are CirrusLabels
    assertEquals("Unexpected checkbox count", colNames.size(), actual.size() - 2);
    assertEquals("Incorrect name1", colNames.get(0), ((CirrusCheckBox) actual.get(2)).getText());
    assertEquals("Incorrect name2", colNames.get(1), ((CirrusCheckBox) actual.get(3)).getText());
  }

  @Test
  public void constructor_columnIsVisible_checkboxIsChecked() {
    //Arrange
    TableColumn<TableRowModel, ?> col = new TableColumn<>("any col name");
    col.setVisible(true);

    //Act
    DisplayDialogPane sut = new DisplayDialogPane(Collections.singletonList(col), getNewMockObjectThemeProperty());

    //Assert
    CirrusCheckBox actual = getFirstCheckbox(sut);
    assertTrue(actual.getSelected());
  }

  @Test
  public void constructor_columnIsHidden_checkboxIsNotChecked() {
    //Arrange
    TableColumn<TableRowModel, ?> col = new TableColumn<>("any col name");
    col.setVisible(false);

    //Act
    DisplayDialogPane sut = new DisplayDialogPane(Collections.singletonList(col), getNewMockObjectThemeProperty());

    //Assert
    CirrusCheckBox actual = getFirstCheckbox(sut);
    assertFalse(actual.getSelected());
  }

  @SuppressWarnings("unchecked")  //Generic mock
  private ObjectProperty<Theme> getNewMockObjectThemeProperty() {
    return mock(ObjectProperty.class);
  }

  @SuppressWarnings("unchecked")  //Generic mock
  private List<TableColumn<TableRowModel, ?>> getNewMockColumnList() {
    return mock(List.class);
  }

  private CirrusCheckBox getFirstCheckbox(DisplayDialogPane pane) {
    //First two widget group children (zero and one) are CirrusLabels
    return (CirrusCheckBox) getCheckboxGroup(pane).getChildren().get(2);
  }

  private List<Node> getCheckboxes(DisplayDialogPane pane) {
    return getCheckboxGroup(pane).getChildren();
  }

  private VBox getCheckboxGroup(DisplayDialogPane pane) {
    try {
      Field field = pane.getClass().getDeclaredField("checkboxGroup");
      field.setAccessible(true);
      return (VBox) field.get(pane);
    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new IllegalStateException("Not found.", e);
    }
  }

  private List<TableColumn<TableRowModel, ?>> getNewListOfColumns(List<String> colNames) {
    Function<String, TableColumn<TableRowModel, ?>> colNameToColumn = TableColumn::new;
    return colNames.stream().map(colNameToColumn).collect(Collectors.toList());
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file AutoscrollHandlerTest.java
 */

package com.cirrus.tools.history.javafx;

import com.cirrus.tools.controls.CirrusTableView;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.javafx.table.HistoryScrollBar;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.ScrollBar;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.verification.VerificationMode;

import static com.cirrus.tools.history.javafx.AutoscrollHandler.CLOSEST_TO_MAX_SCROLL_VALUE;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("PMD.TooManyMethods") //Shouldn't apply to test classes
@RunWith(MockitoJUnitRunner.class)
public class AutoscrollHandlerTest {

  @Rule public final JavaFXInitialisedRule jfxRule = new JavaFXInitialisedRule();

  @Mock private HistoryScrollBar historyScrollBar;
  @Mock private ObjectProperty<ScrollBar> scrollBarObjectProperty;

  private CirrusTableView<TableRowModel> historyTable;
  private AutoscrollHandler testSubject;

  @Before
  public void setUp() {
    historyTable = new CirrusTableView<>();
    when(historyScrollBar.scrollBarProperty()).thenReturn(scrollBarObjectProperty);
    testSubject = new AutoscrollHandler(historyTable, historyScrollBar);
  }

  @Test(expected = NullPointerException.class)
  public void constructor_whenHistoryTableIsNull_shouldThrowNullPointerException() {
    new AutoscrollHandler(null);
  }


  @Test
  public void constructor_whenAllArgsAreValid_nothingThrown() {
    new AutoscrollHandler(historyTable);
  }

  @Test
  public void newEntryListener_whenItemAdded_shouldAttemptToScroll() {
    //Act
    historyTable.getItems().add(mock(TableRowModel.class));

    //Assert
    verifyScrollingAttempted();
  }

  @Test
  public void newEntryListener_whenItemRemoved_shouldNotAttemptToScroll() {
    //Arrange
    addAnEntryToTheTable(); //add entry so it can be removed
    reset(historyScrollBar);

    //Act
    historyTable.getItems().clear();

    //Assert
    verifyScrollingAttempted(never());
  }

  @Test
  public void newEntryListener_whenScrollbarAtBottomAndAutoScrollInactive_shouldDisableInbuiltAutoscroll() {
    //Arrange
    testSubject.setActive(false);
    when(historyScrollBar.isAtBottom()).thenReturn(true);

    //Act
    historyTable.getItems().add(mock(TableRowModel.class));

    //Assert
    verify(historyScrollBar).scrollToValueForTest(CLOSEST_TO_MAX_SCROLL_VALUE);
  }

  @Test
  public void newEntryListener_whenScrollbarNotAtBottomAndAutoScrollInactive_shouldNotDisableInbuiltAutoscroll() {
    //Arrange
    testSubject.setActive(false);
    when(historyScrollBar.isAtBottom()).thenReturn(false);

    //Act
    historyTable.getItems().add(mock(TableRowModel.class));

    //Assert
    verify(historyScrollBar, never()).scrollToValueForTest(anyDouble());
  }

  @Test
  public void newEntryListener_whenAutoScrollActive_shouldNotDisableInbuiltAutoscroll() {
    //Arrange
    testSubject.setActive(true);

    //Act
    historyTable.getItems().add(mock(TableRowModel.class));

    //Assert
    verify(historyScrollBar, never()).scrollToValueForTest(anyDouble());
  }

  @Test
  public void scrollbarAvailabilityListener_whenNotAvailable_shouldNotAttemptToScroll() {
    //Given
    ObjectProperty<ScrollBar> scrollBarObject = new SimpleObjectProperty<>();

    //Arrange
    when(historyScrollBar.scrollBarProperty()).thenReturn(scrollBarObject);
    reinitialiseScrollbar(historyScrollBar);
    reset(historyScrollBar);

    //Act
    scrollBarObject.set(null);

    //Assert
    verifyScrollingAttempted(never());
  }

  @Test
  public void scrollbarAvailabilityListener_whenAvailableButNotVisible_shouldNotAttemptToScroll() {
    //Given
    ObjectProperty<ScrollBar> scrollBarObject = new SimpleObjectProperty<>();
    ScrollBar scrollBar = new ScrollBar();
    BooleanProperty visibleProperty = scrollBar.visibleProperty();

    //Arrange
    when(historyScrollBar.scrollBarProperty()).thenReturn(scrollBarObject);
    reinitialiseScrollbar(historyScrollBar);
    scrollBarObject.set(scrollBar);
    visibleProperty.set(true);

    //Act
    visibleProperty.setValue(false);

    //Assert
    verifyScrollingAttempted(never());
  }

  @Test
  public void scrollbarAvailabilityListener_whenAvailableAndVisible_shouldAttemptToScroll() {
    //Given
    ObjectProperty<ScrollBar> scrollBarObject = new SimpleObjectProperty<>();
    ScrollBar scrollBar = new ScrollBar();
    BooleanProperty visibleProperty = scrollBar.visibleProperty();

    //Arrange
    when(historyScrollBar.scrollBarProperty()).thenReturn(scrollBarObject);
    reinitialiseScrollbar(historyScrollBar);
    scrollBarObject.set(scrollBar);
    visibleProperty.set(false);

    //Act
    visibleProperty.set(true);

    //Assert
    verifyScrollingAttempted();
  }

  @Test
  public void scrolling_whenScrollBarNotVisible_shouldNotProceed() {
    //Arrange
    when(historyScrollBar.isVisible()).thenReturn(false);

    //Act
    triggerScrolling();

    //Assert
    verify(historyScrollBar, never()).scrollToBottom();
  }

  @Test
  public void scrolling_notAtBottom_shouldScrollToBottom() {
    //Arrange
    when(historyScrollBar.isVisible()).thenReturn(true);
    when(historyScrollBar.isAtBottom()).thenReturn(false);

    //Act
    triggerScrolling();

    //Assert
    verify(historyScrollBar).scrollToBottom();
  }

  @Test
  public void scrolling_atBottom_shouldNotScroll() {
    //Arrange
    when(historyScrollBar.isVisible()).thenReturn(true);
    when(historyScrollBar.isAtBottom()).thenReturn(true);

    //Act
    triggerScrolling();

    //Assert
    verify(historyScrollBar, never()).scrollToBottom();
  }

  @Test
  public void scrolling_whenInactive_shouldNotProceed() {
    //Arrange
    testSubject.setActive(false);

    //Act
    triggerScrolling();

    //Assert
    verify(historyScrollBar, never()).isVisible();
  }

  @Test
  public void scrolling_whenBackActive_shouldProceed() {
    //Arrange
    testSubject.setActive(false);

    //Act
    testSubject.setActive(true);
    triggerScrolling();

    //Assert
    verify(historyScrollBar).isVisible();
  }

  @Test
  public void setActive_withActiveFalseAndScrollbarAtBottom_shouldDisableInbuiltAutoScroll() {
    //Arrange
    when(historyScrollBar.isAtBottom()).thenReturn(true);

    //Act
    testSubject.setActive(false);

    //Assert
    verify(historyScrollBar).scrollToValueForTest(CLOSEST_TO_MAX_SCROLL_VALUE);
  }

  @Test
  public void setActive_withActiveFalseAndScrollbarNotAtBottom_shouldNotDisableInbuiltAutoScroll() {
    //Arrange
    when(historyScrollBar.isAtBottom()).thenReturn(false);

    //Act
    testSubject.setActive(false);

    //Assert
    verify(historyScrollBar, never()).scrollToValueForTest(anyDouble());
  }

  @Test
  public void setActive_withActive_shouldNotDisableInbuiltAutoScroll() {
    //Act
    testSubject.setActive(true);

    //Assert
    verify(historyScrollBar, never()).scrollToValueForTest(anyDouble());
  }

  private void reinitialiseScrollbar(HistoryScrollBar configuredHistoryScrollBar) {
    new AutoscrollHandler(historyTable, configuredHistoryScrollBar);
  }

  private void verifyScrollingAttempted() {
    verifyScrollingAttempted(times(1));
  }

  private void verifyScrollingAttempted(VerificationMode verificationMode) {
    verify(historyScrollBar, verificationMode).isVisible();
  }

  private void triggerScrolling() {
    addAnEntryToTheTable();
  }

  private void addAnEntryToTheTable() {
    historyTable.getItems().add(mock(TableRowModel.class));
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ColumnDefinitionsTest.java
 */
package com.cirrus.tools.history.javafx.table.definition;

import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ColumnDefinitionsTest {

  @Test
  public void getColumns_happyPath_notNull() {
    ColumnDefinitions sut = new ColumnDefinitions();

    List<ColumnDefinition> actual = sut.getColumns();

    assertNotNull(actual);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void getColumns_happyPath_returnedListIsUnmodifiable() {
    ColumnDefinitions sut = new ColumnDefinitions();
    List<ColumnDefinition> actual = sut.getColumns();

    actual.clear();
  }

  @Test
  public void getColumnIds_happyPath_notNull() {
    ColumnDefinitions sut = new ColumnDefinitions();

    Set<String> actual = sut.getColumnIds();

    assertNotNull(actual);
  }

  @Test
  public void exists_columnIdIsNull_returnsFalse() {
    ColumnDefinitions sut = new ColumnDefinitions();

    boolean actual = sut.exists(null);

    assertFalse(actual);
  }

  @Test
  public void exists_columnIdIsInvalid_returnsFalse() {
    ColumnDefinitions sut = new ColumnDefinitions();

    boolean actual = sut.exists("wontFindMe");

    assertFalse(actual);
  }

  @Test
  public void exists_columnIdIsValid_returnsTrue() {
    ColumnDefinitions sut = new ColumnDefinitions();

    boolean actual = sut.exists(ColumnDefinitions.TIMESTAMP_COLUMN_ID);

    assertTrue(actual);
  }

  @Test
  public void get_columnIdIsNull_returnsEmptyOptional() {
    ColumnDefinitions sut = new ColumnDefinitions();

    Optional<ColumnDefinition> actual = sut.get(null);

    assertFalse(actual.isPresent());
  }

  @Test
  public void get_columnIdIsInvalid_returnsEmptyOptional() {
    ColumnDefinitions sut = new ColumnDefinitions();

    Optional<ColumnDefinition> actual = sut.get("wontFindMe");

    assertFalse(actual.isPresent());
  }

  @Test
  public void get_columnIdIsValid_returnsItem() {
    ColumnDefinitions sut = new ColumnDefinitions();

    Optional<ColumnDefinition> actual = sut.get(ColumnDefinitions.TIMESTAMP_COLUMN_ID);

    assertEquals(ColumnDefinitions.TIMESTAMP_COLUMN_ID, actual.get().getId());
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ColumnPreferencesTest.java
 */
package com.cirrus.tools.history.javafx.table;

import com.cirrus.tools.history.javafx.table.definition.ColumnDefinition;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class ColumnPreferencesTest {
  private static final double DELTA = 0.01;

  @Test(expected = NullPointerException.class)
  public void constructorWithDefinition_nullDefinition_nullPointerExceptionThrown() {
    new ColumnPreferences(null);
  }

  @Test
  public void constructorWithDefinition_happyPath_visibleByDefault() {
    ColumnDefinition colDef = new ColumnDefinition.Builder()
        .setId("any id")
        .setDisplayName("any display name")
        .setBindParameter("any bind")
        .build();

    ColumnPreferences sut = new ColumnPreferences(colDef);

    assertTrue(sut.visibleProperty().get());
  }

  @Test
  public void constructorWithDefinition_happyPath_preferredWidthEqualsDefinedMinWidth() {
    int expected = 100;

    //Arrange
    ColumnDefinition colDef = new ColumnDefinition.Builder()
        .setId("any id")
        .setDisplayName("any display name")
        .setBindParameter("any bind")
        .setMinWidth(expected)
        .build();

    //Act
    ColumnPreferences sut = new ColumnPreferences(colDef);

    //Assert
    double actual = sut.preferredWidthProperty().get();
    assertEquals(expected, actual, DELTA);
  }

  @Test(expected = NullPointerException.class)
  public void constructorWithProperties_preferredWidthPropertyIsNull_nullPointerExceptionThrown() {
    BooleanProperty visibleProperty = mock(BooleanProperty.class);

    new ColumnPreferences(null, visibleProperty);
  }

  @Test(expected = NullPointerException.class)
  public void constructorWithProperties_visiblePropertyIsNull_nullPointerExceptionThrown() {
    DoubleProperty prefWidthProperty = mock(DoubleProperty.class);

    new ColumnPreferences(prefWidthProperty, null);
  }

  @Test
  public void constructorWithProperties_visibleIsTrue_propertyValueIsTrue() {
    DoubleProperty prefWidthProperty = mock(DoubleProperty.class);
    BooleanProperty visibleProperty = new SimpleBooleanProperty(true);

    ColumnPreferences sut = new ColumnPreferences(prefWidthProperty, visibleProperty);

    assertTrue(sut.visibleProperty().get());
  }

  @Test
  public void constructorWithProperties_visibleIsFalse_propertyValueIsFalse() {
    DoubleProperty prefWidthProperty = mock(DoubleProperty.class);
    BooleanProperty visibleProperty = new SimpleBooleanProperty(false);

    ColumnPreferences sut = new ColumnPreferences(prefWidthProperty, visibleProperty);

    assertFalse(sut.visibleProperty().get());
  }

  @Test
  public void preferredWidthProperty_happyPath_samePropertyReturned() {
    DoubleProperty prefWidthProperty = new SimpleDoubleProperty(100); //any value
    BooleanProperty visibleProperty = mock(BooleanProperty.class);

    ColumnPreferences sut = new ColumnPreferences(prefWidthProperty, visibleProperty);

    assertSame(prefWidthProperty, sut.preferredWidthProperty());
  }

  @Test
  public void visibleProperty_happyPath_samePropertyReturned() {
    DoubleProperty prefWidthProperty = mock(DoubleProperty.class);
    BooleanProperty visibleProperty = new SimpleBooleanProperty(false);

    ColumnPreferences sut = new ColumnPreferences(prefWidthProperty, visibleProperty);

    assertSame(visibleProperty, sut.visibleProperty());
  }
}

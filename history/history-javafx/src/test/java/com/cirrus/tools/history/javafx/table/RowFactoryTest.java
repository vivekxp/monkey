/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file RowFactoryTest.java
 */

package com.cirrus.tools.history.javafx.table;

import com.cirrus.tools.history.api.StringUtil;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.javafx.JavaFXInitialisedRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RowFactoryTest {
  @Rule public final JavaFXInitialisedRule jfxRule = new JavaFXInitialisedRule();

  private RowFactory sut;

  @Before
  public void prepareRowFactory() {
    sut = new RowFactory();
    sut.setStyle("anything: other; than: empty;");
  }

  @Test
  public void updateItem_rowDoesNotHaveError_styleSetToEmpty() {
    TableRowModel rowModel = mock(TableRowModel.class);
    when(rowModel.rowHasError()).thenReturn(false);

    invokeUpdateItem(sut, rowModel);

    assertEquals(StringUtil.EMPTY, sut.getStyle());
  }

  @Test
  public void updateItem_rowIsNull_styleSetToEmpty() {
    invokeUpdateItem(sut, null);

    assertEquals(StringUtil.EMPTY, sut.getStyle());
  }

  @Test
  public void updateItem_rowHasError_errorStyleSet() {
    TableRowModel rowModel = mock(TableRowModel.class);
    when(rowModel.rowHasError()).thenReturn(true);

    invokeUpdateItem(sut, rowModel);

    assertEquals(RowFactory.ERROR_STYLE, sut.getStyle());
  }

  /**
   * The method we want to test is protected so for this test we invoke via reflection.
   */
  private void invokeUpdateItem(RowFactory rowFactory, TableRowModel rowModel) {
    try {
      Method method = rowFactory.getClass()
          .getDeclaredMethod("updateItem", TableRowModel.class, boolean.class);

      method.setAccessible(true);
      method.invoke(rowFactory, rowModel, false); //Styles only apply when true
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      throw new IllegalStateException("Could not invoke updateItem via reflection.", e);
    }
  }
}

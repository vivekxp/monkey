/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file HistoryDataList.java
 */
package com.cirrus.tools.history.javafx;

import com.cirrus.scs.studiolink.api.notifications.INotificationManager;
import com.cirrus.tools.history.api.HistoryConfig;

import com.cirrus.tools.history.impl.HistoryDataList;
import com.cirrus.tools.history.impl.table.DefaultConfig;
import com.cirrus.tools.history.impl.table.TableRowModel;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;

import java.util.Objects;
import java.util.concurrent.ExecutorService;

/**
 * JavaFX wrapper around the HistoryDataList class.
 */
public class HistoryDataListJavaFX {

  private final IntegerProperty maxItemsProperty = new SimpleIntegerProperty();
  private final IntegerProperty minRemovalProperty = new SimpleIntegerProperty();

  private final ExecutorService executorService;
  private final HistoryDataList historyDataList;
  private final ObservableList<TableRowModel> list;

  /**
   * Constructor with a default history config and no preferences
   *
   * @param executorService Executor service to use for starting and stopping
   *                        notification manager
   */
  public HistoryDataListJavaFX(ExecutorService executorService) {
    this(new DefaultConfig(), executorService, null);
  }

  /**
   * Constructor with a default history config
   *
   * @param executorService Executor service to use for starting and stopping
   *                        notification manager
   * @param preferences     Can be null if no persistence of preferences is required
   */
  public HistoryDataListJavaFX(ExecutorService executorService, Preferences preferences) {
    this(new DefaultConfig(), executorService, preferences);
  }

  /**
   * Constructor
   *
   * @param historyConfig   History config to set history with
   * @param executorService Executor service to use for starting and stopping
   *                        notification manager
   * @param preferences     Can be null if no persistence of preferences is required
   */
  public HistoryDataListJavaFX(HistoryConfig<TableRowModel> historyConfig, ExecutorService executorService,
      Preferences preferences) {
    this.executorService = Objects.requireNonNull(executorService);
    list = FXCollections.observableArrayList();
    historyDataList = new HistoryDataList(list, Platform::runLater, historyConfig);
    bindMaxItemsToPreferences(preferences);
    bindMinRemovalToPreferences(preferences);
  }

  private void bindMaxItemsToPreferences(Preferences preferences) {
    if (preferences == null) {
      return;
    }

    IntegerProperty maxItemsPref = preferences.maxItemsProperty();
    if (maxItemsPref == null) {
      return;
    }

    maxItemsProperty.bindBidirectional(maxItemsPref);
    historyDataList.setMaxItems(maxItemsPref.get());

    maxItemsProperty.addListener((observable, oldValue, newValue) -> historyDataList.setMaxItems(newValue.intValue()));
  }

  private void bindMinRemovalToPreferences(Preferences preferences) {
    if (preferences == null) {
      return;
    }

    IntegerProperty minRemovalPref = preferences.minRemovalProperty();
    if (minRemovalPref == null) {
      return;
    }

    minRemovalProperty.bindBidirectional(minRemovalPref);
    historyDataList.setMinRemoval(minRemovalPref.get());

    minRemovalProperty.addListener((observable, oldValue, newValue) ->
        historyDataList.setMinRemoval(newValue.intValue()));
  }

  /**
   * Start subscribing to topics on a background thread.
   *
   * @param notificationManager Notification manager to subscribe to.
   * @return a (submitted) Task that performs the topic subscription. The task
   *         will return false if no action was taken due to already being
   *         subscribed, else true.
   */
  public Task<Boolean> startAsync(INotificationManager notificationManager) {
    Objects.requireNonNull(notificationManager);
    Task<Boolean> startTask = new Task<Boolean>() {
      @Override
      protected Boolean call() {
        return historyDataList.start(notificationManager);
      }
    };

    executorService.submit(startTask);
    return startTask;
  }

  /**
   * Submits and returns a Task that frees up resources.
   */
  public Task<Void> stopAsync() {

    Task<Void> task = new Task<Void>() {
      @Override
      protected Void call() {
        historyDataList.stop();
        return null;
      }
    };

    executorService.submit(task);
    return task;
  }

  public ObservableList<TableRowModel> getObservableList() {
    return list;
  }

  public void addRowToList(TableRowModel row) {
    historyDataList.addRowToList(row);
  }

  public void clearList() {
    historyDataList.clearList();
  }

  public HistoryDataList getHistoryDataList() {
    return historyDataList;
  }

  /**
   * maxItems controls the maximum number of items in the History table.  Note that the actual
   * number of entries is equal to the sum of maxItems and minRemoval.
   */
  public void setMaxItems(int maxItems) {
    maxItemsProperty.set(maxItems);
  }

  /**
   * minRemoval is the number of items that will be removed from the History table when the
   * History table contains maxItems plus minRemoval entries.
   */
  public void setMinRemoval(int minRemoval) {
    minRemovalProperty.set(minRemoval);
  }
}

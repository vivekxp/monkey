/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file AutoscrollHandler.java
 */
package com.cirrus.tools.history.javafx;

import com.cirrus.tools.controls.CirrusTableView;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.javafx.table.HistoryScrollBar;
import javafx.collections.ListChangeListener;

import java.util.Objects;

public class AutoscrollHandler {

  //To disable in-built AuotScroll feature when scroll bar is at the bottom
  //we need to adjust scrollbar slightly such that current scroll value
  //is less than (but closet to) the max scroll value of 1.
  public static final double CLOSEST_TO_MAX_SCROLL_VALUE = 0.9999999999999999;

  private final CirrusTableView<TableRowModel> historyTable;
  private final HistoryScrollBar historyScrollBar;
  private boolean active = true;

  /**
   * Sets up AutoscrollHandler and registers all configured Observers
   *
   * @param historyTable    instance of historyTable
   */
  public AutoscrollHandler(CirrusTableView<TableRowModel> historyTable) {
    this(historyTable, new HistoryScrollBar(historyTable));
  }

  public void setActive(boolean active) {
    this.active = active;
    disableInBuiltAutoScrollingIfNecessary();
  }

  //Required for testing
  protected AutoscrollHandler(CirrusTableView<TableRowModel> historyTable,
      HistoryScrollBar historyScrollBar) {
    this.historyTable = Objects.requireNonNull(historyTable);
    this.historyScrollBar = Objects.requireNonNull(historyScrollBar);

    addNewEntryListener();
    addScrollbarVisibilityListener();
  }

  private void addNewEntryListener() {
    historyTable.getItems().addListener(
        (ListChangeListener<TableRowModel>) change -> {
          disableInBuiltAutoScrollingIfNecessary();
          boolean changed = change.next();
          if (changed && change.wasAdded()) {
            scrollIfNecessary();
          }
        });
  }

  private void addScrollbarVisibilityListener() {
    historyScrollBar.scrollBarProperty().addListener((observable, oldValue, newValue) -> {
      if (newValue != null) { //checking if scrollbar is available
        newValue.visibleProperty().addListener((observable1, oldVisibleValue, newVisibleValue) -> {
          if (newVisibleValue) { // checking if scrollbar is visible
            scrollIfNecessary();
          }
        });
      }
    });
  }

  private synchronized void scrollIfNecessary() {
    if (active && historyScrollBar.isVisible() && !historyScrollBar.isAtBottom()) {
      historyScrollBar.scrollToBottom();
    }
  }

  //This is necessary to disable in-built auto-scroll feature of tableview
  private void disableInBuiltAutoScrollingIfNecessary() {
    if (!active && historyScrollBar.isAtBottom()) {
      historyScrollBar.scrollToValueForTest(CLOSEST_TO_MAX_SCROLL_VALUE);
    }
  }
}

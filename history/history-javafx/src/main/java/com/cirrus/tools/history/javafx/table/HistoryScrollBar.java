/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file HistoryScrollBar.java
 */
package com.cirrus.tools.history.javafx.table;

import com.cirrus.tools.history.impl.table.TableRowModel;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TableView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Glorified wrapper for vertical scrollbar of HistoryTable
 */
public class HistoryScrollBar {

  private static final String SELECTOR_SCROLL_BAR = ".scroll-bar";
  private static final Logger LOG = LoggerFactory.getLogger(HistoryScrollBar.class);
  private final TableView<TableRowModel> historyTable;
  private final ObjectProperty<ScrollBar> scrollBar = new SimpleObjectProperty<>();
  private final ListChangeListener<TableRowModel> lookupListener = c -> lookupScrollBarNode();

  /**
   * Constructor to find and wrap Vertical scrollbar of HistoryTable as soon as it's available
   *
   * @param historyTable instance of historyTable to wrap the scrollbar for
   */
  public HistoryScrollBar(TableView<TableRowModel> historyTable) {
    this.historyTable = Objects.requireNonNull(historyTable);
    // will try to find scrollbar every time row is added until it's found
    historyTable.getItems().addListener(lookupListener);
  }

  /**
   * Constructor with direct reference to scrollbar
   *
   * @param scrollBar instance of scrollbar to wrap
   */
  public HistoryScrollBar(TableView<TableRowModel> historyTable, ScrollBar scrollBar) {
    // Although direct reference to the scrollbar is provided we still need reference to historyTable to scroll to the
    // top and bottom
    this.historyTable = historyTable;
    this.scrollBar.set(Objects.requireNonNull(scrollBar));
  }

  /**
   * To get access to scrollbar property
   *
   * @return reference to scrollBar Property
   */
  public ObjectProperty<ScrollBar> scrollBarProperty() {
    return scrollBar;
  }

  /**
   * Checks if scrollBar has been found
   *
   * @return boolean True if scrollbar is Present else False
   */
  public boolean isPresent() {
    return scrollBar.get() != null;
  }

  /**
   * Checks if scrollBar is present and visible
   *
   * @return boolean True if scrollbar is found and visible else False
   */
  public boolean isVisible() {
    return isPresent() && scrollBar.get().isVisible();
  }

  /**
   * Checks if scrollbar is visible and at the bottom
   *
   * @return boolean True if visible and at the bottom else False
   */
  public synchronized boolean isAtBottom() {
    return isVisible() && (scrollBar.get().getValue() == scrollBar.get().getMax());
  }

  /**
   * Scrolls down to the bottom
   */
  public synchronized void scrollToBottom() {
    if (!isVisible()) {
      return;
    }
    // Using the TableViews scrollTo method rather than setting the scroll bar manually due to an issue seen
    // when integrating into SCS, SCSSTU-1409
    historyTable.scrollTo(historyTable.getItems().size() - 1);
  }

  /**
   * Scrolls to given value, used for testing only
   * scrollToTop and scrollToBottom saw issues when integrated into SCS when directly setting the value of the
   * scrollbar. We are unable to verify if those issues are also present for this method and therefore have
   * made it a test interface.
   * @param value double value that cannot be Null
   */
  public synchronized void scrollToValueForTest(double value) {
    if (!isVisible()) {
      return;
    }

    scrollBar.get().setValue(value);
  }

  private void lookupScrollBarNode() {
    LOG.trace("Looking for scroll bar...");
    Set<Node> scrollBarNodes = historyTable.lookupAll(SELECTOR_SCROLL_BAR);

    Optional<Node> scrollBarNode = scrollBarNodes.stream()
        .filter(node -> ((ScrollBar) node).getOrientation().equals(Orientation.VERTICAL))
        .findFirst();

    if (scrollBarNode.isPresent()) {
      LOG.trace("Scroll bar found. Won't look for it again.");
      scrollBar.set((ScrollBar) scrollBarNode.get());
      historyTable.getItems().removeListener(lookupListener);
    } else {
      LOG.trace("Scrollbar not found. Will try again next time.");
    }
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ColumnPreferences.java
 */
package com.cirrus.tools.history.javafx.table;

import com.cirrus.tools.history.javafx.table.definition.ColumnDefinition;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.util.Objects;

/**
 * Container for JavaFX properties that represent user preferences for a given column.  Hosts
 * should bind to these to be notified when the values change.
 */
public class ColumnPreferences {
  private final DoubleProperty preferredWidthProperty;
  private final BooleanProperty visibleProperty;

  /**
   * Properties will be set to defaults defined in ColumnDefinition.  Visibility will be true by
   * default.
   */
  public ColumnPreferences(ColumnDefinition definition) {
    this(
        new SimpleDoubleProperty(definition.getMinWidth()),
        new SimpleBooleanProperty(true));

    Objects.requireNonNull(definition);
  }

  /**
   * Instantiate column preferences using the supplied properties.
   * @param preferredWidthProperty property representing the preferred width of the column.
   * @param visibleProperty property representing whether the column is visible or not.
   * @throws NullPointerException if any parameter is null.
   */
  public ColumnPreferences(DoubleProperty preferredWidthProperty, BooleanProperty visibleProperty) {
    this.preferredWidthProperty = Objects.requireNonNull(preferredWidthProperty);
    this.visibleProperty = Objects.requireNonNull(visibleProperty);
  }

  public DoubleProperty preferredWidthProperty() {
    return preferredWidthProperty;
  }

  public BooleanProperty visibleProperty() {
    return visibleProperty;
  }
}

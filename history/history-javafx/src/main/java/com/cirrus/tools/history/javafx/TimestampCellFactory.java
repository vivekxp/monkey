//  Copyright (c) 2020 Cirrus Logic, Inc and
//  Cirrus Logic International Semiconductor Ltd.  All rights reserved.
//
//  This software as well as any related documentation is furnished under
//  license and may only be used or copied in accordance with the terms of the
//  license.  The information in this file is furnished for informational use
//  only, is subject to change without notice, and should not be construed as
//  a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
//  liability for any errors or inaccuracies that may appear in this document
//  or any software that may be provided in association with this document.
//
//  Except as permitted by such license, no part of this document may be
//  reproduced, stored in a retrieval system, or transmitted in any form or by
//  any means without the express written consent of Cirrus Logic.
//
//  Warning
//    This software is specifically written for Cirrus Logic devices.
//    It may not be used with other devices.
//
//! @file   TimestampColumn.java
package com.cirrus.tools.history.javafx;

import com.cirrus.tools.history.api.StringUtil;
import com.cirrus.tools.history.impl.table.TableRowModel;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;


public class TimestampCellFactory implements
    Callback<TableColumn<TableRowModel, Long>, TableCell<TableRowModel, Long>> {

  @Override
  public TableCell<TableRowModel, Long> call(TableColumn<TableRowModel, Long> param) {

    return new TableCell<TableRowModel, Long>() {

      @Override
      protected void updateItem(Long item, boolean empty) {
        super.updateItem(item, empty);

        // Get the formatted timestamp from the TableRowModel
        String displayText = StringUtil.EMPTY;
        TableRowModel row =  (TableRowModel) getTableRow().getItem();
        if (row != null) {
          displayText = row.getTimestamp();
        }
        setText(displayText);
      }
    };
  }
}


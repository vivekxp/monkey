/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file RowFactory.java
 */

package com.cirrus.tools.history.javafx.table;

import com.cirrus.tools.history.impl.table.TableRowModel;

import javafx.scene.control.TableRow;

public class RowFactory extends TableRow<TableRowModel> {
  public static final String ERROR_STYLE = "-fx-background-color: -cirrus-table-row-error";

  @Override
  protected void updateItem(TableRowModel item, boolean empty) {
    super.updateItem(item, empty);

    //The if statement below depends on this assertion
    assert !(empty && item != null && item.rowHasError()) : "empty is true AND row has error.";

    /*
     * The normal case is for there not to be an error so this check here allows us to
     * perform the minimum number of checks for this main use case by asserting above that empty
     * has no bearing on the styling decision.
     */
    if (item != null && item.rowHasError()) {
      setStyle(ERROR_STYLE);
    } else {
      setStyle(null);
    }
  }
}

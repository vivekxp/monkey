/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file DisplayDialogPane.java
 */

package com.cirrus.tools.history.javafx;

import com.cirrus.tools.controls.CirrusCheckBox;
import com.cirrus.tools.controls.Theme;
import com.cirrus.tools.history.impl.table.TableRowModel;

import javafx.beans.property.ObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class DisplayDialogPane extends HBox {
  private static final String DIALOG_FXML_PATH = "/fxml/DisplayDialogPane.fxml";

  @SuppressWarnings("unused") //Assigned by JavaFX
  @FXML private VBox checkboxGroup;

  /**
   * Constructor.
   */
  public DisplayDialogPane(List<TableColumn<TableRowModel, ?>> columns, ObjectProperty<Theme> themeProperty) {
    Objects.requireNonNull(columns);
    Objects.requireNonNull(themeProperty);

    loadFxml();
    addColumnCheckboxes(columns);
    themeProperty.addListener((observable, oldValue, newValue) -> setTheme(newValue));

    Theme theme = themeProperty.getValue();
    if (theme != null) {
      setTheme(theme);
    }
  }

  private void loadFxml() {
    FXMLLoader loader = new FXMLLoader(getClass().getResource(DIALOG_FXML_PATH));
    loader.setRoot(this);
    loader.setController(this);

    try {
      loader.load();
    } catch (IOException e) {
      throw new IllegalStateException("Could not load DisplayDialogPane FXML.", e);
    }
  }

  private void setTheme(Theme t) {
    getStylesheets().setAll(t.getCssFilenames());
  }

  private void addColumnCheckboxes(List<TableColumn<TableRowModel, ?>> columns) {
    columns.forEach(col -> {
      CirrusCheckBox cbx = new CirrusCheckBox(col.getText());
      cbx.selectedProperty().bindBidirectional(col.visibleProperty());
      checkboxGroup.getChildren().add(cbx);
    });
  }

  /**
   * FXML initializer.
   * This method is called after all @FXML annotated members have been injected.
   */
  @FXML
  @SuppressWarnings({"PMD.UnusedPrivateMethod", "unused"}) //Method invoked via reflection
  private synchronized void initialize() {
    assert checkboxGroup != null : "checkboxGroup not initialized by JavaFX.";
  }
}

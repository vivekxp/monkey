/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ColumnDefinition.java
 */

package com.cirrus.tools.history.javafx.table.definition;

import com.cirrus.tools.history.api.StringUtil;

import java.util.Objects;

/**
 * Defines the default values for a given column.
 */
public final class ColumnDefinition {
  private final String id;
  private final String displayName;
  private final String bindParameter;
  private final boolean sortable;
  private final int minWidth;

  private ColumnDefinition(
      String id,
      String displayName,
      String bindParameter,
      boolean sortable,
      int minWidth) {

    Objects.requireNonNull(id);
    Objects.requireNonNull(displayName);
    Objects.requireNonNull(bindParameter);

    if (StringUtil.isTrimmedStringEmptyOrNull(id)) {
      throw new IllegalArgumentException("id is empty or whitespace.");
    }

    if (StringUtil.isTrimmedStringEmptyOrNull(displayName)) {
      throw new IllegalArgumentException("displayName is empty or whitespace.");
    }

    if (StringUtil.isTrimmedStringEmptyOrNull(bindParameter)) {
      throw new IllegalArgumentException("bindParameter is empty or whitespace.");
    }

    this.id = id;
    this.displayName = displayName;
    this.bindParameter = bindParameter;
    this.sortable = sortable;
    this.minWidth = minWidth;
  }

  public String getId() {
    return id;
  }

  public String getDisplayName() {
    return displayName;
  }

  public String getBindParameter() {
    return bindParameter;
  }

  public boolean isSortable() {
    return sortable;
  }

  public int getMinWidth() {
    return minWidth;
  }

  public static class Builder {
    private String id;
    private String displayName;
    private String bindParameter;
    private boolean isSortable;
    private int minWidth;

    public Builder setId(String identity) {
      this.id = identity;
      return this;
    }

    public Builder setDisplayName(String dn) {
      this.displayName = dn;
      return this;
    }

    public Builder setBindParameter(String bp) {
      this.bindParameter = bp;
      return this;
    }

    public Builder setSortable(boolean sortable) {
      this.isSortable = sortable;
      return this;
    }

    public Builder setMinWidth(int minWidth) {
      this.minWidth = minWidth;
      return this;
    }

    public ColumnDefinition build() {
      return new ColumnDefinition(id, displayName, bindParameter, isSortable, minWidth);
    }
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ColumnDefinitions.java
 */
package com.cirrus.tools.history.javafx.table.definition;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Defines the set of columns and all the default values for each column.
 */
public class ColumnDefinitions {

  public static final String ADDRESS_COLUMN_ID = "addressColumn";
  public static final String BUS_PROTOCOL_COLUMN_ID = "busProtocolColumn";
  public static final String DATA_COLUMN_ID = "dataColumn";
  public static final String DEVICE_ADDRESS_COLUMN_ID = "deviceAddressColumn";
  public static final String DEVICE_COLUMN_ID = "deviceColumn";
  public static final String OPERATION_COLUMN_ID = "operationColumn";
  public static final String PAGE_COLUMN_ID = "pageColumn";
  public static final String REGISTER_FIELD_NAME_COLUMN_ID = "registerFieldNameColumn";
  public static final String SYSTEM_COLUMN_ID = "systemColumn";
  public static final String TIMESTAMP_COLUMN_ID = "timestampColumn";
  public static final String TYPE_COLUMN_ID = "typeColumn";

  private static final String ADDRESS_DISPLAY_NAME = "Address";
  private static final String DEVICE_ADDRESS_DISPLAY_NAME = "Device Address";
  private static final String DEVICE_DISPLAY_NAME = "Device";
  private static final String BUS_PROTOCOL_DISPLAY_NAME = "Bus Protocol";
  private static final String DATA_DISPLAY_NAME = "Data";
  private static final String OPERATION_DISPLAY_NAME = "Op";
  private static final String PAGE_DISPLAY_NAME = "Page";
  private static final String REGISTER_FIELD_DISPLAY_NAME = "Register/Field";
  private static final String SYSTEM_DISPLAY_NAME = "System";
  private static final String TIMESTAMP_DISPLAY_NAME = "Timestamp";
  private static final String TYPE_DISPLAY_NAME = "Type";

  private static final String ADDRESS_COLUMN_BIND_PARAMETER = "address";
  private static final String BUS_PROTOCOL_COLUMN_BIND_PARAMETER = "busProtocol";
  private static final String DATA_COLUMN_BIND_PARAMETER = "data";
  private static final String DEVICE_ADDRESS_COLUMN_BIND_PARAMETER = "deviceAddress";
  private static final String DEVICE_COLUMN_BIND_PARAMETER = "deviceName";
  private static final String OPERATION_COLUMN_BIND_PARAMETER = "operation";
  private static final String PAGE_COLUMN_BIND_PARAMETER = "page";
  private static final String REGISTER_FIELD_COLUMN_BIND_PARAMETER = "registerOrFieldName";
  private static final String SYSTEM_COLUMN_BIND_PARAMETER = "systemName";
  private static final String TYPE_COLUMN_BIND_PARAMETER = "type";

  private static final int ADDRESS_COLUMN_MIN_WIDTH = 60;
  private static final int BUS_PROTOCOL_COLUMN_MIN_WIDTH = 90;
  private static final int DATA_COLUMN_MIN_WIDTH = 100;
  private static final int DEVICE_ADDRESS_COLUMN_MIN_WIDTH = 100;
  private static final int DEVICE_COLUMN_MIN_WIDTH = 120;
  private static final int OPERATION_COLUMN_MIN_WIDTH = 50;
  private static final int PAGE_COLUMN_MIN_WIDTH = 50;
  private static final int REGISTER_FIELD_COLUMN_MIN_WIDTH = 180;
  private static final int SYSTEM_COLUMN_MIN_WIDTH = 150;
  private static final int TYPE_COLUMN_MIN_WIDTH = 70;
  private static final int TIMESTAMP_COLUMN_MIN_WIDTH = 165;

  //Note that we bind timestamp column to elapsedTime field in TableRowModel
  private static final String TIMESTAMP_COLUMN_BIND_PARAMETER = "elapsedTime";

  private final List<ColumnDefinition> columns;

  /**
   * Constructor.
   */
  public ColumnDefinitions() {
    columns = Collections.unmodifiableList(Arrays.asList(
        getSystemColumnDefinition(),
        getDeviceColumnDefinition(),
        getAddressColumnDefinition(),
        getRegisterFieldNameColumnDefinition(),
        getTypeColumnDefinition(),
        getOperationColumnDefinition(),
        getDataColumnDefinition(),
        getPageColumnDefinition(),
        getBusProtocolColumnDefinition(),
        getDeviceAddressColumnDefinition(),
        getTimestampColumnDefinition()
    ));
  }

  /**
   * Gets all column definitions in the order they are defined.  The returned list is unmodifiable.
   */
  public List<ColumnDefinition> getColumns() {
    return columns;
  }

  public Set<String> getColumnIds() {
    return columns
        .stream()
        .map(ColumnDefinition::getId)
        .collect(Collectors.toSet());
  }

  private ColumnDefinition getTimestampColumnDefinition() {
    return new ColumnDefinition.Builder()
        .setId(TIMESTAMP_COLUMN_ID)
        .setDisplayName(TIMESTAMP_DISPLAY_NAME)
        .setBindParameter(TIMESTAMP_COLUMN_BIND_PARAMETER)
        .setSortable(false)
        .setMinWidth(TIMESTAMP_COLUMN_MIN_WIDTH)
        .build();
  }

  private ColumnDefinition getDeviceAddressColumnDefinition() {
    return new ColumnDefinition.Builder()
        .setId(DEVICE_ADDRESS_COLUMN_ID)
        .setDisplayName(DEVICE_ADDRESS_DISPLAY_NAME)
        .setBindParameter(DEVICE_ADDRESS_COLUMN_BIND_PARAMETER)
        .setSortable(false)
        .setMinWidth(DEVICE_ADDRESS_COLUMN_MIN_WIDTH)
        .build();
  }

  private ColumnDefinition getBusProtocolColumnDefinition() {
    return new ColumnDefinition.Builder()
        .setId(BUS_PROTOCOL_COLUMN_ID)
        .setDisplayName(BUS_PROTOCOL_DISPLAY_NAME)
        .setBindParameter(BUS_PROTOCOL_COLUMN_BIND_PARAMETER)
        .setSortable(false)
        .setMinWidth(BUS_PROTOCOL_COLUMN_MIN_WIDTH)
        .build();
  }

  private ColumnDefinition getPageColumnDefinition() {
    return new ColumnDefinition.Builder()
        .setId(PAGE_COLUMN_ID)
        .setDisplayName(PAGE_DISPLAY_NAME)
        .setBindParameter(PAGE_COLUMN_BIND_PARAMETER)
        .setSortable(false)
        .setMinWidth(PAGE_COLUMN_MIN_WIDTH)
        .build();
  }

  private ColumnDefinition getDataColumnDefinition() {
    return new ColumnDefinition.Builder()
        .setId(DATA_COLUMN_ID)
        .setDisplayName(DATA_DISPLAY_NAME)
        .setBindParameter(DATA_COLUMN_BIND_PARAMETER)
        .setSortable(false)
        .setMinWidth(DATA_COLUMN_MIN_WIDTH)
        .build();
  }

  private ColumnDefinition getOperationColumnDefinition() {
    return new ColumnDefinition.Builder()
        .setId(OPERATION_COLUMN_ID)
        .setDisplayName(OPERATION_DISPLAY_NAME)
        .setBindParameter(OPERATION_COLUMN_BIND_PARAMETER)
        .setSortable(false)
        .setMinWidth(OPERATION_COLUMN_MIN_WIDTH)
        .build();
  }

  private ColumnDefinition getTypeColumnDefinition() {
    return new ColumnDefinition.Builder()
        .setId(TYPE_COLUMN_ID)
        .setDisplayName(TYPE_DISPLAY_NAME)
        .setBindParameter(TYPE_COLUMN_BIND_PARAMETER)
        .setSortable(false)
        .setMinWidth(TYPE_COLUMN_MIN_WIDTH)
        .build();
  }

  private ColumnDefinition getRegisterFieldNameColumnDefinition() {
    return new ColumnDefinition.Builder()
        .setId(REGISTER_FIELD_NAME_COLUMN_ID)
        .setDisplayName(REGISTER_FIELD_DISPLAY_NAME)
        .setBindParameter(REGISTER_FIELD_COLUMN_BIND_PARAMETER)
        .setSortable(false)
        .setMinWidth(REGISTER_FIELD_COLUMN_MIN_WIDTH)
        .build();
  }

  private ColumnDefinition getAddressColumnDefinition() {
    return new ColumnDefinition.Builder()
        .setId(ADDRESS_COLUMN_ID)
        .setDisplayName(ADDRESS_DISPLAY_NAME)
        .setBindParameter(ADDRESS_COLUMN_BIND_PARAMETER)
        .setSortable(false)
        .setMinWidth(ADDRESS_COLUMN_MIN_WIDTH)
        .build();
  }

  private ColumnDefinition getDeviceColumnDefinition() {
    return new ColumnDefinition.Builder()
        .setId(DEVICE_COLUMN_ID)
        .setDisplayName(DEVICE_DISPLAY_NAME)
        .setBindParameter(DEVICE_COLUMN_BIND_PARAMETER)
        .setSortable(false)
        .setMinWidth(DEVICE_COLUMN_MIN_WIDTH)
        .build();
  }

  private ColumnDefinition getSystemColumnDefinition() {
    return new ColumnDefinition.Builder()
        .setId(SYSTEM_COLUMN_ID)
        .setDisplayName(SYSTEM_DISPLAY_NAME)
        .setBindParameter(SYSTEM_COLUMN_BIND_PARAMETER)
        .setSortable(false)
        .setMinWidth(SYSTEM_COLUMN_MIN_WIDTH)
        .build();
  }

  /**
   * Check to see if a column is defined for the given column ID.
   * @return true if columnId is a recognized column ID, else false.  False will be returned if
   * columnId is false.
   */
  public boolean exists(String columnId) {
    return columns.stream()
        .anyMatch(col -> col.getId().equalsIgnoreCase(columnId));
  }

  /**
   * Optional will be empty if columnId is invalid or null.
   */
  public Optional<ColumnDefinition> get(String columnId) {
    return columns.stream()
        .filter(col -> col.getId().equalsIgnoreCase(columnId))
        .findFirst();
  }
}

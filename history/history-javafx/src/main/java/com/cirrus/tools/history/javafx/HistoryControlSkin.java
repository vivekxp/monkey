//  Copyright (c) 2020 Cirrus Logic, Inc and
//  Cirrus Logic International Semiconductor Ltd.  All rights reserved.
//
//  This software as well as any related documentation is furnished under
//  license and may only be used or copied in accordance with the terms of the
//  license.  The information in this file is furnished for informational use
//  only, is subject to change without notice, and should not be construed as
//  a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
//  liability for any errors or inaccuracies that may appear in this document
//  or any software that may be provided in association with this document.
//
//  Except as permitted by such license, no part of this document may be
//  reproduced, stored in a retrieval system, or transmitted in any form or by
//  any means without the express written consent of Cirrus Logic.
//
//  Warning
//    This software is specifically written for Cirrus Logic devices.
//    It may not be used with other devices.
//
//! @file   HistoryControlSkin.java
package com.cirrus.tools.history.javafx;

import com.cirrus.tools.controls.CirrusButton;
import com.cirrus.tools.controls.CirrusCheckBox;
import com.cirrus.tools.controls.CirrusLabel;
import com.cirrus.tools.controls.CirrusPanel;
import com.cirrus.tools.controls.CirrusTableView;
import com.cirrus.tools.controls.Theme;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.javafx.table.ColumnPreferences;
import com.cirrus.tools.history.javafx.table.RowFactory;
import com.cirrus.tools.history.javafx.table.definition.ColumnDefinition;
import com.cirrus.tools.history.javafx.table.definition.ColumnDefinitions;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.util.Map;
import java.util.Objects;

public class HistoryControlSkin extends SkinBase<HistoryControl> {
  public static final String CLEAR_BUTTON_ID = "clearButton";
  public static final String DISPLAY_BUTTON_ID = "displayButton";
  public static final String NO_HISTORY_LABEL_ID = "noHistoryLabel";
  public static final String AUTOSCROLL_CHECKBOX_ID = "autoscrollCheckbox";

  private static final String NO_HISTORY_LABEL_TEXT = "No history";
  private static final String CLEAR_BUTTON_TEXT = "Clear";
  private static final int TOOLBAR_SPACING = 5;
  private static final Insets TOOLBAR_PADDING = new Insets(5, 5, 5, 5);
  private static final String CIRRUS_PANEL_SECTION_STYLE_CLASS = "cirrus-panel-section";
  private static final int BUTTON_PREF_WIDTH = 70;
  private static final String DISPLAY_BUTTON_TEXT = "Display";
  private static final Insets OUTER_PANEL_PADDING = new Insets(0, 0, 0, 0);
  private static final int OUTER_PANEL_MIN_HEIGHT = 0;
  private static final String AUTOSCROLL_CHECKBOX_TEXT = "Auto-Scroll";
  private static final int AVERAGE_TEXT_WIDTH = 10; // trail and error
  private static final int AUTOSCROLL_CHECKBOX_TOTAL_PADDING = 11;

  private final CirrusTableView<TableRowModel> historyTable;
  private final AutoscrollHandler autoscrollHandler;


  protected HistoryControlSkin(final HistoryControl control) {
    super(control);

    Preferences preferences =
        Objects.requireNonNull(getSkinnable().getPreferences(), "Preferences are null.");

    historyTable = createHistoryTable(preferences.getColumnPreferences());
    historyTable.setRowFactory(__ -> new RowFactory());
    CirrusLabel noHistoryLabel = new CirrusLabel(NO_HISTORY_LABEL_TEXT);
    noHistoryLabel.setId(NO_HISTORY_LABEL_ID);
    historyTable.setPlaceholder(noHistoryLabel);

    TableColumn<TableRowModel, Long> timestampColumn = lookupTimestampColumn(historyTable);
    timestampColumn.setCellFactory(new TimestampCellFactory());

    VBox mainVBox = new VBox();
    CirrusButton displayButton = new CirrusButton(DISPLAY_BUTTON_TEXT);

    setBackgroundStyle(getSkinnable().themeProperty().get());
    getSkinnable().themeProperty().addListener((observable, oldValue, newValue) -> setBackgroundStyle(newValue));

    getSkinnable().displayButtonClickHandlerProperty().addListener((observable, oldValue, newValue) ->
        setDisplayButtonAction(displayButton, newValue));
    setDisplayButtonAction(displayButton, getSkinnable().getDisplayButtonClickHandler());

    historyTable.setItems(getSkinnable().getHistoryDataList());
    autoscrollHandler = new AutoscrollHandler(historyTable);

    mainVBox.getChildren()
        .add(createToolbar(displayButton,
            __ -> getSkinnable().getHistoryDataList().clear(),
            getSkinnable().themeProperty(),
            (observable, oldValue, newValue) -> autoscrollHandler.setActive(newValue)));
    mainVBox.getChildren().add(historyTable);

    CirrusPanel outerPanel = createOuterPanel(getSkinnable().themeProperty());
    outerPanel.getChildren().add(mainVBox);
    getChildren().add(outerPanel);
    // Ensure the control always grows vertically when added to a pane.
    VBox.setVgrow(getNode(), Priority.ALWAYS);
  }

  private void setDisplayButtonAction(CirrusButton displayButton, Callback<DisplayDialogPane, Void> callback) {
    displayButton.setVisible(callback != null);
    displayButton.setOnAction(__ -> {
      if (callback != null) {
        DisplayDialogPane dialogPane = new DisplayDialogPane(historyTable.getColumns(), getSkinnable().themeProperty());
        callback.call(dialogPane);
      }
    });
  }

  private void setBackgroundStyle(Theme theme) {
    if (theme == Theme.DARK) {
      getSkinnable().setStyle("-fx-background-color: -battleship;");
    } else {
      getSkinnable().setStyle("-fx-background-color: -boltonskies;");
    }
  }

  private static CirrusTableView<TableRowModel> createHistoryTable(Map<String, ColumnPreferences> prefs) {
    CirrusTableView<TableRowModel> table = new CirrusTableView<>();
    table.getStylesheets().add(HistoryControlSkin.class.getResource("/css/table.css").toExternalForm());
    table.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
    VBox.setVgrow(table, Priority.ALWAYS);

    new ColumnDefinitions().getColumns().forEach(col ->
        createAndAddColumn(
            col,
            prefs,
            table));

    return table;
  }

  private static CirrusPanel createToolbar(CirrusButton displayButton,
      EventHandler<ActionEvent> clearButtonHandler, ObjectProperty<Theme> themeProperty,
      ChangeListener<Boolean> autoScrollCheckboxHandler) {
    CirrusPanel toolbar = new CirrusPanel();
    toolbar.setPadding(TOOLBAR_PADDING);
    toolbar.getStyleClass().add(CIRRUS_PANEL_SECTION_STYLE_CLASS);
    toolbar.setSpacing(TOOLBAR_SPACING);
    toolbar.setAlignment(Pos.CENTER_LEFT);
    toolbar.setFillHeight(false);
    toolbar.themeProperty().bindBidirectional(themeProperty);

    displayButton.setButtonStyle(CirrusButton.CirrusButtonStyle.SECONDARY);
    displayButton.prefWidthProperty().set(BUTTON_PREF_WIDTH);
    displayButton.setId(DISPLAY_BUTTON_ID);
    toolbar.getChildren().add(displayButton);

    CirrusButton clearButton = new CirrusButton(CLEAR_BUTTON_TEXT);
    clearButton.setButtonStyle(CirrusButton.CirrusButtonStyle.SECONDARY);
    clearButton.prefWidthProperty().set(BUTTON_PREF_WIDTH);
    clearButton.setOnAction(clearButtonHandler);
    clearButton.setId(CLEAR_BUTTON_ID);

    CirrusCheckBox autoscrollCheckbox = new CirrusCheckBox(AUTOSCROLL_CHECKBOX_TEXT);
    autoscrollCheckbox.selectedProperty().addListener(autoScrollCheckboxHandler);
    autoscrollCheckbox.setSelected(true);
    autoscrollCheckbox
        .setPadding(new Insets(0, 0, 0, AUTOSCROLL_CHECKBOX_TOTAL_PADDING - TOOLBAR_SPACING));
    autoscrollCheckbox.setMinWidth(AUTOSCROLL_CHECKBOX_TEXT.length() * AVERAGE_TEXT_WIDTH);
    autoscrollCheckbox.setId(AUTOSCROLL_CHECKBOX_ID);

    toolbar.getChildren().add(clearButton);
    toolbar.getChildren().add(autoscrollCheckbox);

    return toolbar;
  }

  private static CirrusPanel createOuterPanel(ObjectProperty<Theme> themeProperty) {
    CirrusPanel outerPanel = new CirrusPanel();
    outerPanel.setMinHeight(OUTER_PANEL_MIN_HEIGHT);
    outerPanel.setPadding(OUTER_PANEL_PADDING);
    outerPanel.getStyleClass().add(CIRRUS_PANEL_SECTION_STYLE_CLASS);
    outerPanel.themeProperty().bindBidirectional(themeProperty);
    VBox.setVgrow(outerPanel, Priority.ALWAYS);
    return outerPanel;
  }

  private static TableColumn<TableRowModel, String> createAndAddColumn(ColumnDefinition colDef,
      Map<String, ColumnPreferences> prefs, CirrusTableView<TableRowModel> table) {

    ColumnPreferences colPrefs = prefs.get(colDef.getId());
    assert colPrefs != null : "No preferences for column: " + colDef.getDisplayName();

    TableColumn<TableRowModel, String> col = new TableColumn<>(colDef.getDisplayName());
    col.visibleProperty().bindBidirectional(colPrefs.visibleProperty());
    bindColumnWidths(col, colPrefs);

    return addColumn(col, colDef, table);
  }

  /**
   * We cannot bind in both directions when it comes to column widths.  The actual column width
   * is read-only so we listen for changes to that property and apply them to the preferredWidth
   * property in preferences.  On start up, we apply the preferredWidth property from preferences
   * to the actual column width.
   */
  private static void bindColumnWidths(TableColumn<TableRowModel, String> col, ColumnPreferences colPrefs) {
    col.prefWidthProperty().bindBidirectional(colPrefs.preferredWidthProperty());

    col.widthProperty().addListener((observable, oldValue, newValue) ->
        colPrefs.preferredWidthProperty().setValue(newValue));
  }

  private static TableColumn<TableRowModel, String> addColumn(
      TableColumn<TableRowModel, String> column,
      ColumnDefinition colDef, CirrusTableView<TableRowModel> table) {
    column.setCellValueFactory(new PropertyValueFactory<>(colDef.getBindParameter()));
    column.setEditable(false);
    column.setSortable(colDef.isSortable());
    column.setMinWidth(colDef.getMinWidth());
    column.setId(colDef.getId());
    table.getColumns().add(column);
    return column;
  }

  @SuppressWarnings("unchecked")  //getColumns returns wildcard
  private static TableColumn<TableRowModel, Long> lookupTimestampColumn(CirrusTableView<TableRowModel> historyTable) {
    return (TableColumn<TableRowModel, Long>) historyTable
        .getColumns()
        .stream()
        .filter(col -> ColumnDefinitions.TIMESTAMP_COLUMN_ID.equalsIgnoreCase(col.getId()))
        .findFirst()
        .orElseThrow(() -> new IllegalStateException("Timestamp column not found."));
  }
}

//  Copyright (c) 2020 Cirrus Logic, Inc and
//  Cirrus Logic International Semiconductor Ltd.  All rights reserved.
//
//  This software as well as any related documentation is furnished under
//  license and may only be used or copied in accordance with the terms of the
//  license.  The information in this file is furnished for informational use
//  only, is subject to change without notice, and should not be construed as
//  a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
//  liability for any errors or inaccuracies that may appear in this document
//  or any software that may be provided in association with this document.
//
//  Except as permitted by such license, no part of this document may be
//  reproduced, stored in a retrieval system, or transmitted in any form or by
//  any means without the express written consent of Cirrus Logic.
//
//  Warning
//    This software is specifically written for Cirrus Logic devices.
//    It may not be used with other devices.
//
//! @file   HistoryControl.java
package com.cirrus.tools.history.javafx;

import com.cirrus.tools.controls.Theme;
import com.cirrus.tools.history.impl.table.TableRowModel;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.util.Callback;

import java.util.Objects;

/**
 * This is the main JavaFX control that the host should instantiate.
 */
public class HistoryControl extends Control {
  private final Preferences preferences;
  private final ObjectProperty<Theme> themeProperty;
  private final ObjectProperty<Callback<DisplayDialogPane, Void>> displayButtonClickHandler
      = new SimpleObjectProperty<>();

  private final ObservableList<TableRowModel> historyDataList;

  /**
   * A new instance of Preferences will be created that can be got later by calling
   * getPreferences();
   *
   * @param historyData   Data source to link control to, null if the data source
   *                      doesn't exist yet.
   * @param themeProperty Theme property to bind controls style to.
   */
  public HistoryControl(ObservableList<TableRowModel> historyData, ObjectProperty<Theme> themeProperty) {
    this(historyData, new Preferences(), themeProperty);
  }

  /**
   * Constructor
   *
   * @param historyData   Data source to link control to, null if the data source
   *                      doesn't exist yet.
   * @param preferences   Allows user preferences to be observed and persisted by the host.
   * @param themeProperty Theme property to bind controls style to.
   */
  public HistoryControl(ObservableList<TableRowModel> historyData,
      Preferences preferences, ObjectProperty<Theme> themeProperty) {
    this.themeProperty = Objects.requireNonNull(themeProperty);
    this.preferences = Objects.requireNonNull(preferences);
    this.historyDataList = Objects.requireNonNull(historyData);
  }

  public ObservableList<TableRowModel> getHistoryDataList() {
    return historyDataList;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Skin<HistoryControl> createDefaultSkin() {
    return new HistoryControlSkin(this);
  }

  @Override
  public String getUserAgentStylesheet() {
    return HistoryControl.class.getResource("/css/history-control.css").toExternalForm();
  }

  /**
   * Set the theme of the control
   *
   * @param theme Theme to set
   */
  public final void setTheme(Theme theme) {
    Objects.requireNonNull(theme);
    themeProperty.set(theme);
  }

  /**
   * Get the theme of the control
   *
   * @return Current theme of the control. Null if not set
   */
  public Theme getTheme() {
    return themeProperty.get();
  }

  /**
   * Get the Theme Property the control is bound too
   *
   * @return The theme property
   */
  public ObjectProperty<Theme> themeProperty() {
    return themeProperty;
  }

  public Callback<DisplayDialogPane, Void> getDisplayButtonClickHandler() {
    return displayButtonClickHandler.get();
  }

  public ObjectProperty<Callback<DisplayDialogPane, Void>> displayButtonClickHandlerProperty() {
    return displayButtonClickHandler;
  }

  /**
   * Used by the host to attach a click handler to the Display button. The handler
   * will be passed an instance of DisplayDialogPane.
   *
   * Note that each click will result in a new instance of DisplayDialogPane being
   * passed to the handler. This is also true of Register Map which dictates the
   * pattern here.
   */
  public void setDisplayButtonClickHandler(Callback<DisplayDialogPane, Void> clickHandler) {
    this.displayButtonClickHandler.set(Objects.requireNonNull(clickHandler));
  }

  public Preferences getPreferences() {
    return preferences;
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file Preferences.java
 */
package com.cirrus.tools.history.javafx;

import com.cirrus.tools.history.impl.table.TableRowConsumer;
import com.cirrus.tools.history.javafx.table.ColumnPreferences;

import com.cirrus.tools.history.javafx.table.definition.ColumnDefinition;
import com.cirrus.tools.history.javafx.table.definition.ColumnDefinitions;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Container for JavaFX properties that represent user preferences.  Hosts should bind to these
 * to be notified when the values change.
 */
public class Preferences {
  private final IntegerProperty maxItemsProperty;
  private final IntegerProperty minRemovalProperty;
  private final Map<String, ColumnPreferences> columnPreferences;

  /**
   * Preferences will be set to sensible defaults.
   */
  public Preferences() {
    maxItemsProperty = new SimpleIntegerProperty(TableRowConsumer.DEFAULT_MAX_NUMBER_OF_ITEMS);
    minRemovalProperty = new SimpleIntegerProperty(TableRowConsumer.DEFAULT_MAX_ITEMS_MIN_REMOVAL);
    columnPreferences = Collections.unmodifiableMap(newPreferencesMap());
  }

  /**
   * Instantiate preferences using the supplied properties and mapping of column IDs to column
   * preferences.
   *
   * 1. If the map contains column IDs for columns that do not exist, or the column ID is null, the
   * map entry is ignored.
   *
   * 2. If columns exist for which there is no entry in the map, or the mapped version is null, a
   * ColumnPreferences object will be created with sensible defaults.
   *
   * N.B. that due to the two 'rules' above, the final map of preferences controlled by this
   * class may not match the supplied map.  Therefore clients should always call
   * getColumnPreferences() to get the actual set of column preferences.
   *
   * @param maxItemsProperty property representing max number of items in history table.
   * @param minRemovalProperty property representing min number of items to remove from history table.
   * @param columnPreferences a map of column IDs to ColumnPreferences.
   * @throws NullPointerException if any parameter is null.
   */
  public Preferences(
      IntegerProperty maxItemsProperty,
      IntegerProperty minRemovalProperty,
      Map<String, ColumnPreferences> columnPreferences) {

    this.maxItemsProperty = Objects.requireNonNull(maxItemsProperty);
    this.minRemovalProperty = Objects.requireNonNull(minRemovalProperty);
    Objects.requireNonNull(columnPreferences);

    //Start with full set of default column preferences
    Map<String, ColumnPreferences> prefs = newPreferencesMap();
    ColumnDefinitions columnDefinitions = new ColumnDefinitions();

    columnPreferences.forEach((colId, colPrefs) -> {
      if (columnDefinitions.exists(colId) && colPrefs != null) {
        prefs.put(colId, colPrefs);
      }
    });

    this.columnPreferences = Collections.unmodifiableMap(prefs);
  }

  /**
   * maxItems controls the maximum number of items in the History table.  Note that the actual
   * number of entries is equal to the sum of maxItems and minRemoval.
   */
  public IntegerProperty maxItemsProperty() {
    return maxItemsProperty;
  }

  /**
   * minRemoval is the number of items that will be removed from the History table when the
   * History table contains maxItems plus minRemoval entries.
   */
  public IntegerProperty minRemovalProperty() {
    return minRemovalProperty;
  }

  /**
   * Returns an unmodifiable map of column ID to column preferences for each defined column.
   * Column IDs are defined in the ColumnDefinitions class.
   */
  public Map<String, ColumnPreferences> getColumnPreferences() {
    return columnPreferences;
  }

  private static Map<String, ColumnPreferences> newPreferencesMap() {
    return new ColumnDefinitions()
        .getColumns()
        .stream()
        .collect(Collectors.toMap(
            ColumnDefinition::getId,
            ColumnPreferences::new));
  }
}

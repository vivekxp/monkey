param (
    [string]$input_path,
    [string]$output_path
 )

$regex = "MeasureKPI:Memory\[MB\]:Max:(?<max>[0-9]+):Total\(Min/Max\):(?<total_min>[0-9]+)/(?<total_max>[0-9]+):Free\(Min/Max\):(?<free_min>[0-9]+)/(?<free_max>[0-9]+).*\.(.*)"
"Test Name, Max, Total Min, Total Min, Free Min, Free Max" > $output_path
select-string -Path $input_path -Pattern $regex -AllMatches | % { $_.Matches} | % { $_.Groups[1].Value + ','+ $_.Groups[2].Value + ',' + $_.Groups[3].Value + ',' + $_.Groups[4].Value + ',' + $_.Groups[5].Value + ',' + $_.Groups[6].Value} >> $output_path

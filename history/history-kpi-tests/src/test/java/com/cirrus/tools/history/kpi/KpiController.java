/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file KpiController.java
 */
package com.cirrus.tools.history.kpi;

import com.cirrus.tools.controls.Theme;
import com.cirrus.tools.history.javafx.HistoryControl;
import com.cirrus.tools.history.javafx.HistoryDataListJavaFX;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.concurrent.Task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class KpiController {
  private static final int EXECUTOR_SHUTDOWN_TIMEOUT_SECS = 5;

  private final ExecutorService executorService = Executors.newCachedThreadPool();
  private final HistoryControl historyControl;
  private final HistoryDataListJavaFX dataList;

  KpiController(ObjectProperty<Theme> themeProperty) {
    dataList = new HistoryDataListJavaFX(executorService);
    historyControl = new HistoryControl(dataList.getObservableList(), themeProperty);
  }

  HistoryControl getHistoryControl() {
    return historyControl;
  }

  HistoryDataListJavaFX getHistoryDataListJavaFx() {
    return dataList;
  }

  /**
   * Allows this controller to free up any resources it may have acquired.
   */
  @SuppressWarnings("ThrowableResultOfMethodCallIgnored") //No choice but to consume and print the exception
  void shutdown() throws InterruptedException {
    Task<Void> t = dataList.stopAsync();
    Platform.runLater(() ->
        t.setOnFailed(__ -> t.getException().printStackTrace()));

    executorService.shutdown();
    executorService.awaitTermination(EXECUTOR_SHUTDOWN_TIMEOUT_SECS, TimeUnit.SECONDS);
  }

}

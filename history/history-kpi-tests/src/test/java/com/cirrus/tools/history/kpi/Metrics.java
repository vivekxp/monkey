/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file Metrics.java
 */
package com.cirrus.tools.history.kpi;

import java.util.Objects;

/**
 * Do not share instances of this class between tests.
 * N.B. This class is not designed to be thread-safe.  The intended usage is:
 * - Call start()
 * - Call updateMemoryStats() as required
 * - Call stop()
 */
class Metrics {
  private final String testClassName;
  private final String testName;

  private long startTimestamp = Long.MIN_VALUE;
  private long durationMillis = Long.MIN_VALUE;
  private long totalMemoryMin = Runtime.getRuntime().totalMemory();
  private long freeMemoryMin = Runtime.getRuntime().freeMemory();
  private long totalMemoryMax = totalMemoryMin;
  private long freeMemoryMax = freeMemoryMin;

  /**
   * Constructor.
   * @param testClass classname used in formatted output.
   * @param testName used in formmatted output.
   * @throws NullPointerException if either parameter is null.
   */
  Metrics(Class<?> testClass, String testName) {
    this.testClassName = Objects.requireNonNull(testClass.getName());
    this.testName = Objects.requireNonNull(testName);
  }

  /**
   * Updates memory counters and sets the start timestamp.  Calling more than once may lead to
   * unpredictable results.
   */
  void start() {
    updateMemoryStats();
    startTimestamp = System.currentTimeMillis();
  }

  /**
   * Calculates duration and updates memory stats.  Can be called more than once if required.
   */
  void stop() {
    durationMillis = System.currentTimeMillis() - startTimestamp;
    updateMemoryStats();
  }

  void updateMemoryStats() {
    updateTotalMemory();
    updateFreeMemory();
  }

  String formattedDuration() {
    return String.format(
        "MeasureKPI:Duration[ms]:%6d:%s.%s.",
        durationMillis,
        testClassName,
        testName);
  }

  String formattedMemory() {
    String format =
        "MeasureKPI:Memory[MB]:"
            + "Max:%4d"
            + ":Total(Min/Max):%4d/%4d"
            + ":Free(Min/Max):%4d/%4d"
            + ":%s"   //class name
            + ".%s";  //test name

    return String.format(
        format,
        Runtime.getRuntime().maxMemory() / 1024 / 1024, //maxMemory does not change over time
        totalMemoryMin / 1024 / 1024,
        totalMemoryMax / 1024 / 1024,
        freeMemoryMin / 1024 / 1024,
        freeMemoryMax / 1024 / 1024,
        testClassName,
        testName);
  }

  private void updateTotalMemory() {
    long total = Runtime.getRuntime().totalMemory();
    totalMemoryMin = Math.min(totalMemoryMin, total);
    totalMemoryMax = Math.max(totalMemoryMax, total);
  }

  private void updateFreeMemory() {
    long free = Runtime.getRuntime().freeMemory();
    freeMemoryMin = Math.min(freeMemoryMin, free);
    freeMemoryMax = Math.max(freeMemoryMax, free);
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TestUtil.java
 */
package com.cirrus.tools.history.kpi;

import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.tools.history.api.HistoryConfig;
import com.cirrus.tools.history.api.Subscriber;
import com.cirrus.tools.history.api.TopicConsumer;
import com.cirrus.tools.history.impl.DefaultTopicConsumer;
import com.cirrus.tools.history.impl.table.DefaultConfig;
import com.cirrus.tools.history.impl.table.TableRowModel;

import java.util.List;
import java.util.function.Consumer;

/**
 * Set of utilities encapsulating common code for these integration tests.
 */
public final class TestUtil {

  private TestUtil() {
    throw new UnsupportedOperationException("Util class only.");
  }

  /**
   * Creates a Subscriber containing a stubbed consumer that takes no action when passed a List
   * of TableRowModels.  This is to allow KPI tests to optionally exclude the JavaFX UI components
   * which are largely out with our control.
   */
  public static ISubscriber newSubscriberWithStubbedUiConsumer() {
    //This effectively stubs out the UI handling
    Consumer<List<TableRowModel>> rowConsumer = rows -> { /* Ignore */ };

    HistoryConfig<TableRowModel> config = new DefaultConfig();
    TopicConsumer consumer = new DefaultTopicConsumer<>(rowConsumer, config);

    return new Subscriber(consumer);
  }
}

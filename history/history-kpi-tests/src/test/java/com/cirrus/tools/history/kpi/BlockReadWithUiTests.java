/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file BlockReadWithUiTests.java
 */

package com.cirrus.tools.history.kpi;

import com.cirrus.scs.studiolink.api.notifications.TopicNotification;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

/**
 * These tests measure the memory and time taken for the Subscriber to process one or more Link
 * notifications.  This <b>includes</b> the time taken to add rows to the History UI.
 *
 * Test set up time is <b>excluded</b> from the results.
 *
 * Note that when these tests run on Jenkins, they will run with a virtual display (headless) so the
 * results (from Jenkins) may vary significantly from the <i>actual</i> results and should be
 * considered relative rather than absolute.
 *
 * Another important thing to be aware of is that the last step of processing a Link notification is
 * to add the formatted row data to the UI itself which must be done on a separate UI thread that
 * is controlled by the JavaFX framework.  Therefore we can expect a degree of fuzziness in the
 * results of these tests.
 *
 * The same set of tests, without this fuzzy UI element, are tested elsewhere.
 *
 * RegionChange messages with multiple elements are the most complex messages to process.
 */
@RunWith(Parameterized.class)
public class BlockReadWithUiTests extends HistoryKpiTest {
  @Parameter public int iterations;
  @Parameter(1) public int elements;

  @Test @SuppressWarnings("PMD.SystemPrintln")  //Logger not appropriate for Jenkins
  public void testSubscriber() {
    String testName = String.format("subscriber_%d_notifications_with_%d_elements_waitForUi", iterations, elements);
    Metrics metrics = new Metrics(getClass(), testName);
    TopicNotification tn = ExampleNotifications.newOnChangeBlockReadMessageWithMultipleElements(elements);

    metrics.start();

    for (int i = 0; i < iterations; i++) {
      subscriber.onMessageReceived(tn);
      metrics.updateMemoryStats();
    }

    waitForTableToBePopulated(iterations);
    metrics.stop();

    System.out.println(metrics.formattedDuration());
    System.out.println(metrics.formattedMemory());
  }

  @Parameters(name = "notifications={0}, elements={1}")
  public static Collection<Object[]> testParams() {
    return Arrays.asList(new Object[][] {
        {1000, 1}, {10000, 1}, {1000, 20000}, {1, 10000}
    });
  }
}

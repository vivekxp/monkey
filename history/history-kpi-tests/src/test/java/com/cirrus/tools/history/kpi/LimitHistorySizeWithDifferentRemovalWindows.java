/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file LimitHistorySizeWithDifferentRemovalWindows.java
 */

package com.cirrus.tools.history.kpi;

import com.cirrus.tools.history.impl.table.ImmutableTableRowData;
import com.cirrus.tools.history.impl.table.TableRowFormatter;
import com.cirrus.tools.history.impl.table.TableRowModel;

import javafx.application.Platform;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;

/**
 * The results of the tests can be used as a direct comparison between different removal window sizes
 */
@RunWith(Parameterized.class)
public class LimitHistorySizeWithDifferentRemovalWindows extends HistoryKpiTest {

  @Parameter public int removalWindowSize;

  @Parameters(name = "windowSize={0}")
  public static Object[] testParams() {
    return new Object[] {1, 100, 1000};
  }

  @Before
  public void setMaxItemsAndWindowSize() {
    assertNotNull(kpiController);
    kpiController.getHistoryDataListJavaFx().getHistoryDataList().setMaxItems(1000);
    kpiController.getHistoryDataListJavaFx().getHistoryDataList().setMinRemoval(removalWindowSize);
    //Assert table is sorted before starting tests
    waitForFxThread();
  }

  @Test
  @SuppressWarnings("PMD.SystemPrintln")  //Logger not appropriate for Jenkins
  public void add_1000000_rows() {
    int iterations = 1000000;
    String testName = String.format("add_1000000_rows_removalWindowSize%d", removalWindowSize);

    //We do this up front so as to not include this work in the metrics
    List<TableRowModel> rows = createTableRowModelsWithAscendingElapsedTime(iterations);

    Metrics metrics = new Metrics(getClass(), testName);
    metrics.start();
    // Populate the list on the application thread in the same way TableRowConsumer does
    Platform.runLater(() -> {
      for (int i = 0; i < iterations; i++) {
        addRowToTable(rows.get(i));
        metrics.updateMemoryStats();
      }
    });
    waitForTableToBePopulated(iterations);
    metrics.stop();

    System.out.println(metrics.formattedDuration());
    System.out.println(metrics.formattedMemory());
  }

  private List<TableRowModel> createTableRowModelsWithAscendingElapsedTime(int count) {
    TableRowFormatter formatter = new TableRowFormatter();
    List<TableRowModel> rows = new ArrayList<>(count);

    for (int i = 0; i < count; i++) {
      ImmutableTableRowData data = new ImmutableTableRowData.Builder()
          .setDeviceName(String.valueOf(count))
          .setElapsedTime(count)
          .build();

      rows.add(new TableRowModel(data, formatter));
    }

    return rows;
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file UnsolicitedMessageWithoutUiTests.java
 */

package com.cirrus.tools.history.kpi;

import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.scs.studiolink.api.notifications.TopicNotification;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

/**
 * These tests measure duration and memory while the Subscriber processes one or more Link
 * notifications.  A mock UI control is used so that this test <b>does not</b> include the memory
 * used when adding rows to the History UI.
 *
 * The same set of tests, <b>with</b> the UI component are tested elsewhere.
 *
 * Note that when these tests run on Jenkins, they will run with a virtual display (headless) so the
 * results (from Jenkins) may vary significantly from the <i>actual</i> results and should be
 * considered relative rather than absolute.
 *
 * UnsolicitedMessages are the simplest messages to process.
 */
@RunWith(Parameterized.class)
public class UnsolicitedMessageWithoutUiTests extends KpiApplicationTest {
  @Parameter public int iterations;

  private final ISubscriber subscriber = TestUtil.newSubscriberWithStubbedUiConsumer();

  @Test @SuppressWarnings("PMD.SystemPrintln")  //Logger not appropriate for Jenkins
  public void testSubscriber() {
    String testName = String.format("subscriber_%d_notifications", iterations);
    Metrics metrics = new Metrics(getClass(), testName);
    TopicNotification tn = ExampleNotifications.newUnsolicitedMessage();

    metrics.start();
    for (int i = 0; i < iterations; i++) {
      subscriber.onMessageReceived(tn);
      metrics.updateMemoryStats();
    }
    metrics.stop();

    System.out.println(metrics.formattedDuration());
    System.out.println(metrics.formattedMemory());
  }

  @Parameters(name = "notifications={0}")
  public static Object[] testParams() {
    return new Object[] {1, 1000, 10000};
  }
}

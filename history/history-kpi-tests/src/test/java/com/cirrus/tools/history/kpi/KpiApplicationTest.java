/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file KpiApplicationTest.java
 */
package com.cirrus.tools.history.kpi;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.Before;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.util.WaitForAsyncUtils;

/**
 * Based on CirrusApplicationTest from the com.cirrus.tools:test-common:1.0.15.0 library.  We
 * roll our own version so that the KPI tests will not be sensitive to changes in external (to
 * History) dependencies.
 */
public class KpiApplicationTest extends ApplicationTest {
  private static final int WAIT_FOR_FX_EVENTS_COUNT = 30;

  protected final Pane root = new VBox();
  private Stage stage;

  @Override
  public void start(Stage testStage) {
    this.stage = testStage;
  }

  @Before
  public void initStageAndScene() {
    interact(() -> {
      Scene scene = new Scene(root);
      root.getChildren().addListener((InvalidationListener) listener -> stage.sizeToScene());
      stage.setScene(scene);
      stage.show();
    });

    waitForFxThread();
  }

  @After
  public void closeStage() {
    Platform.runLater(() -> stage.close());
  }

  protected static void waitForFxThread() {
    WaitForAsyncUtils.waitForFxEvents(WAIT_FOR_FX_EVENTS_COUNT);
  }
}

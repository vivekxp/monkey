/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ExampleNotifications.java
 */

package com.cirrus.tools.history.kpi;

import com.cirrus.scs.studiolink.api.enums.ControlInterface.ControlInterfaceValues;
import com.cirrus.scs.studiolink.api.enums.FieldRegisterOperationStatus;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement.ElementType;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData.RegionEvent;
import com.cirrus.scs.studiolink.api.notifications.TopicNotification;
import com.cirrus.scs.studiolink.api.notifications.UnsolicitedMessageData;

import java.util.LinkedList;
import java.util.List;

/**
 * Test helper to supply prebuilt non-mocked Link notifications.
 */
public final class ExampleNotifications {

  private ExampleNotifications() {
    throw new UnsupportedOperationException("Util class only.");
  }
  /**
   * Still to see a genuine example so not sure how realistic this data is.
   */
  public static TopicNotification newUnsolicitedMessage() {
    String topicUri = UnsolicitedMessageData.TOPIC_UNSOLICITED_MESSAGE;

    UnsolicitedMessageData msg = new UnsolicitedMessageData(new byte[] {1, 2, 3, 4});

    return new TopicNotification(topicUri, msg);
  }

  /**
   * Creates an OnChange / BlockRead Link notification message with the given number of elements.
   */
  public static TopicNotification newOnChangeBlockReadMessageWithMultipleElements(int elementCount) {
    String topicUri = RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE;
    List<RegionChangeElement> changedElements = new LinkedList<>();

    for (int i = 0; i < elementCount; i++) {
      changedElements.add(new RegionChangeElement(
          String.format("ElementName%d", i),  //elementName,
          i,                                  //elementAddress,
          i,                                  //elementPage,
          ElementType.REGISTER,
          i,                                  //newValue,
          false,                              //errorReadingFromDevice,
          FieldRegisterOperationStatus.SUCCESS,
          false,                              //secure,
          "4wireSPI_32inx_32dat",             //protocolName,
          i                                   //deviceAddress
      ));
    }

    RegionChangeMessageData msg = new RegionChangeMessageData(
        RegionEvent.BLOCK_READ,
        "Virtual CDB48LV40-M-1 Mini Board", //systemName
        "CS48LV40F",                        //deviceName,
        ControlInterfaceValues.CTRL_SPI,
        1,                                  //address,
        changedElements);

    return new TopicNotification(topicUri, msg);
  }
}

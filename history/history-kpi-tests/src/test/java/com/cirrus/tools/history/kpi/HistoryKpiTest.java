/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file HistoryKpiTest.java
 */
package com.cirrus.tools.history.kpi;

import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.tools.controls.CirrusTableView;
import com.cirrus.tools.controls.Theme;
import com.cirrus.tools.history.impl.HistoryDataList;
import com.cirrus.tools.history.impl.table.TableRowModel;
import com.cirrus.tools.history.javafx.HistoryDataListJavaFX;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import org.junit.After;
import org.junit.Before;

import java.lang.reflect.Field;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class HistoryKpiTest extends KpiApplicationTest {
  /*
   * The main reason this class is not merged into KpiApplicationTest is because PMD reports that
   * that the combined class has too many methods.  It is quite useful to keep KpiApplicationTest
   * separate anyway since it is modelled on CirrusApplicationTest and may ultimately be
   * relocated or replaced.
   */

  protected ISubscriber subscriber;
  protected CirrusTableView<TableRowModel> historyTable;

  protected KpiController kpiController;

  @Before
  public void initHistory() {
    kpiController = new KpiController(new SimpleObjectProperty<>(Theme.LIGHT));
    interact(() -> root.getChildren().setAll(kpiController.getHistoryControl()));

    //These items not instantiated until after FXML initialized in HistoryControl
    historyTable = lookupHistoryTable();
    subscriber = lookupSubscriberViaReflection();

    assertTrue("Table did not start empty.", historyTable.getItems().isEmpty());
  }

  @After
  public void shutdownController() throws InterruptedException {
    if (kpiController != null) {
      kpiController.shutdown();
      kpiController = null;
    }
  }

  @After
  public void clearItems() {
    if (kpiController != null) {
      kpiController.getHistoryDataListJavaFx().clearList();
    }
  }

  public void addRowToTable(TableRowModel row) {
    assertNotNull(kpiController);
    kpiController.getHistoryDataListJavaFx().addRowToList(row);
  }

  protected void waitForTableToBePopulated(int count) {
    while (historyTable.getItems().size() < count) {
      Thread.yield();
    }
  }

  @SuppressWarnings("unchecked") //Nodes are not generic so no choice but to cast
  private CirrusTableView<TableRowModel> lookupHistoryTable() {
    Node n = root.lookup(CirrusTableView.class.getSimpleName());
    return (CirrusTableView<TableRowModel>) n;
  }

  private ISubscriber lookupSubscriberViaReflection() {
    HistoryDataListJavaFX controller = kpiController.getHistoryDataListJavaFx();
    return lookupSubscriberViaReflection(controller.getHistoryDataList());
  }

  private ISubscriber lookupSubscriberViaReflection(HistoryDataList dataList) {
    try {
      Field field = dataList.getClass().getDeclaredField("subscriber");
      field.setAccessible(true);

      ISubscriber sub = (ISubscriber) field.get(dataList);

      if (sub == null) {
        throw new IllegalStateException("Subscriber is null.");
      }

      return sub;
    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new IllegalStateException("Failed to lookup subscriber.", e);
    }
  }

}

param (
    [string]$input_path,
    [string]$output_path
 )

$regex = "MeasureKPI:Duration\[ms\]: *(?<time>[0-9]+):.*\.(.*)\."
"Test Name,Duration ms" > $output_path
select-string -Path $input_path -Pattern $regex -AllMatches | % { $_.Matches} | % { $_.Groups[1].Value + ','+ $_.Groups[2] }  > $output_path

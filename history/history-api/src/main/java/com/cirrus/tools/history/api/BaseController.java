/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file BaseController.java
 */

package com.cirrus.tools.history.api;

import com.cirrus.scs.studiolink.api.notifications.INotificationManager;
import com.cirrus.scs.studiolink.api.notifications.ISubscriber;

import java.util.Objects;
import java.util.Set;

public class BaseController implements Controller {
  private final INotificationManager notificationManager;

  public BaseController(INotificationManager notificationManager) {
    this.notificationManager = Objects.requireNonNull(notificationManager);
  }

  @Override
  public void subscribeToTopics(ISubscriber subscriber, Set<String> topics) {
    Objects.requireNonNull(subscriber);
    Objects.requireNonNull(topics);

    if (topics.isEmpty()) {
      throw new IllegalArgumentException("No topics to subscribe to.");
    }

    topics.forEach(topicName ->
        notificationManager.addSubscriber(subscriber, topicName));
  }

  @Override
  public void unsubscribe(ISubscriber subscriber) {
    notificationManager.removeSubscriber(subscriber);
  }
}

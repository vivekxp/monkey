/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TimestampedNotification.java
 */
package com.cirrus.tools.history.api;

import com.cirrus.scs.studiolink.api.notifications.TopicNotification;

import java.util.Objects;

public class TimestampedNotification {
  private final TopicNotification topicNotification;
  private final long timestamp;
  private final long elapsedTimeNanoSeconds;

  public TimestampedNotification(TopicNotification notification) {
    this.topicNotification = Objects.requireNonNull(notification);
    timestamp = System.currentTimeMillis();
    elapsedTimeNanoSeconds = System.nanoTime();
  }

  public long getTimestamp() {
    return timestamp;
  }

  /**
   * Get the elapsed time in nanoseconds since the virtual machine started
   * @return elapsed time
   */
  public long getElapsedTime() {
    return elapsedTimeNanoSeconds;
  }

  public String getTopicURI() {
    return topicNotification.getTopicURI();
  }

  public Object getMessage() {
    return topicNotification.getMessage();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null) {
      return false;
    }

    if (getClass() != o.getClass()) {
      return false;
    }

    TimestampedNotification that = (TimestampedNotification) o;

    return timestamp == that.timestamp
        && topicNotification.equals(that.topicNotification);
  }

  @Override
  public int hashCode() {
    return Objects.hash(topicNotification, timestamp);
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file StringUtil.java
 */
package com.cirrus.tools.history.api;

/**
 * Library of commons String utilities.
 * Because History is a component/widget, we want to reduce third-party dependencies to the bare
 * minimum which is why we do not simply use Apache commons.
 */
public final class StringUtil {

  public static final String EMPTY = "";

  private StringUtil() {
    throw new UnsupportedOperationException("Util class only.");
  }

  /**
   * Checks if a String is null, empty or contains only whitespace.
   * It is equivalent to:
   * <code>String.trim().isEmpty();</code>
   * ...with a null check.  The above line of code would generate a PMD warning because calling
   * trim() causes a new String object to be created.  Working around the PMD warning is the
   * motivation for this method.
   * @return true if the given String is null, empty or contains just whitespace; else false.
   */
  public static boolean isTrimmedStringEmptyOrNull(String string) {
    if (string == null) {
      return true;
    }

    for (int index = 0; index < string.length(); index++) {
      if (!Character.isWhitespace(string.charAt(index))) {
        return false;
      }
    }

    return true;
  }
}

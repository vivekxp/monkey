/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file BaseControllerTest.java
 */

package com.cirrus.tools.history.api;

import com.cirrus.scs.studiolink.api.notifications.INotificationManager;
import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class BaseControllerTest {
  private BaseController sut;
  private INotificationManager mockNotificationManager;

  @Before
  public void before() {
    mockNotificationManager = mock(INotificationManager.class);
    sut = new BaseController(mockNotificationManager);
  }

  @Test(expected = NullPointerException.class)
  public void constructor_notificationManagerIsNull_exceptionThrown() {
    new BaseController(null);
  }

  @Test(expected = NullPointerException.class)
  public void subscribeToTopics_subscriberIsNull_exceptionThrown() {
    sut.subscribeToTopics(null, newMockTopicSet());
  }

  @Test(expected = NullPointerException.class)
  public void subscribeToTopics_topicsIsNull_exceptionThrown() {
    sut.subscribeToTopics(mock(ISubscriber.class), null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void subscribeToTopics_topicsIsEmpty_exceptionThrown() {
    sut.subscribeToTopics(mock(ISubscriber.class), Collections.emptySet());
  }

  @Test
  public void subscribeToTopics_happyPath_eachTopicSubscribedTo() {
    ISubscriber mockSubscriber = mock(ISubscriber.class);
    Set<String> topics = new HashSet<>(Arrays.asList("t1", "t2", "t3"));

    sut.subscribeToTopics(mockSubscriber, topics);

    topics.forEach(t -> verify(mockNotificationManager).addSubscriber(mockSubscriber, t));
  }

  @Test
  public void unsubscribe_happyPath_subscriberRemoved() {
    ISubscriber mockSubscriber = mock(ISubscriber.class);

    sut.unsubscribe(mockSubscriber);

    verify(mockNotificationManager).removeSubscriber(mockSubscriber);
  }

  @SuppressWarnings("unchecked") //Mocks are not generic so no choice but to cast
  private Set<String> newMockTopicSet() {
    return mock(Set.class);
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TimestampedNotificationTest.java
 */
package com.cirrus.tools.history.api;

import com.cirrus.scs.studiolink.api.notifications.TopicNotification;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TimestampedNotificationTest {

  @Test
  public void getTimestamp_happyPath_timestampNotInTheFuture() {
    TimestampedNotification sut = newTimestampedNotificationWithMockedTopicNotification();

    long actual = sut.getTimestamp();

    long now = System.currentTimeMillis();
    assertTrue(actual <= now);
  }

  @Test
  public void getTimestamp_happyPath_timestampIsNotMoreThanOneSecondInPast() {
    TimestampedNotification sut = newTimestampedNotificationWithMockedTopicNotification();

    long actual = sut.getTimestamp();

    long now = System.currentTimeMillis();
    assertTrue((now - actual) < 1000);
  }

  @Test
  public void getTopicURI_happyPath_topicReturned() {
    //Given
    String topic = "testTopic";

    //Arrange
    TopicNotification tn = mock(TopicNotification.class);
    when(tn.getTopicURI()).thenReturn(topic);
    TimestampedNotification sut = newTimestampedNotification(tn);

    //Act
    String actual = sut.getTopicURI();

    //Assert
    assertEquals(topic, actual);
  }

  @Test
  public void getMessage_happyPath_messageReturned() {
    //Given
    Object message = "message";

    //Arrange
    TopicNotification tn = mock(TopicNotification.class);
    when(tn.getMessage()).thenReturn(message);
    TimestampedNotification sut = newTimestampedNotification(tn);

    //Act
    Object actual = sut.getMessage();

    //Assert
    assertEquals(message, actual);
  }

  @Test
  public void equals_otherIsNull_returnsFalse() {
    TimestampedNotification sut = newTimestampedNotificationWithMockedTopicNotification();
    TimestampedNotification other = null;

    boolean actual = sut.equals(other);

    assertFalse(actual);
  }

  @Test
  public void equals_otherIsSame_returnsTrue() {
    TimestampedNotification sut = newTimestampedNotificationWithMockedTopicNotification();

    boolean actual = sut.equals(sut);

    assertTrue(actual);
  }

  @Test
  public void equals_otherIsDifferentClass_returnsFalse() {
    TimestampedNotification sut = newTimestampedNotificationWithMockedTopicNotification();

    boolean actual = sut.equals("");

    assertFalse(actual);
  }

  @Test
  public void equals_otherIsEqual_returnsTrue() {
    //Arrange
    long timestamp = System.currentTimeMillis();
    TopicNotification topicNotification = mock(TopicNotification.class);

    TimestampedNotification sut = new TimestampedNotification(topicNotification);
    forceTimestamp(sut, timestamp);

    TimestampedNotification other = new TimestampedNotification(topicNotification);
    forceTimestamp(other, timestamp);

    //Act
    boolean actual = sut.equals(other);

    //Assert
    assertTrue(actual);
  }

  @Test
  public void hashCode_twoIdenticalInstances_hashCodesAreEqual() {
    //Arrange
    long timestamp = System.currentTimeMillis();
    TopicNotification topicNotification = mock(TopicNotification.class);

    TimestampedNotification sut = new TimestampedNotification(topicNotification);
    forceTimestamp(sut, timestamp);

    TimestampedNotification other = new TimestampedNotification(topicNotification);
    forceTimestamp(other, timestamp);

    //Act
    long sutHashCode = sut.hashCode();
    long otherHashCode = other.hashCode();

    //Assert
    assertEquals(sutHashCode, otherHashCode);
  }

  private void forceTimestamp(TimestampedNotification tn, long timestamp) {
    try {
      Field timestampField = TimestampedNotification.class.getDeclaredField("timestamp");
      timestampField.setAccessible(true);
      timestampField.set(tn, timestamp);
    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new IllegalStateException("Could not force timestamp.", e);
    }
  }

  private TimestampedNotification newTimestampedNotificationWithMockedTopicNotification() {
    TopicNotification topicNotification = mock(TopicNotification.class);
    return new TimestampedNotification(topicNotification);
  }

  private TimestampedNotification newTimestampedNotification(TopicNotification topicNotification) {
    return new TimestampedNotification(topicNotification);
  }
}

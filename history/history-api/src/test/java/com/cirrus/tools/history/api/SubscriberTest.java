/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file SubscriberTest.java
 */

package com.cirrus.tools.history.api;

import com.cirrus.scs.studiolink.api.notifications.TopicNotification;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SubscriberTest {
  private Subscriber sut;
  private TopicConsumer mockConsumer;

  @Before
  public void before() {
    mockConsumer = mock(TopicConsumer.class);
    sut = new Subscriber(mockConsumer);
  }

  @Test(expected = NullPointerException.class)
  public void constructor_topicConsumerIsNull_exceptionThrown() {
    new Subscriber(null);
  }

  @Test(expected = NullPointerException.class)
  public void onMessageReceived_messageIsNull_exceptionThrown() {
    sut.onMessageReceived(null);
  }

  @Test
  public void onMessageReceived_happyPath_messageAddedToConsumer() {
    TopicNotification tn = new TopicNotification("topic", new Object());

    sut.onMessageReceived(tn);

    verify(mockConsumer).add(any(TimestampedNotification.class));
  }
}

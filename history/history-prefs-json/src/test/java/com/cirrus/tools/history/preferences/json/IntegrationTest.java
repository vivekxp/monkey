/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file IntegrationTest.java
 */

package com.cirrus.tools.history.preferences.json;

import com.cirrus.tools.history.javafx.Preferences;
import com.cirrus.tools.history.javafx.table.ColumnPreferences;
import com.cirrus.tools.history.javafx.table.definition.ColumnDefinitions;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * These tests verify that if a Preference object is serialized and deserialized, the
 * deserialized object should be equal to the original object.  Unlike the unit tests, these tests
 * do not care about the format or method of deserializing, only that objects can be restored to
 * their original state.
 *
 * The only snag is that JavaFX properties do not implement equals() or hashCode() so we cannot
 * easily compare the original object with the restored object.  Therefore, in these tests, the
 * actual comparison is achieved by serializing the restored object into a String and comparing with
 * the serialized String of the original object.
 */
public class IntegrationTest {
  private final Serializer serializer = new Serializer();
  private final Deserializer deserializer = new Deserializer();

  @Test
  public void serializeAndDeserialize_defaultPreferences_originalObjectRestored() {
    Preferences original = new Preferences();
    String originalJson = serializer.toJson(original);

    Preferences replica = deserializer.fromJson(originalJson);
    String replicaJson = serializer.toJson(replica);

    assertEquals(originalJson, replicaJson);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void serializeAndDeserialize_defaultPreferences_restoredObjectIsUnmodifiable() {
    Preferences original = new Preferences();
    String originalJson = serializer.toJson(original);

    Preferences replica = deserializer.fromJson(originalJson);
    replica.getColumnPreferences().clear();
  }

  @Test
  public void serializeAndDeserialize_customPreferences_originalObjectRestored() {
    IntegerProperty maxItems = new SimpleIntegerProperty(50);
    IntegerProperty minRemoval = new SimpleIntegerProperty(55);
    Preferences original = new Preferences(maxItems, minRemoval, newCustomColumnPreferences());
    String originalJson = serializer.toJson(original);

    Preferences replica = deserializer.fromJson(originalJson);
    String replicaJson = serializer.toJson(replica);

    assertEquals(originalJson, replicaJson);
  }

  private Map<String, ColumnPreferences> newCustomColumnPreferences() {
    Map<String, ColumnPreferences> map = new HashMap<>();

    map.put(
        ColumnDefinitions.SYSTEM_COLUMN_ID,
        new ColumnPreferences(
            new SimpleDoubleProperty(20),       //Preferred width
            new SimpleBooleanProperty(true)));  //Visibility

    map.put(
        ColumnDefinitions.DEVICE_COLUMN_ID,
        new ColumnPreferences(
            new SimpleDoubleProperty(30),        //Preferred width
            new SimpleBooleanProperty(false)));  //Visibility

    return map;
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file DeserializerTest.java
 */

package com.cirrus.tools.history.preferences.json;

import com.cirrus.tools.history.javafx.Preferences;
import com.cirrus.tools.history.javafx.table.ColumnPreferences;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DeserializerTest {
  private final Deserializer sut = new Deserializer();
  private static final double DELTA = 0.01;

  @Test
  public void fromJson_validJson_restoredObjectRepresentsJson() {
    //Act
    Preferences actual = sut.fromJson(validJsonWithTwoColumns());

    //Assert
    assertIntegerProperty(100, actual.maxItemsProperty());
    assertIntegerProperty(5, actual.minRemovalProperty());

    Map<String, ColumnPreferences> actualColPrefs = actual.getColumnPreferences();
    Set<String> expectedColumnIds = new HashSet<>(Arrays.asList("systemColumn", "deviceColumn"));
    assertEquals("Column IDs do not match.", expectedColumnIds, actualColPrefs.keySet());

    assertPreferredWidthProperty("systemColumn", 50, actualColPrefs);
    assertPreferredWidthProperty("deviceColumn", 60, actualColPrefs);
    assertVisibilityProperty("systemColumn", true, actualColPrefs);
    assertVisibilityProperty("deviceColumn", false, actualColPrefs);
  }

  @Test
  public void fromJson_nullJson_newPreferencesReturned() {
    Preferences actual = sut.fromJson(null);

    assertNotNull(actual);
  }

  @Test
  public void fromJson_invalidJson_newPreferencesReturned() {
    Preferences actual = sut.fromJson("invalid");

    assertNotNull(actual);
  }

  /**
   * Preferences object contains a collection of preferences for each column.  If the json for a
   * single column preference is invalid, that column preference should be replaced by a new
   * default ColumnPreference, but other preferences should be restored correctly.
   */
  @Test
  public void fromJson_invalidJsonForSingleColumnPreference_allOthersRestored() {
    double timestampPreferredWidth = 60.0;
    boolean timestampVisible = false;

    //Arrange
    String semiValid = semiValidJsonWithThreeColumns(timestampPreferredWidth, timestampVisible);

    //Act
    Map<String, ColumnPreferences> colPrefs = sut.fromJson(semiValid).getColumnPreferences();

    //Assert
    assertNotNull("systemColumn is null.", colPrefs.get("systemColumn"));
    assertNotNull("deviceColumn is null.", colPrefs.get("deviceColumn"));

    assertPreferredWidthProperty("timestampColumn", timestampPreferredWidth, colPrefs);
    assertVisibilityProperty("timestampColumn", timestampVisible, colPrefs);
  }

  /**
   * By default, Gson library was not restoring correct map implementation.
   */
  @Test
  public void fromJson_validJson_restoredObjectHasCorrectMapImplementation() {
    Preferences defaultPreferences = new Preferences();
    Preferences restoredPrefs = sut.fromJson(validJsonWithTwoColumns());

    assertEquals(
        defaultPreferences.getColumnPreferences().getClass(),
        restoredPrefs.getColumnPreferences().getClass());
  }

  @SuppressWarnings("checkstyle:LineLength")  //for readability
  private String validJsonWithTwoColumns() {
    return new StringBuilder()
        .append("{\"maxItemsProperty\":{\"name\":\"\",\"value\":100,\"valid\":true},")
        .append("\"minRemovalProperty\":{\"name\":\"\",\"value\":5,\"valid\":true},")
        .append("\"columnPreferences\":{")

        .append("\"systemColumn\":{")
        .append("\"preferredWidthProperty\":{\"name\":\"\",\"value\":50.0,\"valid\":true},")
        .append("\"visibleProperty\":{\"name\":\"\",\"value\":true,\"valid\":true}")
        .append("},")

        .append("\"deviceColumn\":{")
        .append("\"preferredWidthProperty\":{\"name\":\"\",\"value\":60.0,\"valid\":true},")
        .append("\"visibleProperty\":{\"name\":\"\",\"value\":false,\"valid\":true}")
        .append("}")

        .append("}}")
        .toString();
  }

  @SuppressWarnings("checkstyle:LineLength")  //for readability
  private String semiValidJsonWithThreeColumns(double timestampPreferredWidth, boolean timestampVisibility) {
    return new StringBuilder()
        .append("{\"maxItemsProperty\":{\"name\":\"\",\"value\":100,\"valid\":true},")
        .append("\"minRemovalProperty\":{\"name\":\"\",\"value\":5,\"valid\":true},")
        .append("\"columnPreferences\":{")

        .append("\"systemColumn\":{\"unexpectedProperty\":{\"xxx\":yyy,\"aaa\":bbb}},")

        .append("\"deviceColumn\":{")
        .append("\"preferredWidthProperty\":{\"name\":\"\",\"value\":50.0,\"valid\":true,\"helper\":{\"observable\":{}}},")
        .append("\"visibleProperty\":{\"invalid\":\"\",\"xxx\":yyy,\"aaa\":bbb}")
        .append("},")

        .append("\"timestampColumn\":{")
        .append("\"preferredWidthProperty\":{\"name\":\"\",\"value\":").append(timestampPreferredWidth).append(",\"valid\":true,\"helper\":{\"observable\":{}}},")
        .append("\"visibleProperty\":{\"name\":\"\",\"value\":").append(timestampVisibility).append(",\"valid\":true}")
        .append("}")

        .append("}}")
        .toString();
  }

  private void assertIntegerProperty(int expectedValue, IntegerProperty property) {
    assertEquals("Wrong value.", expectedValue, property.get());
    assertEquals("Wrong name.", "", property.getName());
  }

  private void assertPreferredWidthProperty(
      String columnId,
      double expectedValue,
      Map<String, ColumnPreferences> colPrefs) {

    DoubleProperty property = colPrefs.get(columnId).preferredWidthProperty();

    assertEquals("Wrong value: " + columnId, expectedValue, property.get(), DELTA);
    assertEquals("Wrong name: " + columnId, "", property.getName());
  }

  private void assertVisibilityProperty(
      String columnId,
      boolean expectedValue,
      Map<String, ColumnPreferences> colPrefs) {

    BooleanProperty property = colPrefs.get(columnId).visibleProperty();

    assertEquals("Wrong value: " + columnId, expectedValue, property.get());
    assertEquals("Wrong name: " + columnId, "", property.getName());
  }
}

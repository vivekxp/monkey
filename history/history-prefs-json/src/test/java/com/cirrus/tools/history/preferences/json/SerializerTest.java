/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file SerializerTest.java
 */

package com.cirrus.tools.history.preferences.json;

import com.cirrus.tools.history.api.StringUtil;
import com.cirrus.tools.history.javafx.Preferences;
import com.cirrus.tools.history.javafx.table.definition.ColumnDefinitions;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SerializerTest {

  @Test(expected = NullPointerException.class)
  public void toJson_nullPreferences_nullPointerExceptionThrown() {
    Serializer sut = new Serializer();

    sut.toJson(null);
  }

  @Test
  public void toJson_happyPath_nonEmptyStringReturned() {
    Serializer sut = new Serializer();

    String actual = sut.toJson(new Preferences());

    assertFalse(StringUtil.isTrimmedStringEmptyOrNull(actual));
  }

  @Test
  public void toJson_happyPath_returnedStringContainsAllColumnIds() {
    Serializer sut = new Serializer();

    String actual = sut.toJson(new Preferences());

    new ColumnDefinitions().getColumnIds().forEach(colId ->
        assertTrue("Not found: " + colId, actual.contains(colId)));
  }
}

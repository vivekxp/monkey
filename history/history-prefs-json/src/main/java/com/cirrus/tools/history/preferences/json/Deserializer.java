/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file Deserializer.java
 */

package com.cirrus.tools.history.preferences.json;

import com.cirrus.tools.history.javafx.Preferences;
import com.cirrus.tools.history.javafx.table.ColumnPreferences;
import com.cirrus.tools.history.javafx.table.definition.ColumnDefinition;
import com.cirrus.tools.history.javafx.table.definition.ColumnDefinitions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

/**
 * Converts JSON string into Preferences.
 */
public class Deserializer {
  private static final Logger LOG = LoggerFactory.getLogger(Deserializer.class);

  private final Gson gson;

  /**
   * Constructor.
   */
  public Deserializer() {
    gson = new GsonBuilder()
        .registerTypeAdapter(BooleanProperty.class, new BooleanPropertyDeserializer())
        .registerTypeAdapter(DoubleProperty.class, new DoublePropertyDeserializer())
        .registerTypeAdapter(IntegerProperty.class, new IntegerPropertyDeserializer())
        .registerTypeAdapter(Map.class, new MapDeserializer())
        .create();
  }

  /**
   * De-serialize a json string into a Preferences object.  If the string is invalid or if the
   * string is out of sync due to changes in Preferences, 'broken' preferences will be replaced
   * with sensible defaults.
   */
  //We want to catch all exceptions here so that we can recover
  @SuppressWarnings({"PMD.AvoidCatchingGenericException", "checkstyle:IllegalCatch"})
  public Preferences fromJson(String json) {
    if (json == null) {
      return new Preferences();
    }

    try {
      Preferences prefs = gson.fromJson(json, Preferences.class);

      if (prefs == null) {  //According to Gson docs this should never happen
        LOG.error("Gson parser returned null for: : " + json);
        prefs = new Preferences();
      }

      return prefs;
    } catch (Exception e) {
      LOG.error("Could not parse Preferences: " + json, e);
      return new Preferences();
    }
  }

  private static class MapDeserializer implements JsonDeserializer<Map<String, ColumnPreferences>> {
    @Override
    public Map<String, ColumnPreferences> deserialize(
        JsonElement element,
        Type type,
        JsonDeserializationContext ctx) throws JsonParseException {

      Map<String, ColumnPreferences> tempMap = new HashMap<>();

      element.getAsJsonObject().entrySet().forEach(mapEntry -> {
        ColumnPreferences cp = deserializeColumnPreferences(mapEntry, ctx);
        tempMap.put(mapEntry.getKey(), cp);
      });

      return Collections.unmodifiableMap(tempMap);
    }

    //We want to catch all exceptions here so that we can recover
    @SuppressWarnings({"PMD.AvoidCatchingGenericException", "checkstyle:IllegalCatch"})
    private ColumnPreferences deserializeColumnPreferences(
        Entry<String, JsonElement> mapEntry,
        JsonDeserializationContext ctx) {

      JsonElement json = mapEntry.getValue();

      try {
        ColumnPreferences cp = ctx.deserialize(json, ColumnPreferences.class);
        validate(cp);
        return cp;
      } catch (Exception e) {
        LOG.error("Could not parse ColumnPreferences: " + json, e);

        String colId = mapEntry.getKey();
        Optional<ColumnDefinition> colDef = new ColumnDefinitions().get(colId);

        if (!colDef.isPresent()) {
          //PMD warns that original exception is lost but we have logged and this one is not related
          throw new IllegalStateException("No column definition for: " + colId); //NOPMD
        }

        return new ColumnPreferences(colDef.get());
      }
    }

    private void validate(ColumnPreferences cp) {
      if (cp.preferredWidthProperty() == null) {
        throw new IllegalStateException("preferredWidthProperty is null.");
      }

      if (cp.visibleProperty() == null) {
        throw new IllegalStateException("visibleProperty is null.");
      }
    }
  }

  private static class BooleanPropertyDeserializer implements JsonDeserializer<BooleanProperty> {
    @Override
    public BooleanProperty deserialize(
        JsonElement json,
        Type type,
        JsonDeserializationContext ctx) throws JsonParseException {

      JsonObject obj = json.getAsJsonObject();
      JsonElement element = obj.get("value");
      boolean propertyValue = element.getAsBoolean();

      return new SimpleBooleanProperty(propertyValue);
    }
  }

  private static class DoublePropertyDeserializer implements JsonDeserializer<DoubleProperty> {
    @Override
    public DoubleProperty deserialize(
        JsonElement json,
        Type type,
        JsonDeserializationContext ctx) throws JsonParseException {

      JsonObject obj = json.getAsJsonObject();
      JsonElement element = obj.get("value");
      double propertyValue = element.getAsDouble();

      return new SimpleDoubleProperty(propertyValue);
    }
  }

  private static class IntegerPropertyDeserializer implements JsonDeserializer<IntegerProperty> {
    @Override
    public IntegerProperty deserialize(
        JsonElement json,
        Type type,
        JsonDeserializationContext ctx) throws JsonParseException {

      JsonObject obj = json.getAsJsonObject();
      JsonElement element = obj.get("value");
      int propertyValue = element.getAsInt();

      return new SimpleIntegerProperty(propertyValue);
    }
  }
}

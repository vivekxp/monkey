/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file Serializer.java
 */

package com.cirrus.tools.history.preferences.json;

import com.cirrus.tools.history.javafx.Preferences;
import com.google.gson.Gson;

import java.util.Objects;

/**
 * Converts Preferences to JSON string.
 */
public class Serializer {

  /**
   * Converts Preferences to JSON string.
   * @throws NullPointerException if preferences is null.
   */
  public String toJson(Preferences preferences) {
    Objects.requireNonNull(preferences);

    Gson gson = new Gson();

    return gson.toJson(preferences);
  }
}

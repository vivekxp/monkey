/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file PollingMessageDataHandler.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.enums.ControlInterface.ControlInterfaceValues;
import com.cirrus.scs.studiolink.api.notifications.PollingMessageData;
import com.cirrus.tools.history.api.TimestampedNotification;

import java.util.Objects;
import java.util.Optional;

public class PollingMessageDataHandler implements NotificationHandler {
  private final String attemptsFormat;

  /**
   * Constructor with parameter to format attempt and max attempt values.
   * @param attemptsFormat is a String Pattern that must contain two integer (%d) placeholders to
   *                       be replaced with the attempt count and the max attempt count.  For
   *                       example: "Polling: %d/%d"
   * @throws NullPointerException if attemptsFormat is null.
   * @throws IllegalArgumentException if attemptsFormat does not contain two integer placeholders.
   */
  public PollingMessageDataHandler(String attemptsFormat) {
    this.attemptsFormat = Objects.requireNonNull(attemptsFormat);

    if (!attemptsFormat.matches(".*%d.*%d.*")) {
      throw new IllegalArgumentException("Illegal string format: " + attemptsFormat);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<ImmutableTableRowData> handle(TimestampedNotification notification) {
    Objects.requireNonNull(notification);
    PollingMessageData msg = (PollingMessageData) notification.getMessage();

    //SCS does not validate the calls to msg
    String systemName = msg.getSystemName();
    String deviceName = msg.getDeviceName();
    int deviceAddress = msg.getDeviceAddress();
    String attempts = String.format(attemptsFormat, msg.getAttempts(), msg.getMaxAttempts());
    String messageInterruptRegister = msg.getMessageInterruptRegister();
    int registerAddress = msg.getRegisterAddress();

    ImmutableTableRowData.Builder builder = new ImmutableTableRowData.Builder()
        .setSystemName(systemName)
        .setDeviceName(deviceName)
        .setDeviceAddress(deviceAddress)
        .setRegisterOrFieldName(messageInterruptRegister)
        .setType(attempts)  //In SCS goes in the comment field which does not exist here
        .setAddress(registerAddress)
        .setTimestamp(notification.getTimestamp())
        .setElapsedTime(notification.getElapsedTime());

    if (msg.getControlInterfaceType() != null) {
      ControlInterfaceValues controlInterfaceType = msg.getControlInterfaceType();
      builder.setBusProtocol(controlInterfaceType.toString());
    }

    return Optional.of(builder.build());
  }
}

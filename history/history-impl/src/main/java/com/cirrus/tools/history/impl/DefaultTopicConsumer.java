/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file DefaultTopicConsumer.java
 */
package com.cirrus.tools.history.impl;

import com.cirrus.tools.history.api.HistoryConfig;
import com.cirrus.tools.history.api.TimestampedNotification;
import com.cirrus.tools.history.api.TopicConsumer;

import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Default implementation that converts Link notifications into a list of items to (typically)
 * be consumed by a UI Control such as Table or List.
 * @param <T> is the type of the model objects that will be consumed.
 */
public class DefaultTopicConsumer<T> implements TopicConsumer {
  private final Consumer<List<T>> rowConsumer;
  private final HistoryConfig<T> historyConfig;

  /**
   * Constructor.
   * @param consumer accepts list of items created by historyConfig when parsing a Link notification.
   * @param historyConfig Converts a Link notification into a list of items to be consumed by rowConsumer.
   */
  public DefaultTopicConsumer(Consumer<List<T>> consumer, HistoryConfig<T> historyConfig) {
    this.rowConsumer = Objects.requireNonNull(consumer);
    this.historyConfig = Objects.requireNonNull(historyConfig);
  }

  /**
   * This method should return quickly so as not to block the notifications thread.
   */
  @Override
  public void add(TimestampedNotification notification) {
    Objects.requireNonNull(notification);
    Objects.requireNonNull(notification.getMessage());

    List<T> rows = historyConfig.handle(notification);
    rowConsumer.accept(rows);
  }
}

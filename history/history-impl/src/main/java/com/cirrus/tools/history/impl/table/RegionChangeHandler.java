/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file RegionChangeHandler.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement.ElementType;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData;
import com.cirrus.tools.history.api.TimestampedNotification;
import com.cirrus.tools.history.impl.table.ImmutableTableRowData.Builder;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
  Example of a real RegionChangeMessageData message with two region change elements:

  TopicNotification[
      topicURI=OnChange,
      message=RegionChangeMessageData[
          regionEvent=RegionEvent.VALUE_CHANGE,
          systemName=Virtual CDB48LV40-M-1 Mini Board,
          deviceName=CS48LV40F,
          controlInterface=CTRL_SPI,
          address=1,
          changedElements=ArrayList {
              RegionChangeElement[
                  elementName=GP1_DB,
                  elementAddress=3080,
                  elementPage=0,
                  elementType=ElementType.FIELD,
                  newValue=0,
                  errorReadingFromDevice=false,
                  status=FieldRegisterOperationStatus.SUCCESS,
                  secure=false,
                  protocolName=4wireSPI_32inx_32dat,
                  deviceAddress=1
              ],
              RegionChangeElement[
                elementName=GPIO1_CTRL1,
                elementAddress=3080,
                elementPage=0,
                elementType=ElementType[REGISTER],
                newValue=3774873601,
                errorReadingFromDevice=false,
                status=FieldRegisterOperationStatus.SUCCESS,
                secure=false,
                protocolName=4wireSPI_32inx_32dat,
                deviceAddress=1
              ]
          }
      ]
  ]
 */
public class RegionChangeHandler implements NotificationHandler {

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<ImmutableTableRowData> handle(TimestampedNotification notification) {
    Objects.requireNonNull(notification);
    RegionChangeMessageData msg = (RegionChangeMessageData) notification.getMessage();
    List<RegionChangeElement> elements = Objects.requireNonNull(msg.getChangedElements());

    if (filterNonSecureElements(elements).isEmpty()) {
      return Optional.empty();
    }

    RegionChangeElement firstElement = elements.get(0);
    if (ElementType.FIELD.equals(firstElement.getElementType())) {
      return Optional.empty();
    }

    switch(msg.getRegionEvent()) {
      case READ: return handleReadEvent(msg, notification.getTimestamp(), notification.getElapsedTime());
      case WRITE: return handleWriteEvent(msg, notification.getTimestamp(), notification.getElapsedTime());
      case BLOCK_READ: return handleBlockRead(msg, notification.getTimestamp(), notification.getElapsedTime());
      case BLOCK_WRITE: return handleBlockWrite(msg, notification.getTimestamp(), notification.getElapsedTime());
      case VALUE_CHANGE: return Optional.empty();
      default:
        throw new AssertionError("Unexpected event enum: " + msg.getRegionEvent());
    }
  }

  private Optional<ImmutableTableRowData> handleReadEvent(RegionChangeMessageData msg,
      long timestamp,
      long elapsedTime) {
    return handleNonBlockEvent(msg, timestamp, elapsedTime);
  }

  private Optional<ImmutableTableRowData> handleWriteEvent(RegionChangeMessageData msg,
      long timestamp,
      long elapsedTime) {
    return handleNonBlockEvent(msg, timestamp, elapsedTime);
  }

  private Optional<ImmutableTableRowData> handleBlockWrite(RegionChangeMessageData msg,
      long timestamp,
      long elapsedTime) {
    return handleBlockEvent(msg, timestamp, elapsedTime);
  }

  private Optional<ImmutableTableRowData> handleBlockRead(RegionChangeMessageData msg,
      long timestamp,
      long elapsedTime) {
    return handleBlockEvent(msg, timestamp, elapsedTime);
  }

  private Optional<ImmutableTableRowData> handleNonBlockEvent(RegionChangeMessageData msg,
      long timestamp,
      long elapsedTime) {
    List<RegionChangeElement> elements = msg.getChangedElements();
    assert elements != null : "Changed elements is null";     //Already checked earlier
    assert !elements.isEmpty() : "Changed elements is empty"; //Already checked earlier

    RegionChangeElement firstElement = elements.get(0);

    ImmutableTableRowData.Builder builder = new ImmutableTableRowData.Builder()
        .setAddress(firstElement.getElementAddress())
        .setBusProtocol(msg.getControlInterface().toString())
        .setData(firstElement.getNewValue())
        .setDeviceAddress(msg.getAddress())
        .setSystemName(msg.getSystemName())
        .setDeviceName(msg.getDeviceName())
        .setOperation(msg.getRegionEvent().toString())
        .setRegisterOrFieldName(firstElement.getElementName())
        .setTimestamp(timestamp)
        .setElapsedTime(elapsedTime)
        .setType(firstElement.getElementType().toString())
        .setBitWidth(firstElement.getBitWidth())
        .setErrorReadingFromDevice(firstElement.getHasError());

    if (firstElement.getIsPaged()) {
      builder.setPage(firstElement.getElementPage());
    }

    return Optional.of(builder.build());
  }

  /**
   * For the time being we format all (handled) messages the same.
   */
  private Optional<ImmutableTableRowData> handleBlockEvent(RegionChangeMessageData msg,
      long timestamp,
      long elapsedTime) {
    List<RegionChangeElement> elements = msg.getChangedElements();
    assert elements != null : "Changed elements is null";     //Already checked earlier
    assert !elements.isEmpty() : "Changed elements is empty"; //Already checked earlier

    List<ImmutableTableRowData> blockRows = new LinkedList<>();

    for (RegionChangeElement element : elements) {
      ImmutableTableRowData.Builder builder = new Builder()
          .setAddress(element.getElementAddress())
          .setRegisterOrFieldName(element.getElementName())
          .setType(element.getElementType().toString())
          .setData(element.getNewValue())
          .setBitWidth(element.getBitWidth())
          .setErrorReadingFromDevice(element.getHasError());

      if (element.getIsPaged()) {
        builder.setPage(element.getElementPage());
      }

      blockRows.add(builder.build());
    }

    ImmutableTableRowData.Builder builder = new ImmutableTableRowData.Builder()
        .setSystemName(msg.getSystemName())
        .setDeviceName(msg.getDeviceName())
        .setOperation(msg.getRegionEvent().toString())
        .setBusProtocol(msg.getControlInterface().toString())
        .setDeviceAddress(msg.getAddress())
        .setTimestamp(timestamp)
        .setElapsedTime(elapsedTime)
        .setChildren(blockRows);

    return Optional.of(builder.build());
  }

  /**
   * Secure messages are handled separately.
   */
  private List<RegionChangeElement> filterNonSecureElements(List<RegionChangeElement> elements) {

    return elements
        .stream()
        .filter(element -> !element.isSecure())
        .collect(Collectors.toList());
  }
}

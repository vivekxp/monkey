/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file HistoryDataList.java
 */
package com.cirrus.tools.history.impl;

import com.cirrus.scs.studiolink.api.notifications.INotificationManager;
import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.tools.history.api.BaseController;
import com.cirrus.tools.history.api.Controller;
import com.cirrus.tools.history.api.HistoryConfig;
import com.cirrus.tools.history.api.ObjectFactory;
import com.cirrus.tools.history.api.TopicConsumer;
import com.cirrus.tools.history.impl.table.DefaultConfig;
import com.cirrus.tools.history.impl.table.TableRowConsumer;
import com.cirrus.tools.history.impl.table.TableRowModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * This is the non-UI-specific model for History.
 */
public class HistoryDataList {
  private static final Logger LOG = LoggerFactory.getLogger(HistoryDataList.class);

  private final List<TableRowModel> list;
  private final HistoryConfig<TableRowModel> historyConfig;
  private final ISubscriber subscriber;
  private final TableRowConsumer rowConsumer;

  private volatile Controller controller;

  @FunctionalInterface
  public interface PlatformRunLaterInterface {
    void run(Runnable runnable);
  }

  /**
   * Constructor with a default history config
   */
  public HistoryDataList(List<TableRowModel> list) {
    this(list, r -> r.run(), new DefaultConfig());
  }

  /**
   * Constructor with a default history config
   */
  public HistoryDataList(List<TableRowModel> list, PlatformRunLaterInterface runLaterInterface) {
    this(list, runLaterInterface, new DefaultConfig());
  }
  /**
   * Constructor
   * @param historyConfig History config to set history with
   */
  public HistoryDataList(
      List<TableRowModel> list,
      PlatformRunLaterInterface runLaterMethod,
      HistoryConfig<TableRowModel> historyConfig) {
    Objects.requireNonNull(runLaterMethod);
    this.list = Objects.requireNonNull(list);
    this.historyConfig = Objects.requireNonNull(historyConfig);

    ObjectFactory factory = newFactory();
    rowConsumer = new TableRowConsumer(list, runLaterMethod);
    TopicConsumer topicConsumer = new DefaultTopicConsumer<>(rowConsumer, historyConfig);
    subscriber = factory.getSubscriber(topicConsumer);
  }

  /**
   * Subscribe to notifications from the notification manager.
   * Do not call from a UI thread. Does nothing if already subscribed
   *
   * @param notificationManager Notification manager to subscribe to.
   *
   * @return false if no action was taken due to already being subscribed, else true.
   *
   * @throws IllegalStateException if called from a UI thread.
   */
  public synchronized boolean start(INotificationManager notificationManager) {
    Objects.requireNonNull(notificationManager);
    if (controller == null) {
      Set<String> topics = historyConfig.getTopics();
      controller = new BaseController(notificationManager);
      controller.subscribeToTopics(subscriber, topics);

      return true;
    }

    LOG.debug("Topics already subscribed. Doing nothing.");
    return false;
  }

  /**
   * Frees up resources. Do not call from UI thread.
   *
   * @throws IllegalStateException if called from UI thread.
   */
  public synchronized void stop() {
    if (controller != null) {
      controller.unsubscribe(subscriber);
      controller = null;
    } else {
      LOG.debug("No controller to shutdown");
    }
  }

  public void clearList() {
    list.clear();
  }

  public void addRowToList(TableRowModel row) {
    list.add(row);
  }

  /**
   * maxItems controls the maximum number of items in the History table.  Note that the actual
   * number of entries is equal to the sum of maxItems and minRemoval.
   */
  public void setMaxItems(int maxItems) {
    rowConsumer.setMaxItems(maxItems);
  }

  /**
   * minRemoval is the number of items that will be removed from the History table when the
   * History table contains maxItems plus minRemoval entries.
   */
  public void setMinRemoval(int minRemoval) {
    rowConsumer.setMinRemoval(minRemoval);
  }

  public List<TableRowModel> getList() {
    return list;
  }

  private static ObjectFactory newFactory() {
    Iterator<ObjectFactory> i = ServiceLoader.load(ObjectFactory.class, ObjectFactory.class.getClassLoader())
        .iterator();

    assert i.hasNext() : "Object factory service not found.";
    return i.next();
  }

}

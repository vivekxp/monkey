/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TableRowConsumer.java
 */

package com.cirrus.tools.history.impl.table;

import com.cirrus.tools.history.impl.HistoryDataList.PlatformRunLaterInterface;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * This class is responsible for ensuring the list doesn't go above a set maximum size.
 */
public class TableRowConsumer implements Consumer<List<TableRowModel>> {
  // Default values set to arbitrary value that seems large enough to be useful without being stupidly big
  public static final int DEFAULT_MAX_NUMBER_OF_ITEMS = 100000;
  public static final int DEFAULT_MAX_ITEMS_MIN_REMOVAL = 1;

  private static final Logger LOG = LoggerFactory.getLogger(TableRowConsumer.class);

  private final List<TableRowModel> list;
  private final PlatformRunLaterInterface runLater;

  private int maxItems = DEFAULT_MAX_NUMBER_OF_ITEMS;
  private int minRemoval = DEFAULT_MAX_ITEMS_MIN_REMOVAL;

  public TableRowConsumer(List<TableRowModel> table) {
    this(table, Runnable::run);
  }

  public TableRowConsumer(List<TableRowModel> table, PlatformRunLaterInterface runLaterInterface) {
    this.list = Objects.requireNonNull(table);
    this.runLater = Objects.requireNonNull(runLaterInterface);
  }

  /**
   * Set the max number of items that can be present in the list
   * @param maxItems maximum number of items
   */
  public void setMaxItems(int maxItems) {
    this.maxItems = maxItems;
    runLater.run(() -> removeItemsIfNeeded(list));
  }

  /**
   * Set the minimum number of items we will remove from the list in one go.
   * Effectively this is the size of a window above maxItems that we wait to fill before shrinking the list.
   * @param minRemoval the size of the max item window
   */
  public void setMinRemoval(int minRemoval) {
    this.minRemoval = minRemoval;
    runLater.run(() ->  removeItemsIfNeeded(list));
  }


  /**
   * Add items to the list and check the max size hasn't been exceeded
   *
   * @param rows List of TableRowModel objects to be consumed.
   * @throws NullPointerException if row List is null.
   */
  @Override
  public void accept(List<TableRowModel> rows) {
    Objects.requireNonNull(rows, "Row list is null.");

    if (rows.isEmpty()) {
      return;
    }

    runLater.run(() ->  addRowsToTable(rows, list));
  }

  private void removeItemsIfNeeded(List<TableRowModel> table) {
    // To limit the amount of work we do implement a high water mark to stop removal triggering
    // too often
    if (list.size() >= (maxItems + minRemoval)) {
      table.subList(0, list.size() - maxItems).clear();
    }
  }

  //This catch is designed to catch all exceptions
  @SuppressWarnings({"PMD.AvoidCatchingGenericException", "checkstyle:IllegalCatch"})
  private void addRowsToTable(List<TableRowModel> rows, List<TableRowModel> table) {
    try {
      table.addAll(rows);
      removeItemsIfNeeded(table);
    } catch (Exception e) {
      LOG.error("Could not add row to the table.", e);
    }
  }
}

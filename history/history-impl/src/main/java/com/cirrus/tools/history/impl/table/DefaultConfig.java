/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file DefaultConfig.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.notifications.AsyncRequestMessageData;
import com.cirrus.scs.studiolink.api.notifications.PollingMessageData;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.SecureMessageData;
import com.cirrus.scs.studiolink.api.notifications.UnsolicitedMessageData;
import com.cirrus.tools.history.api.HistoryConfig;
import com.cirrus.tools.history.api.TimestampedNotification;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Encapsulates the rules and knowledge of SCS when subscribing to Link and parsing the received
 * notifications from Link.  The initial requirement for History V2 is to emulate SCS History and
 * this class attempts to capture that knowledge in one place, making it easier to provide alternate
 * configurations.
 */
public class DefaultConfig implements HistoryConfig<TableRowModel> {
  private static final String POLLING_ATTEMPTS_FORMAT = "Polling: %d/%d"; //As per SCS
  private static final String BYTE_STRING_FORMAT = "%02X "; //Trailing space as per SCS

  private final TableRowFormatter formatter = new TableRowFormatter();
  private final Map<Class<?>, NotificationHandler> handlerLookup;

  /**
   * Constructor.  This JavaDoc required by Checkstyle.
   */
  public DefaultConfig() {
    Map<Class<?>, NotificationHandler> handlers = new HashMap<>();

    handlers.put(SecureMessageData.class, new SecureHandler());
    handlers.put(AsyncRequestMessageData.class, new AsyncRequestHandler());
    handlers.put(RegionChangeMessageData.class, new RegionChangeHandler());
    handlers.put(UnsolicitedMessageData.class, new UnsolicitedHandler(BYTE_STRING_FORMAT));
    handlers.put(PollingMessageData.class, new PollingMessageDataHandler(POLLING_ATTEMPTS_FORMAT));

    handlerLookup = Collections.unmodifiableMap(handlers);
  }

  @Override
  public List<TableRowModel> handle(TimestampedNotification notification) {
    Objects.requireNonNull(notification);
    Object msg = Objects.requireNonNull(notification.getMessage());

    NotificationHandler handler = handlerLookup.get(msg.getClass());
    if (handler == null) {
      return Collections.emptyList();
    }

    Optional<ImmutableTableRowData> data = handler.handle(notification);
    if (!data.isPresent()) {
      return Collections.emptyList();
    }

    TableRowModel model = new TableRowModel(data.get(), formatter);
    return Collections.singletonList(model);
  }

  @Override
  public Set<String> getTopics() {
    Set<String> t = new HashSet<>();

    t.add(AsyncRequestMessageData.TOPIC_ASYNC_REQUEST_MESSAGE);
    t.add(PollingMessageData.TOPIC_POLLING_MESSAGE);
    t.add(RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE);
    t.add(SecureMessageData.TOPIC_SECURE_MESSAGE);
    t.add(UnsolicitedMessageData.TOPIC_UNSOLICITED_MESSAGE);

    return Collections.unmodifiableSet(t);
  }
}

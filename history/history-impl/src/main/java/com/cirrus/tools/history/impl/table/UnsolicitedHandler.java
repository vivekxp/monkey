/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file UnsolicitedHandler.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.notifications.UnsolicitedMessageData;
import com.cirrus.tools.history.api.TimestampedNotification;

import java.util.Objects;
import java.util.Optional;

public class UnsolicitedHandler implements NotificationHandler {
  private static final String OPERATION_NAME = "Unsolicited";
  private final String byteStringFormat;

  /**
   * Constructor with parameter to format attempt and max attempt values.
   * @param byteStringFormat is a String Pattern that must contain one binary (%X or %x)
   *                         placeholder to be replaced with a String representation of
   *                         data byte array. For example: "Data: %02X"
   * @throws NullPointerException if byteStringFormat is null.
   * @throws IllegalArgumentException if byteStringFormat does not contain at least one binary
   * placeholder.
   */
  public UnsolicitedHandler(String byteStringFormat) {
    this.byteStringFormat = Objects.requireNonNull(byteStringFormat);

    if (!byteStringFormat.matches(".*%.*[Xx].*")) {
      throw new IllegalArgumentException("Illegal string format: " + byteStringFormat);
    }
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<ImmutableTableRowData> handle(TimestampedNotification notification) {
    Objects.requireNonNull(notification);
    String topic = Objects.requireNonNull(notification.getTopicURI());
    UnsolicitedMessageData msg = (UnsolicitedMessageData) notification.getMessage();

    if (isUnexpectedTopic(topic)) {
      return Optional.empty();
    }

    byte[] msgDataBytes = Objects.requireNonNull(msg.getMessageData());

    //In SCS this goes to the core field which does not exist in the current UX design
    String packetString = toString(msgDataBytes);

    ImmutableTableRowData.Builder builder = new ImmutableTableRowData.Builder()
        .setOperation(OPERATION_NAME)
        .setRegisterOrFieldName(packetString)
        .setTimestamp(notification.getTimestamp())
        .setElapsedTime(notification.getElapsedTime());

    return Optional.of(builder.build());
  }

  private String toString(byte[] data) {
    StringBuilder sb = new StringBuilder();

    for (byte datum : data) {
      sb.append(String.format(byteStringFormat, datum));
    }

    return sb.toString();
  }

  private boolean isUnexpectedTopic(String topic) {
    return !topic.equalsIgnoreCase(UnsolicitedMessageData.TOPIC_UNSOLICITED_MESSAGE);
  }
}

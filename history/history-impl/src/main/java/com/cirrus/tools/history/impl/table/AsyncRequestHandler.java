/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file AsyncRequestHandler.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.notifications.AsyncRequestMessageData;
import com.cirrus.tools.history.api.StringUtil;
import com.cirrus.tools.history.api.TimestampedNotification;

import java.util.Objects;
import java.util.Optional;

/**
  Example of a real AsyncRequestMessageData message:

  TopicNotification[
     topicURI=AsyncRequest,
     message=AsyncRequestMessageData[
         asyncRequestEvent=AsyncRequestEvent.DEVICE_REFRESH,
         succeeded=true,
         systemName=Virtual CDB48LV40-M-1 Mini Board,
         deviceName=CS48LV40F,
         additionalInfo=
     ]
  ]
 */
public class AsyncRequestHandler implements NotificationHandler {

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<ImmutableTableRowData> handle(TimestampedNotification notification) {
    Objects.requireNonNull(notification);
    String topic = Objects.requireNonNull(notification.getTopicURI());
    AsyncRequestMessageData msg = (AsyncRequestMessageData) notification.getMessage();

    if (isUnexpectedTopic(topic)) {
      return Optional.empty();
    }

    ImmutableTableRowData.Builder builder = new ImmutableTableRowData.Builder()
        .setSystemName(msg.getSystemName())
        .setDeviceName(msg.getDeviceName())
        .setDeviceAddress(msg.getDeviceAddress())
        .setTimestamp(notification.getTimestamp())
        .setElapsedTime(notification.getElapsedTime());

    if (hasAdditionalInfo(msg)) {
      //In SCS this goes to the comment field which does not exist in the current UX design
      builder.setRegisterOrFieldName(msg.getAdditionalInfo());
    }

    if (msg.getControlInterfaceType() != null) {
      builder.setBusProtocol(msg.getControlInterfaceType().toString());
    }

    return Optional.of(builder.build());
  }

  private boolean isUnexpectedTopic(String topic) {
    return !topic.equalsIgnoreCase(AsyncRequestMessageData.TOPIC_ASYNC_REQUEST_MESSAGE);
  }

  private boolean hasAdditionalInfo(AsyncRequestMessageData msg) {
    return !StringUtil.isTrimmedStringEmptyOrNull(msg.getAdditionalInfo());
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file LinkVersionChecker.java
 */

package com.cirrus.tools.history.impl;

import com.cirrus.scs.studiolink.api.IStudioLink;
import com.cirrus.scs.studiolink.api.exceptions.StudioLinkException;
import com.cirrus.scs.studiolink.api.utils.Version;

public class LinkVersionChecker {

  /**
   * Checks Link client and server versions are compatible.  Versions are compatible if client
   * version is less than or equal to server version.
   * @throws IllegalStateException if server version is not compatible with the client version.
   * @throws StudioLinkException if link throws StudioLinkException.
   */
  public void verify(IStudioLink link) throws StudioLinkException {
    Version client = link.getClientAPIVersion();
    Version server = link.getStudioLinkVersion();

    if (client.compareTo(server) > 0) {
      throw new IllegalStateException(String.format(
          "Incompatible Link versions. Client: [%s], server: [%s].", client, server));
    }
  }
}

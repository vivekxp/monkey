/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file SecureHandler.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.notifications.SecureMessageData;
import com.cirrus.tools.history.api.TimestampedNotification;

import java.util.Objects;
import java.util.Optional;

public class SecureHandler implements NotificationHandler {
  /*
   * Until the following ticket is resolved:
   * https://tracker.cirrus.com/browse/SCSSTU-972
   * ... this class contains only very basic handling of secure messages for the reasons set out in
   * the ticket.
   */

  /**
   * {@inheritDoc}
   */
  @Override
  public Optional<ImmutableTableRowData> handle(TimestampedNotification notification) {
    Objects.requireNonNull(notification);
    SecureMessageData msg = (SecureMessageData) notification.getMessage();

    ImmutableTableRowData.Builder builder = new ImmutableTableRowData.Builder()
        .setSystemName(msg.getSystemName())
        .setDeviceName(msg.getDeviceName())
        .setTimestamp(notification.getTimestamp())
        .setElapsedTime(notification.getElapsedTime())
        .setAddress(msg.getRegisterAddress())
        .setDeviceAddress(msg.getDeviceAddress())

        //In SCS this goes to the comment field which does not exist in the current UX design
        .setRegisterOrFieldName(toSecureMessageComment(msg));

    if (msg.getControlInterfaceType() != null) {
      builder.setBusProtocol(msg.getControlInterfaceType().toString());
    }

    return Optional.of(builder.build());
  }

  private String toSecureMessageComment(SecureMessageData msg) {

    /*
     * Until the following ticket is resolved:
     * https://tracker.cirrus.com/browse/SCSSTU-972
     * ... we deliberately exclude the message data itself.
     */
    return String.join(
        ",",
        String.valueOf(msg.getServiceType()),
        String.valueOf(msg.isBulk()),
        msg.getMessageTypeName(),
        String.valueOf(msg.getMessageId()),
        String.valueOf(msg.getStatus()),
        msg.getDeviceType(),
        msg.getDeviceName());
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ImmutableTableRowData.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.tools.history.api.StringUtil;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * This builder class allows collections of history data to be aggregated as a single item.  For
 * example, a block read could represent many rows in the history table, but when emulating the SCS
 * history, we want to display a single row for the that event.
 *
 * This class is the bridge between the notification handlers that know how to
 * parse various Link notifications, and the formatter class that knows how to format the output.
 *
 * The various Link notifications only have a couple of fields in common but we know that there is
 * a fixed set of columns to map to and this class represents the un-formatted data for those
 * columns.
 */
@SuppressWarnings("PMD.TooManyFields") // As we have one field per column we have lots of fields.
public final class ImmutableTableRowData { //final to be immutable
  /*
   * Remember to make any new params final to keep this class immutable
   */
  private final String systemName;
  private final String deviceName;
  private final String operation;
  private final String busProtocol;
  private final Long address;
  private final String registerOrFieldName;
  private final String type;
  private final Long data;
  private final Long page;
  private final Integer deviceAddress;
  private final Integer bitWidth;
  private final boolean errorReadingFromDevice;
  private final Long timestamp;
  private final Long elapsedTimeNanoSeconds;
  private final List<ImmutableTableRowData> children;
  private final boolean rowHasError;

  /*
   * Suppressing this warning because the constructor is private and and a public builder is
   * provided to address the very problem highlighted by PMD.
   */
  @SuppressWarnings("PMD.ExcessiveParameterList")
  private ImmutableTableRowData(
      String systemName,
      String deviceName,
      String operation,
      String busProtocol,
      Long address,
      String registerOrFieldName,
      String type,
      Long data,
      Long page,
      Integer deviceAddress,
      Integer bitWidth,
      Boolean errorReadingFromDevice,
      Long timestamp,
      Long elapsedTimeNanoSeconds,
      List<ImmutableTableRowData> children) {

    /*
     * Remember to clone any new mutable params to keep this class immutable
     */
    this.systemName = systemName == null ? "" : systemName;
    this.deviceName = deviceName == null ? "" : deviceName;
    this.operation = operation == null ? "" : operation;
    this.busProtocol = busProtocol == null ? "" : busProtocol;
    this.address = address;
    this.registerOrFieldName = registerOrFieldName == null ? "" : registerOrFieldName;
    this.type = type == null ? "" : type;
    this.data = data;
    this.page = page;
    this.deviceAddress = deviceAddress;
    this.bitWidth = bitWidth;
    this.errorReadingFromDevice = errorReadingFromDevice != null && errorReadingFromDevice;
    this.timestamp = timestamp;
    this.elapsedTimeNanoSeconds = elapsedTimeNanoSeconds;

    if (children == null) {
      this.children = Collections.emptyList();
    } else {
      this.children = Collections.unmodifiableList(children);
    }

    this.rowHasError = this.errorReadingFromDevice
        || this.children.stream().anyMatch(ImmutableTableRowData::getErrorReadingFromDevice);
  }

  public String getSystemName() {
    return systemName;
  }

  public String getDeviceName() {
    return deviceName;
  }

  public String getOperation() {
    return operation;
  }

  public String getBusProtocol() {
    return busProtocol;
  }

  public Optional<Long> getAddress() {
    return Optional.ofNullable(address);
  }

  public String getRegisterOrFieldName() {
    return registerOrFieldName;
  }

  public String getType() {
    return type;
  }

  public Optional<Long> getData() {
    return Optional.ofNullable(data);
  }

  public Optional<Long> getPage() {
    return Optional.ofNullable(page);
  }

  public Optional<Integer> getDeviceAddress() {
    return Optional.ofNullable(deviceAddress);
  }

  public Optional<Integer> getBitWidth() {
    return Optional.ofNullable(bitWidth);
  }

  public boolean getErrorReadingFromDevice() {
    return errorReadingFromDevice;
  }

  public Optional<Long> getTimestamp() {
    return Optional.ofNullable(timestamp);
  }

  /**
   * Get the elapsed time in nanoseconds since the virtual machine started.
   * @return An option of the elapsed time if present.
   */
  public Optional<Long> getElapsedTime() {
    return Optional.ofNullable(elapsedTimeNanoSeconds);
  }

  public List<ImmutableTableRowData> getChildren() {
    return children;
  }

  public boolean rowHasError() {
    return rowHasError;
  }

  public static class Builder {
    private String systemName;
    private String deviceName;
    private String operation;
    private String busProtocol;
    private Integer deviceAddress;
    private Long timestamp;
    private Long elapsedTimeNanoSeconds;
    private Long data;
    private Long address;
    private String registerOrFieldName;
    private String type;
    private Long page;
    private Integer bitWidth;
    private Boolean errorReadingFromDevice;
    private List<ImmutableTableRowData> children;

    public Builder setSystemName(String systemName) {
      if (systemName != null) {
        this.systemName = systemName;
      }
      return this;
    }

    public Builder setDeviceName(String deviceName) {
      if (deviceName != null) {
        this.deviceName = deviceName;
      }
      return this;
    }

    public Builder setOperation(String operation) {
      if (operation != null) {
        this.operation = operation;
      }
      return this;
    }

    public Builder setBusProtocol(String busProtocol) {
      if (busProtocol != null) {
        this.busProtocol = busProtocol;
      }
      return this;
    }

    public Builder setDeviceAddress(int deviceAddress) {
      this.deviceAddress = deviceAddress;
      return this;
    }

    public Builder setTimestamp(long timestamp) {
      this.timestamp = timestamp;
      return this;
    }

    /**
     * Set the elapsed time since the virtual machine started in nanoseconds.
     * @param elapsedTime elapsed time in nanoseconds since the virtual machine started
     * @return Instance of the builder
     */
    public Builder setElapsedTime(long elapsedTime) {
      this.elapsedTimeNanoSeconds = elapsedTime;
      return this;
    }

    public Builder setAddress(long address) {
      this.address = address;
      return this;
    }

    public Builder setRegisterOrFieldName(String name) {
      if (name != null) {
        this.registerOrFieldName = name;
      }
      return this;
    }

    public Builder setType(String type) {
      if (type != null) {
        this.type = type;
      }
      return this;
    }

    public Builder setData(long data) {
      this.data = data;
      return this;
    }

    public Builder setPage(long page) {
      this.page = page;
      return this;
    }

    public Builder setBitWidth(int bitWidth) {
      this.bitWidth = bitWidth;
      return this;
    }

    public Builder setErrorReadingFromDevice(boolean errorReadingFromDevice) {
      this.errorReadingFromDevice = errorReadingFromDevice;
      return this;
    }

    public Builder setChildren(List<ImmutableTableRowData> children) {
      if (children != null) {
        this.children = children;
      }
      return this;
    }

    /**
     * Builds and returns.
     */
    public ImmutableTableRowData build() {
      ImmutableTableRowData firstElement = children == null ? null : children.get(0);

      buildAddress(firstElement);
      buildRegisterOrFieldName(firstElement);
      buildType(firstElement);
      buildData(firstElement);
      buildPage(firstElement);
      buildBitWidth(firstElement);
      buildErrorReadingFromDevice(firstElement);

      return new ImmutableTableRowData(
          systemName,
          deviceName,
          operation,
          busProtocol,
          address,
          registerOrFieldName,
          type,
          data,
          page,
          deviceAddress,
          bitWidth,
          errorReadingFromDevice,
          timestamp,
          elapsedTimeNanoSeconds,
          children);
    }

    private void buildAddress(ImmutableTableRowData firstElement) {
      if (address == null && firstElement != null) {
        address = firstElement.getAddress().orElse(null);
      }
    }

    private void buildRegisterOrFieldName(ImmutableTableRowData firstElement) {
      if (StringUtil.isTrimmedStringEmptyOrNull(registerOrFieldName) && firstElement != null) {
        registerOrFieldName = firstElement.getRegisterOrFieldName();
      }
    }

    private void buildType(ImmutableTableRowData firstElement) {
      if (StringUtil.isTrimmedStringEmptyOrNull(type) && firstElement != null) {
        type = firstElement.getType();
      }
    }

    private void buildData(ImmutableTableRowData firstElement) {
      if (data == null && firstElement != null) {
        data = firstElement.getData().orElse(null);
      }
    }

    private void buildPage(ImmutableTableRowData firstElement) {
      if (page == null && firstElement != null) {
        page = firstElement.getPage().orElse(null);
      }
    }

    private void buildBitWidth(ImmutableTableRowData firstElement) {
      if (bitWidth == null && firstElement != null) {
        bitWidth = firstElement.getBitWidth().orElse(null);
      }
    }

    private void buildErrorReadingFromDevice(ImmutableTableRowData firstElement) {
      if (errorReadingFromDevice == null && firstElement != null) {
        errorReadingFromDevice = firstElement.getErrorReadingFromDevice();
      }
    }
  }
}

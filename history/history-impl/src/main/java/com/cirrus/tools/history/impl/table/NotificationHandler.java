/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file NotificationHandler.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.tools.history.api.TimestampedNotification;

import java.util.Optional;

@FunctionalInterface
public interface NotificationHandler {
  /**
   * Handles a Link notification (wrapped with a timestamp) by unpacking the notification into a new
   * Optional ImmutableTableRowData instance.
   *
   * @return Optional.empty() if there is no entry to be added to the History table, else a new
   * ImmutableTableRowData object populated with the relevant data from the notification.
   *
   * @throws NullPointerException if notification is null, notification.getMessage() is null.
   * Some but not all implementations <i>might</i> also throw if notification.getTopicURI() is
   * null.
   *
   * @throws ClassCastException if notification.getMessage() cannot be cast to the message type
   * expected by the implemented handler.  In principle this should never happen since a given
   * handler is selected inspecting the message type.
   */
  Optional<ImmutableTableRowData> handle(TimestampedNotification notification);
}

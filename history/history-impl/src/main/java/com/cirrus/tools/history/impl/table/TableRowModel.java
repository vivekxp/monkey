/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TableRowModel.java
 */
package com.cirrus.tools.history.impl.table;

import java.util.Objects;

/**
 * Represents a row in the History table.  Note this class does not know how to format the cells.
 * That is the responsibility of the supplied TableRowFormatter class.
 */
public class TableRowModel {
  private final ImmutableTableRowData row;
  private final TableRowFormatter formatted;
  private final boolean hasChildren;

  public TableRowModel(ImmutableTableRowData tableRowData, TableRowFormatter format) {
    this.row = Objects.requireNonNull(tableRowData);
    this.formatted = Objects.requireNonNull(format);
    this.hasChildren = !row.getChildren().isEmpty();
  }

  public String getSystemName() {
    return formatted.systemName(row.getSystemName());
  }

  public String getDeviceName() {
    return formatted.deviceName(row.getDeviceName());
  }

  public String getAddress() {
    Long addressOrNull = row.getAddress().orElse(null);
    return formatted.address(addressOrNull, hasChildren);
  }

  public String getRegisterOrFieldName() {
    return formatted.registerOrFieldName(row.getRegisterOrFieldName());
  }

  public String getType() {
    return formatted.type(row.getType());
  }

  public String getOperation() {
    return formatted.operation(row.getOperation());
  }

  public String getData() {
    Long dataOrNull = row.getData().orElse(null);
    return formatted.data(dataOrNull, hasChildren);
  }

  public String getPage() {
    Long pageOrNull = row.getPage().orElse(null);
    return formatted.page(pageOrNull);
  }

  public String getBusProtocol() {
    return formatted.busProtocol(row.getBusProtocol());
  }

  public String getDeviceAddress() {
    Integer addressOrNull = row.getDeviceAddress().orElse(null);
    return formatted.deviceAddress(addressOrNull);
  }

  public boolean rowHasError() {
    return row.rowHasError();
  }

  public String getTimestamp() {
    Long timestampOrNull = row.getTimestamp().orElse(null);
    return formatted.timestamp(timestampOrNull);
  }

  //
  /**
   * Get the elapsed time in nanoseconds since the virtual machine started
   * TimeStampColumn binds to this. Returning a raw long rather than a formatted string improves sort time.
   * @return Elapsed time in nanoseconds if set else null
   */
  public Long getElapsedTime() {
    return row.getElapsedTime().orElse(null);
  }

  @Override
  public String toString() {
    return String.format(
        "%s - %s, %s, %s, %s, %s, %s, %s, %s, %s, %s.",
        getTimestamp(),
        getSystemName(),
        getDeviceName(),
        getOperation(),
        getRegisterOrFieldName(),
        getAddress(),
        getBusProtocol(),
        getData(),
        getDeviceAddress(),
        getPage(),
        getType());
  }
}

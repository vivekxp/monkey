/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TableRowFormatter.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData.RegionEvent;
import com.cirrus.tools.history.api.StringUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class knows how to take the data from a ImmutableTableRowData and convert into a TableRowModel
 * instance ready to be added to the table.  This helps to separate the parsing of Link
 * notifications, and their display format in the table.
 */
@SuppressWarnings("PMD.TooManyMethods") //A method is required for each field in the table row
public class TableRowFormatter {
  private static final String NONE = "N/A";
  private static final String MULTIPLE_ADDRESSES = "(...)";
  private static final String MULTIPLE_DATA = " (...)";
  private static final String ADDRESS_HEX_FORMAT = "0x%02x";
  private static final String DATA_HEX_FORMAT = "0x%08x";
  private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd_HH:mm:ss.sss"; //As per SCS
  private static final Map<String, String> OPERATIONS_LOOKUP = newOperationMnemonicMap();

  /** Gets the formatted value.  Null or empty param is valid and handled. */
  public String systemName(String systemName) {
    return stringOrNone(systemName);
  }

  /** Gets the formatted value.  Null or empty param is valid and handled. */
  public String deviceName(String deviceName) {
    return stringOrNone(deviceName);
  }

  /** Gets the formatted value.  Null or empty param is valid and handled. */
  public String address(Long address, boolean hasMultiple) {
    return toAddressHex(address, hasMultiple);
  }

  /** Gets the formatted value.  Null or empty param is valid and handled. */
  public String registerOrFieldName(String registerOrFieldName) {
    return stringOrNone(registerOrFieldName);
  }

  /** Gets the formatted value.  Null or empty param is valid and handled. */
  public String type(String type) {
    return stringOrNone(type);
  }

  /** Gets the formatted value.  Null or empty param is valid and handled. */
  public String operation(String operation) {
    if (StringUtil.isTrimmedStringEmptyOrNull(operation)) {
      return NONE;
    }
    return OPERATIONS_LOOKUP.getOrDefault(operation.toLowerCase(), operation);
  }

  /** Gets the formatted value.  Null or empty param is valid and handled. */
  public String data(Long data, boolean hasMultiple) {
    return toDataHex(data, hasMultiple);
  }

  /** Gets the formatted value.  Null or empty param is valid and handled. */
  public String page(Long page) {
    boolean hasMultiple = false;
    return toAddressHex(page, hasMultiple);
  }

  /** Gets the formatted value.  Null or empty param is valid and handled. */
  public String busProtocol(String busProtocol) {
    return stringOrNone(busProtocol);
  }

  /** Gets the formatted value.  Null or empty param is valid and handled. */
  public String deviceAddress(Integer deviceAddress) {
    if (deviceAddress == null) {
      return NONE;
    }

    return String.format(ADDRESS_HEX_FORMAT, deviceAddress);
  }

  /** Gets the formatted value.  Null or empty param is valid and handled. */
  public String timestamp(Long timestamp) {
    if (timestamp == null) {
      return "";
    }

    @SuppressWarnings("PMD.SimpleDateFormatNeedsLocale") //We want default/system locale
    DateFormat df = new SimpleDateFormat(TIMESTAMP_FORMAT);
    return df.format(new Date(timestamp));
  }

  private String stringOrNone(String s) {
    return StringUtil.isTrimmedStringEmptyOrNull(s) ? NONE : s.trim();
  }

  private String toAddressHex(Long value, boolean hasMultiple) {
    if (value == null) {
      return NONE;
    }

    return String.format(
        "%s%s",
        String.format(ADDRESS_HEX_FORMAT, value),
        hasMultiple ? MULTIPLE_ADDRESSES : "");
  }

  private String toDataHex(Long value, boolean hasMultiple) {
    if (value == null) {
      return NONE;
    }

    return String.format(
        "%s%s",
        String.format(DATA_HEX_FORMAT, value),
        hasMultiple ? MULTIPLE_DATA : "");
  }

  private static Map<String, String> newOperationMnemonicMap() {
    Map<String, String> ops = new HashMap<>();

    ops.put(RegionEvent.BLOCK_READ.toString().toLowerCase(), "BR");
    ops.put(RegionEvent.BLOCK_WRITE.toString().toLowerCase(), "BW");
    ops.put(RegionEvent.READ.toString().toLowerCase(), "R");
    ops.put(RegionEvent.WRITE.toString().toLowerCase(), "W");
    ops.put(RegionEvent.VALUE_CHANGE.toString().toLowerCase(), "VC");

    return Collections.unmodifiableMap(ops);
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file LinkVersionCheckerTest.java
 */
package com.cirrus.tools.history.impl;

import com.cirrus.scs.studiolink.api.IStudioLink;
import com.cirrus.scs.studiolink.api.exceptions.StudioLinkException;
import com.cirrus.scs.studiolink.api.utils.Version;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LinkVersionCheckerTest {
  private final Version v123 = new Version(1, 2, 3);
  private final Version v321 = new Version(3, 2, 1);

  @Test
  public void verify_clientLessThanServer_noExceptions() throws StudioLinkException {
    IStudioLink link = mock(IStudioLink.class);
    when(link.getClientAPIVersion()).thenReturn(v123);
    when(link.getStudioLinkVersion()).thenReturn(v321);

    LinkVersionChecker sut = new LinkVersionChecker();

    sut.verify(link);
  }

  @Test
  public void verify_clientEqualsServer_noExceptions() throws StudioLinkException {
    IStudioLink link = mock(IStudioLink.class);
    when(link.getClientAPIVersion()).thenReturn(v123);
    when(link.getStudioLinkVersion()).thenReturn(v123);

    LinkVersionChecker sut = new LinkVersionChecker();

    sut.verify(link);
  }

  @Test(expected = IllegalStateException.class)
  public void verify_clientGreaterThanServer_throwsIllegalStateException() throws StudioLinkException {
    IStudioLink link = mock(IStudioLink.class);
    when(link.getClientAPIVersion()).thenReturn(v321);
    when(link.getStudioLinkVersion()).thenReturn(v123);

    LinkVersionChecker sut = new LinkVersionChecker();

    sut.verify(link);
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file DefaultConfigTest.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.notifications.AsyncRequestMessageData;
import com.cirrus.scs.studiolink.api.notifications.PollingMessageData;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.SecureMessageData;
import com.cirrus.scs.studiolink.api.notifications.UnsolicitedMessageData;
import com.cirrus.tools.history.api.TimestampedNotification;

import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultConfigTest {

  @Test(expected = Exception.class)
  public void handle_notificationNull_exceptionThrown() {
    DefaultConfig sut = new DefaultConfig();

    sut.handle(null);
  }

  @Test(expected = Exception.class)
  public void handle_messageNull_exceptionThrown() {
    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(null);

    DefaultConfig sut = new DefaultConfig();

    sut.handle(mockNotification);
  }

  @Test
  public void getTopics_happyPath_setContainsUnsolicitedMessage() {
    DefaultConfig sut = new DefaultConfig();

    Set<String> topics = sut.getTopics();

    assertTrue(topics.contains(UnsolicitedMessageData.TOPIC_UNSOLICITED_MESSAGE));
  }

  @Test
  public void getTopics_happyPath_setContainsSecureMessage() {
    DefaultConfig sut = new DefaultConfig();

    Set<String> topics = sut.getTopics();

    assertTrue(topics.contains(SecureMessageData.TOPIC_SECURE_MESSAGE));
  }

  @Test
  public void getTopics_happyPath_setContainsAsyncRequest() {
    DefaultConfig sut = new DefaultConfig();

    Set<String> topics = sut.getTopics();

    assertTrue(topics.contains(AsyncRequestMessageData.TOPIC_ASYNC_REQUEST_MESSAGE));
  }

  @Test
  public void getTopics_happyPath_setContainsOnChange() {
    DefaultConfig sut = new DefaultConfig();

    Set<String> topics = sut.getTopics();

    assertTrue(topics.contains(RegionChangeMessageData.TOPIC_IDATAELEMENT_CHANGE));
  }

  @Test
  public void getTopics_happyPath_setContainsPollingMessage() {
    DefaultConfig sut = new DefaultConfig();

    Set<String> topics = sut.getTopics();

    assertTrue(topics.contains(PollingMessageData.TOPIC_POLLING_MESSAGE));
  }
}

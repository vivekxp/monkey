/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file SecureHandlerTest.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.enums.ControlInterface.ControlInterfaceValues;
import com.cirrus.scs.studiolink.api.notifications.SecureMessageData;
import com.cirrus.tools.history.api.TimestampedNotification;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings({ "OptionalGetWithoutIsPresent", // Shouldn't apply to test classes
    "PMD.TooManyMethods" }) // Ignoring as its nice to have all the test methods together
public class SecureHandlerTest {
  private final SecureHandler sut = new SecureHandler();

  @Test(expected = Exception.class)
  public void handle_notificationIsNull_exceptionThrown() {
    sut.handle(null);
  }

  @Test(expected = Exception.class)
  public void handle_messageIsNull_exceptionThrown() {
    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(null);

    sut.handle(mockNotification);
  }

  @Test
  public void handle_happyPath_systemNameCorrect() {
    //Given
    String expected = "system name";

    //Arrange
    SecureMessageData mockMsg = mock(SecureMessageData.class);
    when(mockMsg.getSystemName()).thenReturn(expected);

    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(mockMsg);

    //Act
    String actual = sut.handle(mockNotification).get().getSystemName();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_deviceNameCorrect() {
    //Given
    String expected = "device name";

    //Arrange
    SecureMessageData mockMsg = mock(SecureMessageData.class);
    when(mockMsg.getDeviceName()).thenReturn(expected);

    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(mockMsg);

    //Act
    String actual = sut.handle(mockNotification).get().getDeviceName();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_addressCorrect() {
    //Given
    int expected = 50;

    //Arrange
    SecureMessageData mockMsg = mock(SecureMessageData.class);
    when(mockMsg.getDeviceName()).thenReturn("any device name");
    when(mockMsg.getRegisterAddress()).thenReturn(expected);

    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(mockMsg);

    //Act
    long actual = sut.handle(mockNotification).get().getAddress().get();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_registerOrFieldNameMatchesSecureMessageComment() {
    //Given
    int anyServiceType = 7;
    int anyMessageId = 15;
    int anyStatus = 25;
    boolean anyIsBulk = false;
    String anyDeviceType = "any device type";
    String anyDeviceName = "any device name";
    String anyMessageTypeName = "any message type name";

    //Arrange
    SecureMessageData mockMsg = mock(SecureMessageData.class);
    when(mockMsg.getServiceType()).thenReturn(anyServiceType);
    when(mockMsg.isBulk()).thenReturn(anyIsBulk);
    when(mockMsg.getMessageTypeName()).thenReturn(anyMessageTypeName);
    when(mockMsg.getMessageId()).thenReturn(anyMessageId);
    when(mockMsg.getStatus()).thenReturn(anyStatus);
    when(mockMsg.getDeviceType()).thenReturn(anyDeviceType);
    when(mockMsg.getDeviceName()).thenReturn(anyDeviceName);

    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(mockMsg);

    //Act
    String actual = sut.handle(mockNotification).get().getRegisterOrFieldName();

    //Assert
    String expected = String.format(
        "%d,%b,%s,%d,%d,%s,%s",
        anyServiceType,
        anyIsBulk,
        anyMessageTypeName,
        anyMessageId,
        anyStatus,
        anyDeviceType,
        anyDeviceName);

    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_deviceAddressCorrect() {
    //Given
    int expected = 60;

    //Arrange
    SecureMessageData mockMsg = mock(SecureMessageData.class);
    when(mockMsg.getDeviceName()).thenReturn("any device name");
    when(mockMsg.getDeviceAddress()).thenReturn(expected);

    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(mockMsg);

    //Act
    int actual = sut.handle(mockNotification).get().getDeviceAddress().get();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void handle_messageHasControlInterface_busProtocolEqualsControlInterface() {
    //Given
    ControlInterfaceValues expected = ControlInterfaceValues.CTRL_SPI;

    //Arrange
    SecureMessageData mockMsg = mock(SecureMessageData.class);
    when(mockMsg.getDeviceName()).thenReturn("any device name");
    when(mockMsg.getControlInterfaceType()).thenReturn(expected);

    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(mockMsg);

    //Act
    String actual = sut.handle(mockNotification).get().getBusProtocol();

    //Assert
    assertEquals(expected.toString(), actual);
  }
  @Test
  public void handle_messageHasNoControlInterface_busProtocolFieldEmpty() {
    //Arrange
    SecureMessageData mockMsg = mock(SecureMessageData.class);
    when(mockMsg.getDeviceName()).thenReturn("any device name");

    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(mockMsg);

    //Act
    String actual = sut.handle(mockNotification).get().getBusProtocol();

    //Assert
    assertTrue(actual.isEmpty());
  }

  @Test
  public void handle_happyPath_timestampCorrect() {
    //Given
    long expected = System.currentTimeMillis();

    //Arrange
    SecureMessageData mockMsg = mock(SecureMessageData.class);
    when(mockMsg.getDeviceName()).thenReturn("any device name");

    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(mockMsg);
    when(mockNotification.getTimestamp()).thenReturn(expected);

    //Act
    long actual = sut.handle(mockNotification).get().getTimestamp().get();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_elapsedTimeCorrect() {
    //Given
    long expected = System.nanoTime();

    //Arrange
    SecureMessageData mockMsg = mock(SecureMessageData.class);
    when(mockMsg.getDeviceName()).thenReturn("any device name");

    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(mockMsg);
    when(mockNotification.getElapsedTime()).thenReturn(expected);

    //Act
    long actual = sut.handle(mockNotification).get().getElapsedTime().get();

    //Assert
    assertEquals(expected, actual);
  }
}

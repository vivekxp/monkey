/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file PollingMessageDataHandlerTest.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.enums.ControlInterface.ControlInterfaceValues;
import com.cirrus.scs.studiolink.api.notifications.PollingMessageData;
import com.cirrus.tools.history.api.TimestampedNotification;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings({"PMD.TooManyMethods", "OptionalGetWithoutIsPresent"}) //Shouldn't apply to test classes
public class PollingMessageDataHandlerTest {
  private static final String ATTEMPTS_FORMAT = "attempts %d,%d";

  private final PollingMessageDataHandler sut = new PollingMessageDataHandler(ATTEMPTS_FORMAT);

  @Test(expected = Exception.class)
  public void construct_paramIsNull_exceptionThrown() {
    new PollingMessageDataHandler(null);
  }

  @Test(expected = Exception.class)
  public void construct_regexMissingPlaceholder_exceptionThrown() {
    new PollingMessageDataHandler("%d");
  }

  @Test
  public void construct_minimumRegex_success() {
    new PollingMessageDataHandler("%d%d");
  }

  @Test
  public void construct_regexWithPadding_success() {
    new PollingMessageDataHandler("pad %d pad %d pad");
  }

  @Test(expected = Exception.class)
  public void handle_notificationIsNull_exceptionThrown() {
    sut.handle(null);
  }

  @Test(expected = Exception.class)
  public void handle_messageIsNull_exceptionThrown() {
    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(null);

    sut.handle(mockNotification);
  }

  @Test
  public void handle_happyPath_systemNameCorrect() {
    //Given
    String expected = "system name";

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withSystemName(expected)
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getSystemName();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_deviceNameCorrect() {
    //Given
    String expected = "device name";

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withDeviceName(expected)
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getDeviceName();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_deviceAddressCorrect() {
    //Given
    int expected = 77;

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withDeviceAddress(expected)
        .build();

    //Act
    int actual = sut.handle(mockNotification).get().getDeviceAddress().get();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void handle_messageHasNoControlInterface_busProtocolFieldEmpty() {
    TimestampedNotification mockNotification = new Fixture().build();

    //Act
    String actual = sut.handle(mockNotification).get().getBusProtocol();

    //Assert
    assertTrue(actual.isEmpty());
  }

  @Test
  public void handle_messageHasControlInterface_busProtocolEqualsControlInterface() {
    //Given
    ControlInterfaceValues expected = ControlInterfaceValues.CTRL_SPI;

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withControlInterface(expected)
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getBusProtocol();

    //Assert
    assertEquals(expected.toString(), actual);
  }

  @Test
  public void handle_happyPath_typeFieldEqualsPollingAttemptsFormat() {
    //Given
    int attempts = 3;
    int maxAttempts = 5;

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withAttempts(attempts)
        .withMaxAttempts(maxAttempts)
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getType();

    //Assert
    String expected = String.format(ATTEMPTS_FORMAT, attempts, maxAttempts);
    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_regFieldNameEqualsMessageInterruptedRegister() {
    //Given
    String expected = "interrupted register";

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withMessageInterruptRegister(expected)
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getRegisterOrFieldName();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_addressFieldEqualsRegisterAddress() {
    //Given
    int expected = 3800;

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withRegisterAddress(expected)
        .build();

    //Act
    Long actual = sut.handle(mockNotification).get().getAddress().get();

    //Assert
    assertEquals(Long.valueOf(expected), actual);
  }

  @Test
  public void handle_happyPath_timestampCorrect() {
    //Given
    long expected = System.currentTimeMillis();

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withTimestamp(expected)
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getTimestamp().get();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_elapsedTimeCorrect() {
    //Given
    long expected = System.nanoTime();

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withElapsedTime(expected)
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getElapsedTime().get();

    //Assert
    assertEquals(expected, actual);
  }

  private static class Fixture {
    private final TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    private final PollingMessageData mockMessage = mock(PollingMessageData.class);

    private String systemName = "any system name";
    private String deviceName = "any device name";
    private int deviceAddress = 200;  //any
    private String messageInterruptRegister = "any message interrupt register";

    public Fixture withTimestamp(long timestamp) {
      when(mockNotification.getTimestamp()).thenReturn(timestamp);
      return this;
    }

    public Fixture withElapsedTime(long elapsedTime) {
      when(mockNotification.getElapsedTime()).thenReturn(elapsedTime);
      return this;
    }

    public Fixture withSystemName(String sn) {
      this.systemName = sn;
      return this;
    }

    public Fixture withDeviceName(String dn) {
      this.deviceName = dn;
      return this;
    }

    public Fixture withDeviceAddress(int da) {
      this.deviceAddress = da;
      return this;
    }

    public Fixture withMessageInterruptRegister(String mir) {
      this.messageInterruptRegister = mir;
      return this;
    }

    public Fixture withControlInterface(ControlInterfaceValues controlInterface) {
      when(mockMessage.getControlInterfaceType()).thenReturn(controlInterface);
      return this;
    }

    public Fixture withRegisterAddress(int registerAddress) {
      when(mockMessage.getRegisterAddress()).thenReturn(registerAddress);
      return this;
    }

    public Fixture withAttempts(int attempts) {
      when(mockMessage.getAttempts()).thenReturn(attempts);
      return this;
    }

    public Fixture withMaxAttempts(int maxAttempts) {
      when(mockMessage.getMaxAttempts()).thenReturn(maxAttempts);
      return this;
    }

    public TimestampedNotification build() {
      when(mockMessage.getSystemName()).thenReturn(systemName);
      when(mockMessage.getDeviceName()).thenReturn(deviceName);
      when(mockMessage.getDeviceAddress()).thenReturn(deviceAddress);
      when(mockMessage.getMessageInterruptRegister()).thenReturn(messageInterruptRegister);
      when(mockNotification.getMessage()).thenReturn(mockMessage);

      return mockNotification;
    }
  }
}

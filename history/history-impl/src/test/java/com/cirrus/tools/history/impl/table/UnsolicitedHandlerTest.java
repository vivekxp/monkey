/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file UnsolicitedHandlerTest.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.notifications.UnsolicitedMessageData;
import com.cirrus.tools.history.api.TimestampedNotification;
import org.junit.Test;

import java.util.Optional;

import static com.cirrus.scs.studiolink.api.notifications.UnsolicitedMessageData.TOPIC_UNSOLICITED_MESSAGE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings({"PMD.TooManyMethods", "OptionalGetWithoutIsPresent"}) //Shouldn't apply to test classes
public class UnsolicitedHandlerTest {
  private static final String BYTE_STRING_FORMAT = "%02X ";

  private final UnsolicitedHandler sut = new UnsolicitedHandler(BYTE_STRING_FORMAT);

  @Test(expected = Exception.class)
  public void construct_paramIsNull_exceptionThrown() {
    new UnsolicitedHandler(null);
  }

  @Test(expected = Exception.class)
  public void construct_regexMissingPlaceholder_exceptionThrown() {
    new UnsolicitedHandler("");
  }

  @Test
  public void construct_minimumRegexLowercase_success() {
    new UnsolicitedHandler("%x");
  }

  @Test
  public void construct_minimumRegexUppercase_success() {
    new UnsolicitedHandler("%X");
  }

  @Test
  public void construct_regexWithPadding_success() {
    new UnsolicitedHandler("pad %X pad");
  }

  @Test(expected = Exception.class)
  public void handle_notificationIsNull_exceptionThrown() {
    sut.handle(null);
  }

  @Test(expected = Exception.class)
  public void handle_messageIsNull_exceptionThrown() {
    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getTopicURI()).thenReturn(TOPIC_UNSOLICITED_MESSAGE);
    when(mockNotification.getMessage()).thenReturn(null);

    sut.handle(mockNotification);
  }

  @Test(expected = Exception.class)
  public void handle_topicIsNull_exceptionThrown() {
    TimestampedNotification mockNotification = new Fixture()
        .withTopic(null)
        .build();

    sut.handle(mockNotification);
  }

  @Test(expected = Exception.class)
  public void handle_messageDataIsNull_exceptionThrown() {
    TimestampedNotification mockNotification = new Fixture()
        .withMessageData(null)
        .build();

    sut.handle(mockNotification);
  }

  @Test
  public void handle_incorrectTopic_rowNotPresent() {
    TimestampedNotification mockNotification = new Fixture()
        .withTopic("wrong topic")
        .build();

    Optional<ImmutableTableRowData> actual = sut.handle(mockNotification);

    assertFalse(actual.isPresent());
  }

  @Test
  public void handle_happyPath_regFieldNameSetCorrectly() {
    byte[] data = new byte[] {7, 8, 9};

    TimestampedNotification mockNotification = new Fixture()
        .withMessageData(data)
        .build();

    String actual = sut.handle(mockNotification).get().getRegisterOrFieldName();

    String expected = "07 08 09 ";
    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_opSetCorrectly() {
    TimestampedNotification mockNotification = new Fixture()
        .build();

    String actual = sut.handle(mockNotification).get().getOperation();

    String expected = "Unsolicited";
    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_timestampSetCorrectly() {
    long expected = System.currentTimeMillis();

    TimestampedNotification mockNotification = new Fixture()
        .withTimestamp(expected)
        .build();

    long actual = sut.handle(mockNotification).get().getTimestamp().get();

    assertEquals(expected, actual);
  }

  @Test
  public void handle_happyPath_elapsedTimeSetCorrectly() {
    long expected = System.currentTimeMillis();

    TimestampedNotification mockNotification = new Fixture()
        .withElapsedTime(expected)
        .build();

    long actual = sut.handle(mockNotification).get().getElapsedTime().get();

    assertEquals(expected, actual);
  }

  private static class Fixture {
    private final TimestampedNotification mock = mock(TimestampedNotification.class);

    private String topic = TOPIC_UNSOLICITED_MESSAGE;
    private byte[] messageData = new byte[] {1, 2, 3};

    public Fixture withTopic(String t) {
      this.topic = t;
      return this;
    }

    public Fixture withTimestamp(long timestamp) {
      when(mock.getTimestamp()).thenReturn(timestamp);
      return this;
    }

    public Fixture withElapsedTime(long elapsedTime) {
      when(mock.getElapsedTime()).thenReturn(elapsedTime);
      return this;
    }

    @SuppressWarnings("PMD.ArrayIsStoredDirectly") //Unnecessary check within this test
    public Fixture withMessageData(byte[] data) {
      this.messageData = data;
      return this;
    }

    public TimestampedNotification build() {
      UnsolicitedMessageData mockMsg = mock(UnsolicitedMessageData.class);
      when(mockMsg.getMessageData()).thenReturn(messageData);

      when(mock.getMessage()).thenReturn(mockMsg);
      when(mock.getTopicURI()).thenReturn(topic);

      return mock;
    }
  }
}

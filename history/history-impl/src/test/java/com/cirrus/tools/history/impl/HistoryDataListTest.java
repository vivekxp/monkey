//  Copyright (c) 2020 Cirrus Logic, Inc and
//  Cirrus Logic International Semiconductor Ltd.  All rights reserved.
//
//  This software as well as any related documentation is furnished under
//  license and may only be used or copied in accordance with the terms of the
//  license.  The information in this file is furnished for informational use
//  only, is subject to change without notice, and should not be construed as
//  a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
//  liability for any errors or inaccuracies that may appear in this document
//  or any software that may be provided in association with this document.
//
//  Except as permitted by such license, no part of this document may be
//  reproduced, stored in a retrieval system, or transmitted in any form or by
//  any means without the express written consent of Cirrus Logic.
//
//  Warning
//    This software is specifically written for Cirrus Logic devices.
//    It may not be used with other devices.
//
//! @file   HistoryDataListTest.java
package com.cirrus.tools.history.impl;

import com.cirrus.scs.studiolink.api.notifications.INotificationManager;
import com.cirrus.scs.studiolink.api.notifications.ISubscriber;
import com.cirrus.tools.history.api.HistoryConfig;
import com.cirrus.tools.history.impl.table.TableRowModel;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("PMD.TooManyMethods") //Shouldn't apply to test classes
public class HistoryDataListTest {

  @Test(expected = NullPointerException.class)
  public void constructor_listIsNull_throwsException() {
    HistoryConfig<TableRowModel> config = newMockHistoryConfig();

    new HistoryDataList(null, runnable -> runnable.run(), config);
  }

  @Test(expected = NullPointerException.class)
  public void constructor_runnerIsNull_throwsException() {
    HistoryConfig<TableRowModel> config = newMockHistoryConfig();

    new HistoryDataList(new ArrayList<>(), null, config);
  }

  @Test(expected = NullPointerException.class)
  public void constructor_historyConfigIsNull_throwsException() {

    new HistoryDataList(new ArrayList<>(), runnable -> runnable.run(), null);
  }

  @Test
  public void constructor_paramsNotNull_noExceptions() {
    newHistoryDataWithMockedMembers();
  }

  @Test
  public void subscribeToTopics_happyPath_atLeastOneTopicSubscribedTo() {
    INotificationManager mockNotificationManager = mock(INotificationManager.class);
    HistoryDataList historyData = new HistoryDataList(new ArrayList<>(),
        runnable -> runnable.run(), newMockHistoryConfig());

    historyData.start(mockNotificationManager);

    verify(mockNotificationManager).addSubscriber(any(ISubscriber.class), anyString());
  }

  @Test
  public void start_notPreviouslySubscribed_returnsTrue() {
    INotificationManager mockNotificationManager = mock(INotificationManager.class);
    HistoryDataList historyData = new HistoryDataList(new ArrayList<>());

    boolean notPreviouslySubscribed = historyData.start(mockNotificationManager);

    assertTrue(notPreviouslySubscribed);
  }

  @Test
  public void start_previouslySubscribed_returnsFalse() {
    INotificationManager mockNotificationManager = mock(INotificationManager.class);
    HistoryDataList historyData = new HistoryDataList(new ArrayList<>());
    historyData.start(mockNotificationManager);

    boolean notPreviouslySubscribed = historyData.start(mockNotificationManager);

    assertFalse(notPreviouslySubscribed);
  }

  @Test
  public void stop_happyPath_subscriberRemoved() {
    INotificationManager mockNotificationManager = mock(INotificationManager.class);
    HistoryDataList historyData = new HistoryDataList(new ArrayList<>());

    historyData.start(mockNotificationManager);

    historyData.stop();

    verify(mockNotificationManager).removeSubscriber(any(ISubscriber.class));
  }
  /*
   * Unit tests for:
   * - getColumnManager()
   * - setDisplayButtonClickHandler()
   * - clearTable()
   * to be added as part of:
   * https://tracker.cirrus.com/browse/SCSSTU-897
   *
   * These methods are currently tested via integration tests.
   */

  private HistoryDataList newHistoryDataWithMockedMembers() {
    HistoryConfig<TableRowModel> mockConfig = newMockHistoryConfig();
    return  new HistoryDataList(new ArrayList<>(), Runnable::run, mockConfig);
  }

  @SuppressWarnings("unchecked") //Mocks are not generic so no choice but to cast
  private HistoryConfig<TableRowModel> newMockHistoryConfig() {
    HistoryConfig<TableRowModel> mockConfig = mock(HistoryConfig.class);
    when(mockConfig.getTopics()).thenReturn(Collections.singleton("topic"));
    return mockConfig;
  }
}

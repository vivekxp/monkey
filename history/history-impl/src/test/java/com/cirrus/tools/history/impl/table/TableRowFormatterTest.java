/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TableRowFormatterTest.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData.RegionEvent;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@SuppressWarnings({"PMD.TooManyMethods", "PMD.ExcessivePublicCount"})  //Shouldn't apply to test classes
public class TableRowFormatterTest {
  private static final String NONE = "N/A";
  private static final String MULTIPLE_DATA = "(...)";
  private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd_HH:mm:ss.sss"; //As per SCS

  private final TableRowFormatter sut = new TableRowFormatter();

  @Test
  public void registerOrFieldName_inputIsNull_outputIsNone() {
    String actual = sut.registerOrFieldName(null);

    assertEquals(NONE, actual);
  }

  @Test
  public void registerOrFieldName_inputIsEmpty_outputIsNone() {
    String actual = sut.registerOrFieldName("");

    assertEquals(NONE, actual);
  }

  @Test
  public void registerOrFieldName_inputIsWhitespace_outputIsNone() {
    String actual = sut.registerOrFieldName(" \n\t");

    assertEquals(NONE, actual);
  }

  @Test
  public void registerOrFieldName_inputGiven_outputIsSame() {
    String expected = "name";

    String actual = sut.registerOrFieldName(expected);

    assertEquals(expected, actual);
  }

  @Test
  public void registerOrFieldName_inputGivenWithWhitespace_outputIsSameButTrimmed() {
    String expected = "  name  ";

    String actual = sut.registerOrFieldName(expected);

    assertEquals(expected.trim(), actual);
  }

  @Test
  public void type_inputIsNull_outputIsNone() {
    String actual = sut.type(null);

    assertEquals(NONE, actual);
  }

  @Test
  public void type_inputIsEmpty_outputIsNone() {
    String actual = sut.type("");

    assertEquals(NONE, actual);
  }

  @Test
  public void type_inputIsWhitespace_outputIsNone() {
    String actual = sut.type(" \t\n  ");

    assertEquals(NONE, actual);
  }

  @Test
  public void type_inputGiven_outputIsSame() {
    String expected = "T1";

    String actual = sut.type(expected);

    assertEquals(expected, actual);
  }

  @Test
  public void type_inputGivenWithWhitespace_outputIsSameButTrimmed() {
    String expected = "  type  ";

    String actual = sut.type(expected);

    assertEquals(expected.trim(), actual);
  }

  @Test
  public void operation_inputIsNull_outputIsNone() {
    String actual = sut.operation(null);

    assertEquals(NONE, actual);
  }

  @Test
  public void operation_inputIsEmpty_outputIsNone() {
    String actual = sut.operation("");

    assertEquals(NONE, actual);
  }

  @Test
  public void operation_inputIsUnknown_outputIsSame() {
    String expected = "no enum for this op";

    String actual = sut.operation(expected);

    assertEquals(expected, actual);
  }

  @Test
  public void operation_inputIsBlockRead_outputIsAbbreviated() {
    String actual = sut.operation(RegionEvent.BLOCK_READ.toString());

    assertEquals("BR", actual);
  }

  @Test
  public void operation_inputIsBlockWrite_outputIsAbbreviated() {
    String actual = sut.operation(RegionEvent.BLOCK_WRITE.toString());

    assertEquals("BW", actual);
  }

  @Test
  public void operation_inputIsWrite_outputIsAbbreviated() {
    String actual = sut.operation(RegionEvent.WRITE.toString());

    assertEquals("W", actual);
  }

  @Test
  public void operation_inputIsRead_outputIsAbbreviated() {
    String actual = sut.operation(RegionEvent.READ.toString());

    assertEquals("R", actual);
  }

  @Test
  public void operation_inputIsValueChange_outputIsAbbreviated() {
    String actual = sut.operation(RegionEvent.VALUE_CHANGE.toString());

    assertEquals("VC", actual);
  }

  @Test
  public void timestamp_inputIsNull_outputIsEmpty() {
    String actual = sut.timestamp(null);

    assertEquals("", actual);
  }

  @Test
  public void busProtocol_inputIsNull_outputIsNone() {
    String actual = sut.busProtocol(null);

    assertEquals(NONE, actual);
  }

  @Test
  public void timestamp_inputIsGiven_timestampFormattedCorrect() {
    long timestamp = System.currentTimeMillis();

    String actual = sut.timestamp(timestamp);

    @SuppressWarnings("PMD.SimpleDateFormatNeedsLocale") //We want default/system locale
    String expected = new SimpleDateFormat(TIMESTAMP_FORMAT).format(new Date(timestamp));
    assertEquals(expected, actual);
  }

  @Test
  public void busProtocol_inputIsEmpty_outputIsNone() {
    String actual = sut.busProtocol("");

    assertEquals(NONE, actual);
  }

  @Test
  public void busProtocol_inputIsWhitespace_outputIsNone() {
    String actual = sut.busProtocol(" \t\n  ");

    assertEquals(NONE, actual);
  }

  @Test
  public void busProtocol_inputGiven_outputIsSame() {
    String expected = "bus protocol";

    String actual = sut.busProtocol(expected);

    assertEquals(expected, actual);
  }

  @Test
  public void busProtocol_inputGivenWithWhitespace_outputIsSameButTrimmed() {
    String expected = "  bus protocol  ";

    String actual = sut.busProtocol(expected);

    assertEquals(expected.trim(), actual);
  }

  @Test
  public void systemName_inputIsNull_outputIsNone() {
    String actual = sut.systemName(null);

    assertEquals(NONE, actual);
  }

  @Test
  public void systemName_inputIsEmpty_outputIsNone() {
    String actual = sut.systemName("");

    assertEquals(NONE, actual);
  }

  @Test
  public void systemName_inputIsWhitespace_outputIsNone() {
    String actual = sut.systemName("  \t\n  ");

    assertEquals(NONE, actual);
  }

  @Test
  public void systemName_inputGiven_outputIsSame() {
    String expected = "systemName";

    String actual = sut.systemName(expected);

    assertEquals(expected, actual);
  }

  @Test
  public void systemName_inputGivenWithWhitespace_outputIsSameButTrimmed() {
    String expected = "  systemName  ";

    String actual = sut.systemName(expected);

    assertEquals(expected.trim(), actual);
  }

  @Test
  public void deviceName_inputIsNull_outputIsNone() {
    String actual = sut.deviceName(null);

    assertEquals(NONE, actual);
  }

  @Test
  public void deviceName_inputIsEmpty_outputIsNone() {
    String actual = sut.deviceName("");

    assertEquals(NONE, actual);
  }

  @Test
  public void deviceName_inputIsWhitespace_outputIsNone() {
    String actual = sut.deviceName("  \t\n  ");

    assertEquals(NONE, actual);
  }

  @Test
  public void deviceName_inputGiven_outputIsSame() {
    String expected = "deviceName";

    String actual = sut.deviceName(expected);

    assertEquals(expected, actual);
  }

  @Test
  public void deviceName_inputGivenWithWhitespace_outputIsSameButTrimmed() {
    String expected = "  deviceName  ";

    String actual = sut.deviceName(expected);

    assertEquals(expected.trim(), actual);
  }

  @Test
  public void address_inputIsNullMultipleIsFalse_outputIsNone() {
    String actual = sut.address(null, false);

    assertEquals(NONE, actual);
  }

  @Test
  public void address_inputIsNullMultipleIsTrue_outputIsNone() {
    String actual = sut.address(null, true);

    assertEquals(NONE, actual);
  }

  @Test
  public void address_inputGivenMultipleIsFalse_outputIsSame() {
    Long address = 100L;

    String actual = sut.address(address, false);

    String expected = String.format("%#x", address);
    assertEquals(expected, actual);
  }

  @Test
  public void address_inputGivenMultipleIsTrue_outputIsSameWithEllipse() {
    Long address = 100L;

    String actual = sut.address(address, true);

    String expected = String.format("%#x%s", address, MULTIPLE_DATA);
    assertEquals(expected, actual);
  }

  @Test
  public void deviceAddress_inputIsNull_outputIsNone() {
    String actual = sut.deviceAddress(null);

    assertEquals(NONE, actual);
  }

  @Test
  public void deviceAddress_inputGiven_outputIsSame() {
    int expected = 100;

    String actual = sut.deviceAddress(expected);

    assertEquals(String.format("%#x", expected), actual);
  }

  @Test
  public void data_inputIsNullMultipleIsFalse_outputIsNone() {
    String actual = sut.data(null, false);

    assertEquals(NONE, actual);
  }

  @Test
  public void data_inputIsNullMultipleIsTrue_outputIsNone() {
    String actual = sut.data(null, true);

    assertEquals(NONE, actual);
  }

  @Test
  public void data_inputGivenMultipleIsFalse_outputIsSame() {
    Long data = 200L;

    String actual = sut.data(data, false);

    String expected = String.format("0x%08x", data);
    assertEquals(expected, actual);
  }

  @Test
  public void data_inputGivenMultipleIsTrue_outputIsSameWithEllipse() {
    Long data = 300L;

    String actual = sut.data(data, true);

    String expected = String.format("0x%08x %s", data, MULTIPLE_DATA);
    assertEquals(expected, actual);
  }

  @Test
  public void page_inputIsNull_outputIsNone() {
    String actual = sut.page(null);

    assertEquals(NONE, actual);
  }

  @Test
  public void page_inputGiven_outputIsSame() {
    Long page = 200L;

    String actual = sut.page(page);

    String expected = String.format("%#x", page);
    assertEquals(expected, actual);
  }
}

/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file RegionChangeHandlerTest.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.scs.studiolink.api.enums.ControlInterface.ControlInterfaceValues;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeElement.ElementType;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData;
import com.cirrus.scs.studiolink.api.notifications.RegionChangeMessageData.RegionEvent;
import com.cirrus.tools.history.api.TimestampedNotification;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

//Shouldn't apply to test classes
@SuppressWarnings({
    "PMD.TooManyMethods", "PMD.ExcessivePublicCount", "PMD.ExcessiveClassLength", "OptionalGetWithoutIsPresent"
})

public class RegionChangeHandlerTest {
  private final RegionChangeHandler sut = new RegionChangeHandler();

  @Test(expected = Exception.class)
  public void handle_notificationIsNull_exceptionThrown() {
    sut.handle(null);
  }

  @Test(expected = Exception.class)
  public void handle_messageIsNull_exceptionThrown() {
    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(null);

    sut.handle(mockNotification);
  }

  @Test(expected = Exception.class)
  public void handle_changedElementsIsNull_exceptionThrown() {
    RegionChangeMessageData mockMsg = mock(RegionChangeMessageData.class);
    when(mockMsg.getChangedElements()).thenReturn(null);

    TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    when(mockNotification.getMessage()).thenReturn(mockMsg);

    sut.handle(mockNotification);
  }

  @Test
  public void handle_changedElementsIsEmpty_rowNotCreated() {
    //Not adding an element is the key part of test
    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(RegionEvent.READ)  //Any
        .build();

    Optional<ImmutableTableRowData> actual = sut.handle(mockNotification);

    assertFalse(actual.isPresent());
  }

  @Test
  public void handle_changedElementsIsSecureElementsOnly_rowNotCreated() {
    //Arrange
    RegionChangeElement mockElement = new MockElementBuilder()
        .withSecure(true) //Important part
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(RegionEvent.READ)  //Any
        .addElement(mockElement)
        .build();

    //Act
    Optional<ImmutableTableRowData> actual = sut.handle(mockNotification);

    //Assert
    assertFalse(actual.isPresent());
  }

  @Test
  public void handle_firstChangedElementIsFieldType_rowNotCreated() {
    //Arrange
    RegionChangeElement mockElement = new MockElementBuilder()
        .withElementType(ElementType.FIELD) //Important part for this test
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(RegionEvent.READ)  //any
        .addElement(mockElement)
        .build();

    //Act
    Optional<ImmutableTableRowData> actual = sut.handle(mockNotification);

    //Assert
    assertFalse(actual.isPresent());
  }

  @Test
  public void handle_valueChangeEvent_rowNotCreated() {
    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(RegionEvent.VALUE_CHANGE)  //Important part for this test
        .build();

    Optional<ImmutableTableRowData> actual = sut.handle(mockNotification);

    assertFalse(actual.isPresent());
  }

  @Test
  public void handle_readEventWithOneElement_typeCorrect() {
    handle_eventWithOneElement_typeCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_deviceNameCorrect() {
    handle_eventWithOneElement_deviceNameCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_systemNameCorrect() {
    handle_eventWithOneElement_systemNameCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_operationCorrect() {
    handle_eventWithOneElement_operationCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_busProtocolCorrect() {
    handle_eventWithOneElement_busProtocolCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_timestampCorrect() {
    handle_eventWithOneElement_timestampCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_elapsedTimeCorrect() {
    handle_eventWithOneElement_elapsedTimeCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_addressCorrect() {
    handle_eventWithOneElement_addressCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_deviceAddressCorrect() {
    handle_eventWithOneElement_deviceAddressCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_BitWidthCorrect() {
    handle_eventWithOneElement_bitWidthCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_regFieldNameCorrect() {
    handle_eventWithOneElement_regFieldNameCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_dataCorrect() {
    handle_eventWithOneElement_dataCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_pageNotPresent() {
    handle_eventWithOneElement_pageNotPresent(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithOneElement_errorReadingFromDeviceCorrect() {
    handle_eventWithOneElement_errorReadingFromDeviceCorrect(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithTwoElements_oneRowCreated() {
    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(RegionEvent.READ) //Important part for this test
        .addElement(new MockElementBuilder().build())
        .addElement(new MockElementBuilder().build()) //Important part for this test
        .build();

    //Act
    Optional<ImmutableTableRowData> actual = sut.handle(mockNotification);

    //Assert
    assertTrue(actual.isPresent());
  }

  @Test
  public void handle_readEventWithTwoElements_firstElementTypeReturned() {
    handle_eventWithTwoElements_firstElementTypeReturned(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithTwoElements_firstElementAddressReturned() {
    handle_eventWithTwoElements_firstElementAddressReturned(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithTwoElements_firstNewValueReturned() {
    handle_eventWithTwoElements_firstNewValueReturned(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithTwoElements_firstElementNameReturned() {
    handle_eventWithTwoElements_firstElementNameReturned(RegionEvent.READ);
  }

  @Test
  public void handle_readEventWithTwoElements_pageNotPresent() {
    handle_eventWithTwoElements_pageNotPresent(RegionEvent.READ);
  }

  @Test
  public void handle_writeEventWithOneElement_typeCorrect() {
    handle_eventWithOneElement_typeCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_systemNameCorrect() {
    handle_eventWithOneElement_systemNameCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_deviceNameCorrect() {
    handle_eventWithOneElement_deviceNameCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_operationCorrect() {
    handle_eventWithOneElement_operationCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_busProtocolCorrect() {
    handle_eventWithOneElement_busProtocolCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_timestampCorrect() {
    handle_eventWithOneElement_timestampCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_elapsedTimeCorrect() {
    handle_eventWithOneElement_elapsedTimeCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_addressCorrect() {
    handle_eventWithOneElement_addressCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_deviceAddressCorrect() {
    handle_eventWithOneElement_deviceAddressCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_bitWidthCorrect() {
    handle_eventWithOneElement_bitWidthCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_regFieldNameCorrect() {
    handle_eventWithOneElement_regFieldNameCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_dataCorrect() {
    handle_eventWithOneElement_dataCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_pageNotPresent() {
    handle_eventWithOneElement_pageNotPresent(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithOneElement_errorReadingFromDeviceCorrect() {
    handle_eventWithOneElement_errorReadingFromDeviceCorrect(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithTwoElements_oneRowCreated() {
    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(RegionEvent.WRITE) //Important part for this test
        .addElement(new MockElementBuilder().build())
        .addElement(new MockElementBuilder().build()) //Important part for this test
        .build();

    //Act
    Optional<ImmutableTableRowData> actual = sut.handle(mockNotification);

    //Assert
    assertTrue(actual.isPresent());
  }

  @Test
  public void handle_writeEventWithTwoElements_firstElementTypeReturned() {
    handle_eventWithTwoElements_firstElementTypeReturned(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithTwoElements_firstElementAddressReturned() {
    handle_eventWithTwoElements_firstElementAddressReturned(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithTwoElements_firstNewValueReturned() {
    handle_eventWithTwoElements_firstNewValueReturned(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithTwoElements_firstElementNameReturned() {
    handle_eventWithTwoElements_firstElementNameReturned(RegionEvent.WRITE);
  }

  @Test
  public void handle_writeEventWithTwoElements_pageNotPresent() {
    handle_eventWithTwoElements_pageNotPresent(RegionEvent.WRITE);
  }

  @Test
  public void handle_blockReadEventWithOneElement_typeCorrect() {
    handle_eventWithOneElement_typeCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_systemNameCorrect() {
    handle_eventWithOneElement_systemNameCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_deviceNameCorrect() {
    handle_eventWithOneElement_deviceNameCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_operationCorrect() {
    handle_eventWithOneElement_operationCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_busProtocolCorrect() {
    handle_eventWithOneElement_busProtocolCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_timestampCorrect() {
    handle_eventWithOneElement_timestampCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_elapsedTimeCorrect() {
    handle_eventWithOneElement_elapsedTimeCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_addressCorrect() {
    handle_eventWithOneElement_addressCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_deviceAddressCorrect() {
    handle_eventWithOneElement_deviceAddressCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_bitWidthCorrect() {
    handle_eventWithOneElement_bitWidthCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_regFieldNameCorrect() {
    handle_eventWithOneElement_regFieldNameCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_dataCorrect() {
    handle_eventWithOneElement_dataCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_pageCorrect() {
    handle_eventWithOneElement_pageCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithOneElement_errorReadingFromDeviceCorrect() {
    handle_eventWithOneElement_errorReadingFromDeviceCorrect(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithTwoElements_oneRowCreated() {
    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(RegionEvent.BLOCK_READ) //Important part for this test
        .addElement(new MockElementBuilder().build())
        .addElement(new MockElementBuilder().build()) //Important part for this test
        .build();

    //Act
    Optional<ImmutableTableRowData> actual = sut.handle(mockNotification);

    //Assert
    assertTrue(actual.isPresent());
  }

  @Test
  public void handle_blockReadEventWithTwoElements_firstElementTypeReturned() {
    handle_eventWithTwoElements_firstElementTypeReturned(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithTwoElements_firstElementAddressReturned() {
    handle_eventWithTwoElements_firstElementAddressReturned(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithTwoElements_firstNewValueReturned() {
    handle_eventWithTwoElements_firstNewValueReturned(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithTwoElements_firstElementNameReturned() {
    handle_eventWithTwoElements_firstElementNameReturned(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockReadEventWithTwoElements_firstElementPageReturned() {
    handle_eventWithTwoElements_firstElementPageReturned(RegionEvent.BLOCK_READ);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_typeCorrect() {
    handle_eventWithOneElement_typeCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_systemNameCorrect() {
    handle_eventWithOneElement_systemNameCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_deviceNameCorrect() {
    handle_eventWithOneElement_deviceNameCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_operationCorrect() {
    handle_eventWithOneElement_operationCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_busProtocolCorrect() {
    handle_eventWithOneElement_busProtocolCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_timestampCorrect() {
    handle_eventWithOneElement_timestampCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_elapsedTimeCorrect() {
    handle_eventWithOneElement_elapsedTimeCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_addressesCorrect() {
    handle_eventWithOneElement_addressCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_deviceAddressCorrect() {
    handle_eventWithOneElement_deviceAddressCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_bitWidthCorrect() {
    handle_eventWithOneElement_bitWidthCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_regFieldNameCorrect() {
    handle_eventWithOneElement_regFieldNameCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_dataCorrect() {
    handle_eventWithOneElement_dataCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_pageCorrect() {
    handle_eventWithOneElement_pageCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithOneElement_errorReadingFromDeviceCorrect() {
    handle_eventWithOneElement_errorReadingFromDeviceCorrect(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithTwoElements_oneRowCreated() {
    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(RegionEvent.BLOCK_WRITE) //Important part for this test
        .addElement(new MockElementBuilder().build())
        .addElement(new MockElementBuilder().build()) //Important part for this test
        .build();

    //Act
    Optional<ImmutableTableRowData> actual = sut.handle(mockNotification);

    //Assert
    assertTrue(actual.isPresent());
  }

  @Test
  public void handle_blockWriteEventWithTwoElements_firstElementTypeReturned() {
    handle_eventWithTwoElements_firstElementTypeReturned(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithTwoElements_firstElementAddressReturned() {
    handle_eventWithTwoElements_firstElementAddressReturned(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithTwoElements_firstNewValueReturned() {
    handle_eventWithTwoElements_firstNewValueReturned(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithTwoElements_firstElementNameReturned() {
    handle_eventWithTwoElements_firstElementNameReturned(RegionEvent.BLOCK_WRITE);
  }

  @Test
  public void handle_blockWriteEventWithTwoElements_firstElementPageReturned() {
    handle_eventWithTwoElements_firstElementPageReturned(RegionEvent.BLOCK_WRITE);
  }

  private void handle_eventWithOneElement_typeCorrect(RegionEvent regionEvent) {
    //Given
    ElementType elementType = ElementType.REGISTER; //Anything but field

    //Arrange
    RegionChangeElement mockElement = new MockElementBuilder()
        .withElementType(elementType)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement)
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getType();

    //Assert
    assertEquals(elementType.toString(), actual);
  }

  private void handle_eventWithOneElement_systemNameCorrect(RegionEvent regionEvent) {
    //Given
    String expected = "any system name";

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withSystemName(expected)
        .withRegionEvent(regionEvent)
        .addElement(new MockElementBuilder().build())
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getSystemName();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithOneElement_deviceNameCorrect(RegionEvent regionEvent) {
    //Given
    String expected = "any device name";

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withDeviceName(expected)
        .withRegionEvent(regionEvent)
        .addElement(new MockElementBuilder().build())
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getDeviceName();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithOneElement_operationCorrect(RegionEvent expected) {
    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(expected)
        .addElement(new MockElementBuilder().build())
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getOperation();

    //Assert
    assertEquals(expected.toString(), actual);
  }

  private void handle_eventWithOneElement_busProtocolCorrect(RegionEvent regionEvent) {
    //Given
    ControlInterfaceValues busProtocol = ControlInterfaceValues.CTRL_SPI;

    //Assert
    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .withControlInterface(busProtocol)
        .addElement(new MockElementBuilder().build())
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getBusProtocol();

    //Assert
    assertEquals(busProtocol.toString(), actual);
  }

  private void handle_eventWithOneElement_timestampCorrect(RegionEvent regionEvent) {
    //Given
    long expected = System.currentTimeMillis();

    //Assert
    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .withTimestamp(expected)
        .addElement(new MockElementBuilder().build())
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getTimestamp().get();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithOneElement_elapsedTimeCorrect(RegionEvent regionEvent) {
    //Given
    long expected = System.nanoTime();

    //Assert
    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .withElapsedTime(expected)
        .addElement(new MockElementBuilder().build())
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getElapsedTime().get();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithOneElement_addressCorrect(RegionEvent regionEvent) {
    //Given
    long expected = 10;

    //Arrange
    RegionChangeElement mockElement = new MockElementBuilder()
        .withElementAddress(expected)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement)
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getAddress().get();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithOneElement_deviceAddressCorrect(RegionEvent regionEvent) {
    //Given
    int address = 20;

    //Arrange
    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .withAddress(address)
        .addElement(new MockElementBuilder().build())
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getDeviceAddress().get();

    //Assert
    assertEquals(address, actual);
  }

  private void handle_eventWithOneElement_bitWidthCorrect(RegionEvent regionEvent) {
    //Given
    int bitWidth = 25;

    //Arrange
    RegionChangeElement mockElement = new MockElementBuilder()
        .withBitWidth(bitWidth)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement)
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getBitWidth().get();

    //Assert
    assertEquals(bitWidth, actual);
  }

  private void handle_eventWithOneElement_regFieldNameCorrect(RegionEvent regionEvent) {
    //Given
    String expected = "en1";

    //Arrange
    RegionChangeElement mockElement = new MockElementBuilder()
        .withElementName(expected)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement)
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getRegisterOrFieldName();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithOneElement_dataCorrect(RegionEvent regionEvent) {
    //Given
    long expected = 30;

    //Arrange
    RegionChangeElement mockElement = new MockElementBuilder()
        .withNewValue(expected)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement)
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getData().get();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithOneElement_pageCorrect(RegionEvent regionEvent) {
    //Given
    long expected = 1;

    //Arrange
    RegionChangeElement mockElement = new MockElementBuilder()
        .withIsPaged(true)
        .withElementPage(expected)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement)
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getPage().get();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithOneElement_pageNotPresent(RegionEvent regionEvent) {
    //Arrange
    RegionChangeElement mockElement = new MockElementBuilder()
        .withElementPage(0)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement)
        .build();

    //Act
    Optional<Long> actual = sut.handle(mockNotification).get().getPage();

    //Assert
    assertFalse(actual.isPresent());
  }

  private void handle_eventWithOneElement_errorReadingFromDeviceCorrect(RegionEvent regionEvent) {
    //Arrange
    RegionChangeElement mockElement = new MockElementBuilder()
        .withHasError(true)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement)
        .build();

    //Act
    boolean actual = sut.handle(mockNotification).get().getErrorReadingFromDevice();

    //Assert
    assertTrue(actual);
  }

  private void handle_eventWithTwoElements_firstElementTypeReturned(RegionEvent regionEvent) {
    //Given
    ElementType elementType = ElementType.REGISTER; //Important for this test

    //Arrange
    RegionChangeElement mockElement1 = new MockElementBuilder()
        .withElementType(elementType)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement1)
        .addElement(new MockElementBuilder().build()) //Details of second element not important here
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getType();

    //Assert
    assertEquals(elementType.toString(), actual);
  }

  private void handle_eventWithTwoElements_firstElementAddressReturned(RegionEvent regionEvent) {
    //Given
    long expected = 10L;

    //Arrange
    RegionChangeElement mockElement1 = new MockElementBuilder()
        .withElementAddress(expected)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement1)
        .addElement(new MockElementBuilder().build()) //Details of second element not important here
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getAddress().get();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithTwoElements_firstNewValueReturned(RegionEvent regionEvent) {
    //Given
    long expected = 30L;

    //Arrange
    RegionChangeElement mockElement1 = new MockElementBuilder()
        .withNewValue(expected)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement1)
        .addElement(new MockElementBuilder().build()) //Details of second element not important here
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getData().get();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithTwoElements_firstElementNameReturned(RegionEvent regionEvent) {
    //Given
    String expected = "any element name";

    //Arrange
    RegionChangeElement mockElement1 = new MockElementBuilder()
        .withElementName(expected)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement1)
        .addElement(new MockElementBuilder().build()) //Details of second element not important here
        .build();

    //Act
    String actual = sut.handle(mockNotification).get().getRegisterOrFieldName();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithTwoElements_firstElementPageReturned(RegionEvent regionEvent) {
    //Given
    long expected = 0L;

    //Arrange
    RegionChangeElement mockElement1 = new MockElementBuilder()
        .withIsPaged(true)
        .withElementPage(expected)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement1)
        .addElement(new MockElementBuilder().build()) //Details of second element not important here
        .build();

    //Act
    long actual = sut.handle(mockNotification).get().getPage().get();

    //Assert
    assertEquals(expected, actual);
  }

  private void handle_eventWithTwoElements_pageNotPresent(RegionEvent regionEvent) {
    //Arrange
    RegionChangeElement mockElement1 = new MockElementBuilder()
        .withElementPage(0)
        .build();

    TimestampedNotification mockNotification = new Fixture()
        .withRegionEvent(regionEvent)
        .addElement(mockElement1)
        .addElement(new MockElementBuilder().build()) //Details of second element not important here
        .build();

    //Act
    Optional<Long> actual = sut.handle(mockNotification).get().getPage();

    //Assert
    assertFalse(actual.isPresent());
  }

  private static class MockElementBuilder {
    private final RegionChangeElement mockElement = mock(RegionChangeElement.class);

    private String elementName = "any element name";
    private ElementType elementType = ElementType.REGISTER; //Default to register
    private boolean isSecure; //Default to false

    public MockElementBuilder withBitWidth(int bitWidth) {
      when(mockElement.getBitWidth()).thenReturn(bitWidth);
      return this;
    }

    public MockElementBuilder withElementAddress(long elementAddress) {
      when(mockElement.getElementAddress()).thenReturn(elementAddress);
      return this;
    }

    public MockElementBuilder withIsPaged(boolean isPaged) {
      when(mockElement.getIsPaged()).thenReturn(isPaged);
      return this;
    }

    public MockElementBuilder withNewValue(long newValue) {
      when(mockElement.getNewValue()).thenReturn(newValue);
      return this;
    }

    public MockElementBuilder withElementType(ElementType et) {
      this.elementType = et;
      return this;
    }

    public MockElementBuilder withElementName(String en) {
      this.elementName = en;
      return this;
    }

    public MockElementBuilder withSecure(boolean is) {
      this.isSecure = is;
      return this;
    }

    public MockElementBuilder withElementPage(long elementPage) {
      when(mockElement.getElementPage()).thenReturn(elementPage);
      return this;
    }

    public MockElementBuilder withHasError(boolean hasError) {
      when(mockElement.getHasError()).thenReturn(hasError);
      return this;
    }

    public RegionChangeElement build() {
      when(mockElement.getElementName()).thenReturn(elementName);
      when(mockElement.getElementType()).thenReturn(elementType);
      when(mockElement.isSecure()).thenReturn(isSecure);

      return mockElement;
    }
  }

  private static class Fixture {
    private final TimestampedNotification mockNotification = mock(TimestampedNotification.class);
    private final RegionChangeMessageData mockMessage = mock(RegionChangeMessageData.class);
    private final List<RegionChangeElement> regionChangeElements = new LinkedList<>();

    private String systemName = "any system name";
    private String deviceName = "any device name";
    private ControlInterfaceValues controlInterface = ControlInterfaceValues.CTRL_SPI;  //Any
    private int address; //Any

    public Fixture withTimestamp(long timestamp) {
      when(mockNotification.getTimestamp()).thenReturn(timestamp);
      return this;
    }

    public Fixture withElapsedTime(long elapsedTime) {
      when(mockNotification.getElapsedTime()).thenReturn(elapsedTime);
      return this;
    }

    public Fixture withSystemName(String sn) {
      this.systemName = sn;
      return this;
    }

    public Fixture withDeviceName(String dn) {
      this.deviceName = dn;
      return this;
    }

    public Fixture withAddress(int addr) {
      this.address = addr;
      return this;
    }

    public Fixture withControlInterface(ControlInterfaceValues ci) {
      this.controlInterface = ci;
      return this;
    }

    public Fixture withRegionEvent(RegionEvent regionEvent) {
      when(mockMessage.getRegionEvent()).thenReturn(regionEvent);
      return this;
    }

    public Fixture addElement(RegionChangeElement element) {
      regionChangeElements.add(element);
      return this;
    }

    public TimestampedNotification build() {
      when(mockMessage.getSystemName()).thenReturn(systemName);
      when(mockMessage.getDeviceName()).thenReturn(deviceName);
      when(mockMessage.getAddress()).thenReturn(address);
      when(mockMessage.getControlInterface()).thenReturn(controlInterface);
      when(mockMessage.getChangedElements()).thenReturn(regionChangeElements);
      when(mockNotification.getMessage()).thenReturn(mockMessage);

      return mockNotification;
    }
  }
}

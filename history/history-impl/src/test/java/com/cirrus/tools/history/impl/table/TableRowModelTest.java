/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TableRowModelTest.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.tools.history.impl.table.ImmutableTableRowData.Builder;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Strictly speaking, these tests are integration/white-box tests.  The getters in TableRowModel are
 * called from the JavaFX framework using reflection and cannot have parameters.  The getters
 * return the result of calling methods on objects that were passed into the constructor of
 * TableRowModel.  Therefore these test can only test the combination of TableRowModel and its
 * members.
 */
public class TableRowModelTest {
  private static final String NONE = "N/A";
  private static final String MULTIPLE_DATA = "(...)";
  private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd_HH:mm:ss.sss"; //As per SCS

  @Test
  public void getSystemName_valueNotSet_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getSystemName();

    assertEquals(NONE, actual);
  }

  @Test
  public void getSystemName_valueWasSet_valueReturned() {
    String expected = "expected";

    ImmutableTableRowData row = new Builder().setSystemName(expected).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getSystemName();

    assertEquals(expected, actual);
  }

  @Test
  public void getDeviceName_valueNotSet_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getDeviceName();

    assertEquals(NONE, actual);
  }

  @Test
  public void getDeviceName_valueWasSet_valueReturned() {
    String expected = "expected";

    ImmutableTableRowData row = new Builder().setDeviceName(expected).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getDeviceName();

    assertEquals(expected, actual);
  }

  @Test
  public void getAddress_valueNotSetAndNoChildren_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getAddress();

    assertEquals(NONE, actual);
  }

  @Test
  public void getAddress_valueNotSetButHasChildren_childValueReturnedWithEllipses() {
    //Given
    long address = 10L;

    //Arrange
    List<ImmutableTableRowData> child = Collections.singletonList(
        new Builder().setAddress(address).build()
    );

    ImmutableTableRowData row = new Builder().setChildren(child).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    //Act
    String actual = sut.getAddress();

    //Assert
    String expected = String.format("0x%02x%s", address, MULTIPLE_DATA);
    assertEquals(expected, actual);
  }

  @Test
  public void getAddress_valueWasSet_valueReturned() {
    long address = 100;

    ImmutableTableRowData row = new Builder().setAddress(address).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getAddress();

    String expected = String.format("%#x", address);
    assertEquals(expected, actual);
  }

  @Test
  public void getDeviceAddress_valueNotSet_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getDeviceAddress();

    assertEquals(NONE, actual);
  }

  @Test
  public void getDeviceAddress_valueWasSet_valueReturned() {
    int devAddr = 200;

    ImmutableTableRowData row = new Builder().setDeviceAddress(devAddr).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getDeviceAddress();

    String expected = String.format("%#x", devAddr);
    assertEquals(expected, actual);
  }

  @Test
  public void getRegisterOrFieldName_valueNotSet_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getRegisterOrFieldName();

    assertEquals(NONE, actual);
  }

  @Test
  public void getRegisterOrFieldName_valueWasSet_valueReturned() {
    String expected = "expected";

    ImmutableTableRowData row = new Builder().setRegisterOrFieldName(expected).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getRegisterOrFieldName();

    assertEquals(expected, actual);
  }

  @Test
  public void getType_valueNotSet_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String type = sut.getType();

    assertEquals(NONE, type);
  }

  @Test
  public void getType_valueWasSet_valueReturned() {
    String expected = "expected";

    ImmutableTableRowData row = new Builder().setType(expected).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String type = sut.getType();

    assertEquals(expected, type);
  }

  @Test
  public void getOperation_valueNotSet_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getOperation();

    assertEquals(NONE, actual);
  }

  @Test
  public void getOperation_valueWasSet_valueReturned() {
    String expected = "expected";

    ImmutableTableRowData row = new Builder().setOperation(expected).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getOperation();

    assertEquals(expected, actual);
  }

  @Test
  public void getData_valueNotSetAndNoChildren_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getData();

    assertEquals(NONE, actual);
  }

  @Test
  public void getData_valueNotSetButHasChildren_childValueReturnedWithEllipses() {
    //Given
    long data = 10L;

    //Arrange
    List<ImmutableTableRowData> child = Collections.singletonList(
        new Builder().setData(data).build()
    );

    ImmutableTableRowData row = new Builder().setChildren(child).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    //Act
    String actual = sut.getData();

    //Assert
    String expected = String.format("0x%08x %s", data, MULTIPLE_DATA);
    assertEquals(expected, actual);
  }

  @Test
  public void getData_valueWasSet_valueReturned() {
    long data = 200;

    ImmutableTableRowData row = new Builder().setData(data).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getData();

    String expected = String.format("0x%08x", data);
    assertEquals(expected, actual);
  }

  @Test
  public void getPage_valueNotSetAndNoChildren_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getPage();

    assertEquals(NONE, actual);
  }

  @Test
  public void getPage_valueNotSetButHasChildren_childValueReturnedWithEllipses() {
    //Given
    long page = 1;

    //Arrange
    List<ImmutableTableRowData> child = Collections.singletonList(
        new Builder().setPage(page).build()
    );

    ImmutableTableRowData row = new Builder().setChildren(child).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    //Act
    String actual = sut.getPage();

    //Assert
    String expected = String.format("0x%02x", page);
    assertEquals(expected, actual);
  }

  @Test
  public void getPage_valueWasSet_valueReturned() {
    long page = 1;

    ImmutableTableRowData row = new Builder().setPage(page).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getPage();

    String expected = String.format("0x%02x", page);
    assertEquals(expected, actual);
  }

  @Test
  public void getBusProtocol_valueNotSet_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getBusProtocol();

    assertEquals(NONE, actual);
  }

  @Test
  public void getBusProtocol_valueWasSet_valueReturned() {
    String expected = "expected";

    ImmutableTableRowData row = new Builder().setBusProtocol(expected).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getBusProtocol();

    assertEquals(expected, actual);
  }

  @Test
  public void geTimestamp_valueNotSet_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getTimestamp();

    assertEquals("", actual);
  }

  @Test
  public void geTimestamp_valueWasSet_formattedValueReturned() {
    long now = System.currentTimeMillis();

    ImmutableTableRowData row = new Builder().setTimestamp(now).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    String actual = sut.getTimestamp();

    @SuppressWarnings("PMD.SimpleDateFormatNeedsLocale") //We want default/system locale
    String expected = new SimpleDateFormat(TIMESTAMP_FORMAT).format(new Date(now));
    assertEquals(expected, actual);
  }

  @Test
  public void getElapsedTime_valueNotSet_defaultReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    Long actual = sut.getElapsedTime();

    assertNull(actual);
  }

  @Test
  public void geElapsedTime_valueWasSet_valueReturned() {
    long elapsedTime = System.nanoTime();

    ImmutableTableRowData row = new Builder().setElapsedTime(elapsedTime).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    Long actual = sut.getElapsedTime();

    assertEquals(elapsedTime, actual.longValue());
  }

  @Test
  public void rowHasError_noErrors_falseReturned() {
    ImmutableTableRowData row = newRowWithDefaults();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    boolean actual = sut.rowHasError();

    assertFalse(actual);
  }

  @Test
  public void rowHasError_parentHasError_trueReturned() {
    ImmutableTableRowData row = new Builder().setErrorReadingFromDevice(true).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    boolean actual = sut.rowHasError();

    assertTrue(actual);
  }

  @Test
  public void rowHasError_firstChildHasError_trueReturned() {
    //Arrange
    List<ImmutableTableRowData> children = Collections.singletonList(
        new Builder().setErrorReadingFromDevice(true).build()
    );

    ImmutableTableRowData row = new Builder().setChildren(children).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    //Act
    boolean actual = sut.rowHasError();

    //Assert
    assertTrue(actual);
  }

  /**
   * This test verifies that if the second (or subsequent) element of a multi-element change
   * request message is marked as having an error, the row is highlighted as having an error,
   * even if the first/top element did not have an error.
   */
  @Test
  public void rowHasError_onlySecondChildHasError_trueReturned() {
    //Arrange
    List<ImmutableTableRowData> children = Arrays.asList(
        new Builder().setErrorReadingFromDevice(false).build(),
        new Builder().setErrorReadingFromDevice(true).build()
    );

    ImmutableTableRowData row = new Builder().setChildren(children).build();
    TableRowModel sut = new TableRowModel(row, new TableRowFormatter());

    //Act
    boolean actual = sut.rowHasError();

    //Assert
    assertTrue(actual);
  }

  private ImmutableTableRowData newRowWithDefaults() {
    return new Builder().build();
  }
}

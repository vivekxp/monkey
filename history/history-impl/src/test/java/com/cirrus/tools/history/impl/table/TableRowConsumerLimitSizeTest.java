/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TableRowConsumerLimitSizeTest.java
 */
package com.cirrus.tools.history.impl.table;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class TableRowConsumerLimitSizeTest {

  private final List<TableRowModel> testList = new ArrayList<>();
  private final TableRowModel mockData1 = mock(TableRowModel.class);
  private final TableRowModel mockData2 = mock(TableRowModel.class);
  private final TableRowModel mockData3 = mock(TableRowModel.class);
  private final TableRowModel mockData4 = mock(TableRowModel.class);

  @Before
  public final void init() {
    testList.clear();
  }

  @Test
  public void constructor_addMoreItemsThanMaxItems_itemsAreRemovedFromFrontOfList() {
    TableRowConsumer sut = new TableRowConsumer(testList);
    sut.setMinRemoval(0);
    sut.setMaxItems(2);

    sut.accept(Arrays.asList(mockData1, mockData2, mockData3));

    assertEquals("List size not as expected", 2, testList.size());
    assertEquals("mockData2 not head of list ", mockData2, testList.get(0));
    assertEquals("mockData3 not at end of list", mockData3, testList.get(1));
  }

  @Test
  public void setMaxItems_addItemsThenReduceMaxItems_itemsAreRemovedFromFrontOfList() {
    TableRowConsumer sut = new TableRowConsumer(testList);
    sut.setMinRemoval(0);
    sut.setMaxItems(5);
    sut.accept(Arrays.asList(mockData1, mockData2, mockData3));

    sut.setMaxItems(2);

    assertEquals("List size not as expected", 2, testList.size());
    assertEquals("mockData2 is not at head of list", mockData2, testList.get(0));
    assertEquals("mockData3 not tail of list", mockData3, testList.get(1));
  }

  @Test
  public void setMinRemoval_addItemsThenReduceMinRemoval_itemsAreRemovedFromFrontOfList() {
    TableRowConsumer sut = new TableRowConsumer(testList);
    sut.setMinRemoval(5);
    sut.setMaxItems(2);
    sut.accept(Arrays.asList(mockData1, mockData2, mockData3));

    sut.setMinRemoval(0);

    assertEquals("List size not as expected", 2, testList.size());
    assertEquals("mockData2 not at the head of list", mockData2, testList.get(0));
    assertEquals("mockData3 not at the tail of list", mockData3, testList.get(1));
  }

  @Test
  public void setMinRemoval_addItems_itemsAreOnlyRemovedWhenWindowIsFull() {
    TableRowConsumer sut = new TableRowConsumer(testList);
    sut.setMinRemoval(2);
    sut.setMaxItems(2);
    sut.accept(Arrays.asList(mockData1, mockData2, mockData3));

    assertEquals("List size not as expected before adding 4th item", 3, testList.size());

    sut.accept(Arrays.asList(mockData4));
    assertEquals("List size not as expected, items are not removed", 2, testList.size());
  }

}

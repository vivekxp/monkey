/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file ImmutableTableRowDataTest.java
 */
package com.cirrus.tools.history.impl.table;

import com.cirrus.tools.history.impl.table.ImmutableTableRowData.Builder;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@SuppressWarnings({"PMD.TooManyMethods", "OptionalGetWithoutIsPresent"}) //Shouldn't apply to test classes
public class ImmutableTableRowDataTest {

  @Test
  public void getSystemName_valueWasSet_valueReturned() {
    String expected = "system name";

    ImmutableTableRowData sut = new Builder().setSystemName(expected).build();

    String actual = sut.getSystemName();

    assertEquals(expected, actual);
  }

  @Test
  public void getSystemName_valueNotSet_defaultReturned() {
    ImmutableTableRowData sut = newRowWithDefaults();

    String actual = sut.getSystemName();

    assertEquals("", actual);
  }

  @Test
  public void getDeviceName_valueWasSet_valueReturned() {
    String expected = "device name";

    ImmutableTableRowData sut = new Builder().setDeviceName(expected).build();

    String actual = sut.getDeviceName();

    assertEquals(expected, actual);
  }

  @Test
  public void getDeviceName_valueNotSet_defaultReturned() {
    ImmutableTableRowData sut = newRowWithDefaults();

    String actual = sut.getDeviceName();

    assertEquals("", actual);
  }

  @Test
  public void getDeviceAddress_valueWasSet_valueReturned() {
    int expected = 5;

    ImmutableTableRowData sut = new Builder().setDeviceAddress(expected).build();

    Optional<Integer> actual = sut.getDeviceAddress();

    assertEquals(Optional.of(expected), actual);
  }

  @Test
  public void getDeviceAddress_valueNotSet_resultNotPresent() {
    ImmutableTableRowData sut = newRowWithDefaults();

    Optional<Integer> actual = sut.getDeviceAddress();

    assertFalse(actual.isPresent());
  }

  @Test
  public void getBitWidth_valueWasSet_valueReturned() {
    int expected = 55;

    ImmutableTableRowData sut = new Builder().setBitWidth(expected).build();

    Optional<Integer> actual = sut.getBitWidth();

    assertEquals(Optional.of(expected), actual);
  }

  @Test
  public void getBitWidth_valueNotSet_resultNotPresent() {
    ImmutableTableRowData sut = newRowWithDefaults();

    Optional<Integer> actual = sut.getBitWidth();

    assertFalse(actual.isPresent());
  }

  @Test
  public void getOperation_valueWasSet_valueReturned() {
    String expected = "operation";

    ImmutableTableRowData sut = new Builder().setOperation(expected).build();

    String actual = sut.getOperation();

    assertEquals(expected, actual);
  }

  @Test
  public void getOperation_valueNotSet_defaultReturned() {
    ImmutableTableRowData sut = newRowWithDefaults();

    String actual = sut.getOperation();

    assertEquals("", actual);
  }

  @Test
  public void getBusProtocol_valueWasSet_valueReturned() {
    String expected = "bus protocol";

    ImmutableTableRowData sut = new Builder().setBusProtocol(expected).build();

    String actual = sut.getBusProtocol();

    assertEquals(expected, actual);
  }

  @Test
  public void getBusProtocol_valueNotSet_defaultReturned() {
    ImmutableTableRowData sut = newRowWithDefaults();

    String actual = sut.getBusProtocol();

    assertEquals("", actual);
  }

  @Test
  public void getTimestamp_valueWasSet_valueReturned() {
    long expected = 100L;

    ImmutableTableRowData sut = new Builder().setTimestamp(expected).build();

    long actual = sut.getTimestamp().get();

    assertEquals(expected, actual);
  }

  @Test
  public void getTimestamp_valueNotSet_valueNotPresent() {
    ImmutableTableRowData sut = newRowWithDefaults();

    Optional<Long> actual = sut.getTimestamp();

    assertFalse(actual.isPresent());
  }

  @Test
  public void getElapsedTime_valueWasSet_valueReturned() {
    long expected = 100L;

    ImmutableTableRowData sut = new Builder().setElapsedTime(expected).build();

    long actual = sut.getElapsedTime().get();

    assertEquals(expected, actual);
  }

  @Test
  public void getElapsedTime_valueNotSet_valueNotPresent() {
    ImmutableTableRowData sut = newRowWithDefaults();

    Optional<Long> actual = sut.getElapsedTime();

    assertFalse(actual.isPresent());
  }

  @Test
  public void getAddress_valueWasSet_valueReturned() {
    long expected = 10L;

    ImmutableTableRowData sut = new Builder().setAddress(expected).build();

    Optional<Long> actual = sut.getAddress();

    assertEquals(Optional.of(expected), actual);
  }

  @Test
  public void getAddress_valueNotSetAndNoChildren_resultNotPresent() {
    ImmutableTableRowData sut = newRowWithDefaults();

    Optional<Long> actual = sut.getAddress();

    assertFalse(actual.isPresent());
  }

  @Test
  public void getAddress_valueNotSetButHasChildren_firstChildAddressReturned() {
    //Given
    long expected = 10L;

    //Arrange
    List<ImmutableTableRowData> child = Collections.singletonList(
        new Builder().setAddress(expected).build()
    );
    ImmutableTableRowData sut = new Builder().setChildren(child).build();

    //Act
    Optional<Long> actual = sut.getAddress();

    //Assert
    assertEquals(Optional.of(expected), actual);
  }

  @Test
  public void getData_valueWasSet_valueReturned() {
    long expected = 10L;

    ImmutableTableRowData sut = new Builder().setData(expected).build();

    Optional<Long> actual = sut.getData();

    assertEquals(Optional.of(expected), actual);
  }

  @Test
  public void getData_valueNotSetAndNoChildren_resultNotPresent() {
    ImmutableTableRowData sut = newRowWithDefaults();

    Optional<Long> actual = sut.getData();

    assertFalse(actual.isPresent());
  }

  @Test
  public void getData_valueNotSetButHasChildren_firstChildDataReturned() {
    //Given
    long expected = 10L;

    //Arrange
    List<ImmutableTableRowData> child = Collections.singletonList(
        new Builder().setData(expected).build()
    );
    ImmutableTableRowData sut = new Builder().setChildren(child).build();

    //Act
    Optional<Long> actual = sut.getData();

    //Assert
    assertEquals(Optional.of(expected), actual);
  }

  @Test
  public void getRegisterOrFieldName_valueWasSet_valueReturned() {
    String expected = "name";

    ImmutableTableRowData sut = new Builder().setRegisterOrFieldName(expected).build();

    String actual = sut.getRegisterOrFieldName();

    assertEquals(expected, actual);
  }

  @Test
  public void getRegisterOrFieldName_valueNotSetAndNoChildren_defaultReturned() {
    ImmutableTableRowData sut = newRowWithDefaults();

    String actual = sut.getRegisterOrFieldName();

    assertEquals("", actual);
  }

  @Test
  public void getRegisterOrFieldName_valueNotSetButHasChildren_firstChildAddressReturned() {
    //Given
    String expected = "name";

    //Arrange
    List<ImmutableTableRowData> child = Collections.singletonList(
        new Builder().setRegisterOrFieldName(expected).build()
    );
    ImmutableTableRowData sut = new Builder().setChildren(child).build();

    //Act
    String actual = sut.getRegisterOrFieldName();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void getType_valueWasSet_valueReturned() {
    String expected = "T1";

    ImmutableTableRowData sut = new Builder().setType(expected).build();

    String actual = sut.getType();

    assertEquals(expected, actual);
  }

  @Test
  public void getType_valueNotSet_defaultReturned() {
    ImmutableTableRowData sut = newRowWithDefaults();

    String actual = sut.getType();

    assertEquals("", actual);
  }

  @Test
  public void getPage_valueWasSet_valueReturned() {
    long expected = 10L;

    ImmutableTableRowData sut = new Builder().setPage(expected).build();

    Optional<Long> actual = sut.getPage();

    assertEquals(Optional.of(expected), actual);
  }

  @Test
  public void getPage_valueNotSetAndNoChildren_resultNotPresent() {
    ImmutableTableRowData sut = newRowWithDefaults();

    Optional<Long> actual = sut.getPage();

    assertFalse(actual.isPresent());
  }

  @Test
  public void getPage_valueNotSetButHasChildren_firstChildPageReturned() {
    //Given
    long expected = 10L;

    //Arrange
    List<ImmutableTableRowData> child = Collections.singletonList(
        new Builder().setPage(expected).build()
    );
    ImmutableTableRowData sut = new Builder().setChildren(child).build();

    //Act
    Optional<Long> actual = sut.getPage();

    //Assert
    assertEquals(Optional.of(expected), actual);
  }

  @Test
  public void getChildren_valueWasSet_valueReturned() {
    //Arrange
    List<ImmutableTableRowData> expected = Collections.singletonList(
        new Builder().setDeviceName("child").build()
    );
    ImmutableTableRowData sut = new Builder().setChildren(expected).build();

    //Act
    List<ImmutableTableRowData> actual = sut.getChildren();

    //Assert
    assertEquals(expected, actual);
  }

  @Test
  public void getChildren_valueNotSet_defaultReturned() {
    ImmutableTableRowData sut = newRowWithDefaults();

    List<ImmutableTableRowData> actual = sut.getChildren();

    assertTrue(actual.isEmpty());
  }

  @Test(expected = UnsupportedOperationException.class)
  public void getChildren_modifyChildList_exceptionThrown() {
    ImmutableTableRowData sut = newRowWithDefaults();
    List<ImmutableTableRowData> immutableList = sut.getChildren();

    //Act
    immutableList.add(sut);
  }

  @Test
  public void getErrorReadingFromDevice_trueWasSet_trueReturned() {
    ImmutableTableRowData sut = new Builder().setErrorReadingFromDevice(true).build();

    boolean actual = sut.getErrorReadingFromDevice();

    assertTrue(actual);
  }

  @Test
  public void getErrorReadingFromDevice_falseWasSet_falseReturned() {
    ImmutableTableRowData sut = new Builder().setErrorReadingFromDevice(false).build();

    boolean actual = sut.getErrorReadingFromDevice();

    assertFalse(actual);
  }

  @Test
  public void getErrorReadingFromDevice_valueNotSet_falseReturned() {
    ImmutableTableRowData sut = newRowWithDefaults();

    boolean actual = sut.getErrorReadingFromDevice();

    assertFalse(actual);
  }

  @Test
  public void rowHasError_noErrors_falseReturned() {
    ImmutableTableRowData sut = newRowWithDefaults();

    boolean actual = sut.rowHasError();

    assertFalse(actual);
  }

  @Test
  public void rowHasError_parentHasError_trueReturned() {
    ImmutableTableRowData sut = new Builder().setErrorReadingFromDevice(true).build();

    boolean actual = sut.rowHasError();

    assertTrue(actual);
  }

  @Test
  public void rowHasError_firstChildHasError_trueReturned() {
    List<ImmutableTableRowData> children = Collections.singletonList(
        new Builder().setErrorReadingFromDevice(true).build()
    );
    ImmutableTableRowData sut = new Builder().setChildren(children).build();

    boolean actual = sut.rowHasError();

    assertTrue(actual);
  }

  /**
   * This test verifies that if the second (or subsequent) element of a multi-element change
   * request message is marked as having an error, the row is highlighted as having an error,
   * even if the first/top element did not have an error.
   */
  @Test
  public void rowHasError_onlySecondChildHasError_trueReturned() {
    //Arrange
    List<ImmutableTableRowData> children = Arrays.asList(
        new Builder().setErrorReadingFromDevice(false).build(),
        new Builder().setErrorReadingFromDevice(true).build()
    );

    ImmutableTableRowData sut = new Builder().setChildren(children).build();

    //Act
    boolean actual = sut.rowHasError();

    //Assert
    assertTrue(actual);
  }

  private ImmutableTableRowData newRowWithDefaults() {
    return new Builder().build();
  }
}

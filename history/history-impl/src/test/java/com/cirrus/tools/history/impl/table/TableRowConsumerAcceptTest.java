/*
 * Copyright (c) 2020. Cirrus Logic, Inc and
 * Cirrus Logic International Semiconductor Ltd.  All rights reserved.
 *
 * This software as well as any related documentation is furnished under
 * license and may only be used or copied in accordance with the terms of the
 * license.  The information in this file is furnished for informational use
 * only, is subject to change without notice, and should not be construed as
 * a commitment by Cirrus Logic.  Cirrus Logic assumes no responsibility or
 * liability for any errors or inaccuracies that may appear in this document
 * or any software that may be provided in association with this document.
 *
 * Except as permitted by such license, no part of this document may be
 * reproduced, stored in a retrieval system, or transmitted in any form or by
 * any means without the express written consent of Cirrus Logic.
 *
 * Warning
 * This software is specifically written for Cirrus Logic devices.
 * It may not be used with other devices.
 *
 * !@file TableRowConsumerAcceptTest.java
 */
package com.cirrus.tools.history.impl.table;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;

public class TableRowConsumerAcceptTest {
  private final AtomicLong nextTimestamp = new AtomicLong(System.currentTimeMillis());
  private final List<TableRowModel> list = new ArrayList<>();

  @Test(expected = NullPointerException.class)
  public void accept_rowsIsNull_nullPointerExceptionThrown() {
    TableRowConsumer sut = new TableRowConsumer(list);

    sut.accept(null);
  }

  @Test
  public void accept_rowsIsEmpty_noException() {
    TableRowConsumer sut = new TableRowConsumer(list);

    sut.accept(Collections.emptyList());
  }

  @Test
  public void accept_itemExistsInList_itemAppendedToEndOfTable() {
    //Given
    TableRowModel expected = mock(TableRowModel.class);

    TableRowConsumer sut = new TableRowConsumer(list);
    list.add(mock(TableRowModel.class));  //Add a pre-existing row

    //Act
    sut.accept(Collections.singletonList(expected));

    //Assert
    waitForTableToBePopulated(2);
    assertSame(list.get(1), expected);
  }

  @Test
  public void accept_multipleRows_allRowsAddedInAscendingOrder() {
    //Given
    List<TableRowModel> rowsToAdd = getNewRowsInAscendingTimestampOrder(3);

    //Arrange
    TableRowConsumer sut = new TableRowConsumer(list);

    //Act
    sut.accept(rowsToAdd);

    //Assert
    waitForTableToBePopulated(rowsToAdd.size());
    assertEquals(rowsToAdd, list);
  }

  /**
   * In history, new items are always added in ascending timestamp order.
   */
  private List<TableRowModel> getNewRowsInAscendingTimestampOrder(int rowCount) {
    List<TableRowModel> rows = new LinkedList<>();

    for (int i = 0; i < rowCount; i++) {
      rows.add(getNewTableRowModel());
    }

    return rows;
  }

  private TableRowModel getNewTableRowModel() {
    ImmutableTableRowData rowData = new ImmutableTableRowData.Builder()
        .setElapsedTime(getNextElapsedTime())
        .build();

    return new TableRowModel(rowData, new TableRowFormatter());
  }

  private void waitForTableToBePopulated(int count) {
    //N.B. The total max timeout is this value multiplied by count.
    final long timeoutMillisPerCount = 100;

    for (int i = 0; i < count; i++) {
      if (list.size() >= count) {
        return;
      }

      try {
        Thread.sleep(timeoutMillisPerCount);
      } catch (InterruptedException e) {
        throw new IllegalStateException("Interrupted while waiting for table to populate.", e);
      }
    }

    throw new AssertionError("Timeout waiting for table to populate.");
  }

  /**
   * Adds one minute to elapsed time before returning current value of next elapsed time.
   */
  private long getNextElapsedTime() {
    return nextTimestamp.updateAndGet(et -> et += 60000L);
  }
}
